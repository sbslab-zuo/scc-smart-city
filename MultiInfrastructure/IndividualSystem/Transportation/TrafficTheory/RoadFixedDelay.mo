within MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory;
model RoadFixedDelay "Model of a road with constant delay"

  Modelica.Blocks.Nonlinear.FixedDelay fixDel(delayTime=600)
    annotation (Placement(transformation(extent={{-12,-10},{8,10}})));
  Modelica.Blocks.Interfaces.RealOutput qOut "Connector of Real output signal"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  Modelica.Blocks.Interfaces.RealInput qIn "Connector of Real input signal"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
equation
  connect(fixDel.y, qOut)
    annotation (Line(points={{9,0},{110,0}}, color={0,0,127}));
  connect(fixDel.u, qIn)
    annotation (Line(points={{-14,0},{-120,0}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
                             Rectangle(
          extent={{-100,24},{100,-24}},
          pattern=LinePattern.None,
          lineThickness=1,
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid), Line(
          points={{-108,60},{92,60}},
          color={255,255,255},
          pattern=LinePattern.Dash,
          thickness=0.5),                 Line(
          points={{-100,0},{100,0}},
          color={255,255,255},
          pattern=LinePattern.Dash,
          thickness=0.5),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name")}),                                 Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This model shows a road model with a constant delay time, which assumes all the vehicles are kept in an even speed and there is no car-following effect or traffic jams. </p>
</html>"));
end RoadFixedDelay;
