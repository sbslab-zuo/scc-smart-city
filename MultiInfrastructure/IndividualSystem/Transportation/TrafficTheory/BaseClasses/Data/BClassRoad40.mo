within MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data;
record BClassRoad40
  extends
    MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.Generic(
  a1=1.40,
  a2=1.88,
  a3=6.97,
  uf=11.11,
  q_nominal=800);
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)));
end BClassRoad40;
