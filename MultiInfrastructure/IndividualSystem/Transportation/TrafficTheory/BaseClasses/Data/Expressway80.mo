within MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data;
record Expressway80
  extends
    MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.Generic(
  a1=1,
  a2=1.88,
  a3=4.9,
  uf=22.22,
  q_nominal=2000);
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)));
end Expressway80;
