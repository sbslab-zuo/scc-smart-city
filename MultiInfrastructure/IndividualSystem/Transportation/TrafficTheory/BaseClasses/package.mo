within MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory;
package BaseClasses

annotation (Documentation(info="<html>
  <p>This package contains base classes that are used to construct the models in 
  <a href=\"modelica://MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory\">Transportation.TrafficTheory</a>  </p>
</html>"));
end BaseClasses;
