within MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.Examples;
model TransportationBlock
    "Example that tests the Transporation.TrafficTheory.Transportation"
  extends Modelica.Icons.Example;
  MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.TransportationBlock
    traBlo(numEV(start=300, fixed=true), num=2)
                                         "Transportation block"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Blocks.Sources.CombiTimeTable qOutSet1(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0; 1,7; 2,5; 3,3; 4,8; 5,10; 6,80; 7,100; 8,120; 9,80; 10,50; 11,30;
        12,20; 13,20; 14,30; 15,30; 16,20; 17,30; 18,30; 19,20; 20,30; 21,10; 22,
        5; 23,0; 24,0]) "Number of traffic outflow profile 1"
    annotation (Placement(transformation(extent={{-60,20},{-50,30}})));
  Modelica.Blocks.Sources.CombiTimeTable qOutSet2(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0; 1,0; 2,4; 3,0; 4,8; 5,10; 6,20; 7,15; 8,30; 9,30; 10,15; 11,30;
        12,33; 13,25; 14,30; 15,25; 16,25; 17,40; 18,33; 19,30; 20,50; 21,35; 22,
        15; 23,15; 24,0]) "Number of traffic outflow profile 2"
    annotation (Placement(transformation(extent={{-60,4},{-50,14}})));
  Modelica.Blocks.Sources.CombiTimeTable qIn1(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0; 1,8; 2,4; 3,0; 4,8; 5,10; 6,15; 7,30; 8,35; 9,25; 10,38; 11,45;
        12,23; 13,26; 14,38; 15,40; 16,15; 17,25; 18,33; 19,40; 20,20; 21,20; 22,
        10; 23,10; 24,0]) "Number of traffic inflow1"
    annotation (Placement(transformation(extent={{-60,-14},{-50,-4}})));
  Modelica.Blocks.Sources.CombiTimeTable qIn2(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0; 1,0; 2,0; 3,0; 4,8; 5,10; 6,20; 7,20; 8,30; 9,30; 10,15; 11,30;
        12,20; 13,20; 14,30; 15,30; 16,50; 17,98; 18,119; 19,80; 20,60; 21,43; 22,
        10; 23,15; 24,0]) "Number of traffic inflow2"
    annotation (Placement(transformation(extent={{-60,-32},{-50,-22}})));
  Modelica.Blocks.Sources.CombiTimeTable resPro(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.9; 1,0.9; 2,0.9; 3,0.9; 4,0.8; 5,0.7; 6,0.5; 7,0.5; 8,0.3; 9,0.4;
        10,0.35; 11,0.45; 12,0.5; 13,0.45; 14,0.4; 15,0.6; 16,0.55; 17,0.7; 18,
        0.65; 19,0.65; 20,0.75; 21,0.8; 22,0.8; 23,0.8; 24,0.9])
    "Probability of charging for a single EV at different time in a residential block"
    annotation (Placement(transformation(extent={{-60,-60},{-50,-50}})));
equation
  connect(qOutSet1.y[1], traBlo.qOutSet[1]) annotation (Line(points={{-49.5,25},
          {-34,25},{-34,6},{-12,6},{-12,4}}, color={0,0,127}));
  connect(qOutSet2.y[1], traBlo.qOutSet[2]) annotation (Line(points={{-49.5,9},
          {-34,9},{-34,6},{-12,6}},color={0,0,127}));
  connect(qIn1.y[1], traBlo.qIn[1]) annotation (Line(points={{-49.5,-9},{-34,-9},
          {-34,-1},{-12,-1}}, color={0,0,127}));
  connect(qIn2.y[1], traBlo.qIn[2]) annotation (Line(points={{-49.5,-27},{-34,
          -27},{-34,1},{-12,1}},
                            color={0,0,127}));
  connect(resPro.y[1], traBlo.proEV) annotation (Line(points={{-49.5,-55},{-26,
          -55},{-26,-8},{-12,-8}}, color={0,0,127}));
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)),
 experiment(
  StopTime=86400,
  Tolerance=1e-06),
    __Dymola_Commands(file=
          "modelica://MultiInfrastructure/Resources/Scripts/Dymola/IndividualSystem/Transportation/TrafficTheory/Examples/TransportationBlock.mos"
        "Simulate and Plot"),
    Documentation(info="<html>
<p>This example shows the use of TransportationBlock model.</p>
</html>"));
end TransportationBlock;
