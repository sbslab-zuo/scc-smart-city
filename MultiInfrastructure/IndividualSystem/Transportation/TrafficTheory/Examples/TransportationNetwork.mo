within MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.Examples;
model TransportationNetwork
  "Example that show the usage of models in Transporation.TrafficTheory.TransportationNetwork"
  import MultiInfrastructure;
  extends Modelica.Icons.Example;
  MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.TransportationBlock
    res(num=1, numEV(start=2000, fixed=true)) annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-40,0})));
  MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.TransportationBlock
    com(numEV(start=300, fixed=true), num=1) annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={40,0})));
  MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.RoadVariableDelay
    roa1(l=1600,
    numIni=25,   redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.AClassRoad100
      roaTyp) annotation (Placement(transformation(extent={{-10,10},{10,30}})));
  MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.RoadVariableDelay
    roa2(l=5000,
    numIni=25,   redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.CClassRoad30
      roaTyp)
    annotation (Placement(transformation(extent={{10,-30},{-10,-10}})));
  Modelica.Blocks.Sources.CombiTimeTable resTra(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,25; 1,35; 2,10; 3,15; 4,40; 5,50; 6,400; 7,500; 8,600; 9,400; 10,
        250; 11,150; 12,100; 13,100; 14,150; 15,150; 16,100; 17,150; 18,150; 19,
        100; 20,150; 21,50; 22,25; 23,0; 24,25])
    "Number of traffic outflow for a residential building block"
    annotation (Placement(transformation(extent={{-80,-50},{-70,-40}})));
  Modelica.Blocks.Sources.CombiTimeTable comTra(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,25; 1,0; 2,0; 3,0; 4,40; 5,50; 6,100; 7,100; 8,150; 9,150; 10,75;
        11,150; 12,100; 13,100; 14,150; 15,150; 16,250; 17,490; 18,595; 19,400;
        20,300; 21,200; 22,50; 23,75; 24,25])
    "Number of traffic outflow for a commercial building block"
    annotation (Placement(transformation(extent={{70,50},{60,60}})));
  Modelica.Blocks.Sources.CombiTimeTable resPro(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.9; 1,0.9; 2,0.9; 3,0.9; 4,0.8; 5,0.7; 6,0.5; 7,0.5; 8,0.3; 9,0.4;
        10,0.35; 11,0.45; 12,0.5; 13,0.45; 14,0.4; 15,0.6; 16,0.55; 17,0.7; 18,
        0.65; 19,0.65; 20,0.75; 21,0.8; 22,0.8; 23,0.8; 24,0.9])
    "Probability of charging for a single EV at different time in a residential block"
    annotation (Placement(transformation(extent={{-80,-70},{-70,-60}})));
  Modelica.Blocks.Sources.CombiTimeTable comPro(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.6; 1,0.5; 2,0.55; 3,0.5; 4,0.5; 5,0.6; 6,0.6; 7,0.6; 8,0.7; 9,
        0.8; 10,0.8; 11,0.7; 12,0.8; 13,0.75; 14,0.8; 15,0.7; 16,0.65; 17,0.65;
        18,0.7; 19,0.7; 20,0.7; 21,0.75; 22,0.7; 23,0.65; 24,0.6])
    "Probability of charging for a single EV at different time in a commercial block"
    annotation (Placement(transformation(extent={{70,70},{60,80}})));
equation
  connect(res.qOut[1], roa1.qIn) annotation (Line(
      points={{-40,11},{-40,20},{-12,20}},
      color={85,170,255},
      thickness=1));
  connect(roa1.qOut, com.qIn[1]) annotation (Line(
      points={{11,20},{40,20},{40,12}},
      color={85,170,255},
      thickness=1));
  connect(com.qOut[1], roa2.qIn) annotation (Line(
      points={{40,-11},{40,-20},{12,-20}},
      color={85,170,255},
      thickness=1));
  connect(roa2.qOut, res.qIn[1]) annotation (Line(
      points={{-11,-20},{-40,-20},{-40,-12}},
      color={85,170,255},
      thickness=1));
  connect(comTra.y[1], com.qOutSet[1]) annotation (Line(
      points={{59.5,55},{45,55},{45,12}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(resTra.y[1], res.qOutSet[1]) annotation (Line(
      points={{-69.5,-45},{-45,-45},{-45,-12}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(resPro.y[1], res.proEV) annotation (Line(
      points={{-69.5,-65},{-32,-65},{-32,-12}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(comPro.y[1], com.proEV) annotation (Line(
      points={{59.5,75},{32,75},{32,12}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
 experiment(
  StopTime=86400,
  Tolerance=1e-06),
    __Dymola_Commands(file=
          "Resources/Scripts/Dymola/IndividualSystem/Transportation/TrafficTheory/Examples/TransportationNetwork.mos"
        "Simulate and Plot"),
    Documentation(info="<html>
<p>This example shows the use of TransportationNetwork model.</p>
</html>"));
end TransportationNetwork;
