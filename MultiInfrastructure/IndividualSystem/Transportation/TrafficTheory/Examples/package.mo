within MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory;
package Examples "Examples that demonstrate the use of models in Transportation.TrafficTheory"
  extends Modelica.Icons.ExamplesPackage;


annotation (Documentation(info="<html>
<p>Examples to demonstrate the usage of transportation models based on traffic theory.</p>
</html>"));
end Examples;
