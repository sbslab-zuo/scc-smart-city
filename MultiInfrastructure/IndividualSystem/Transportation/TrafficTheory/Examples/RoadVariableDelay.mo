within MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.Examples;
model RoadVariableDelay
    "Example that tests the Transporation.TrafficTheory.RoadVariableDelay"
  extends Modelica.Icons.Example;
  MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.RoadVariableDelay
    roaVarDel(
    numIni=5,
    l=10000,
    redeclare BaseClasses.Data.BClassRoad40 roaTyp)
              annotation (Placement(transformation(extent={{-8,-10},{12,10}})));
  Modelica.Blocks.Sources.CombiTimeTable qIn(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,18; 1,10; 2,5; 3,8; 4,18; 5,54; 6,72; 7,108; 8,576; 9,109; 10,79;
        11,80; 12,81; 13,85; 14,97; 15,72; 16,38; 17,49; 18,65; 19,74; 20,65;
        21,28; 22,15; 23,18; 24,18])
                               "Traffic flow output for a certain block"
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));
equation
  connect(qIn.y[1], roaVarDel.qIn)
    annotation (Line(points={{-59,0},{-10,0}}, color={0,0,127}));
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)),
    experiment(
 StopTime=86400,
 Tolerance=1e-06),
    __Dymola_Commands(file=
          "modelica://MultiInfrastructure/Resources/Scripts/Dymola/IndividualSystem/Transportation/TrafficTheory/Examples/RoadVariableDelay.mos"
        "Simulate and Plot"),
    Documentation(info="<html>
<p>This example shows the use of RoadVariableDelay model.</p>
</html>"));
end RoadVariableDelay;
