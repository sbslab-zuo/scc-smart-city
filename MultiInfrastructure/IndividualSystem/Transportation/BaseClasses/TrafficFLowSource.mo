within MultiInfrastructure.IndividualSystem.Transportation.BaseClasses;
model TrafficFlowSource
  "Ideal source that produces a prescribe traffic flow rate"
  parameter Integer nPorts=0 "Number of ports" annotation(Dialog(connectorSizing=true));
  Modelica.Blocks.Interfaces.RealInput q_in(final unit="1/h")
    "Prescribed traffic flow rate" annotation (Placement(transformation(extent={{-140,60},
            {-100,100}}),          iconTransformation(extent={{-140,60},{-100,100}})));

  Modelica.Blocks.Interfaces.RealInput U_in(final unit="s")
    "Prescribed traffic cost" annotation (Placement(transformation(extent={{-140,
            10},{-100,50}}), iconTransformation(extent={{-140,20},{-100,60}})));
  MultiInfrastructure.IndividualSystem.Transportation.BaseClasses.TrafficPorts_b ports[nPorts]
    annotation (Placement(transformation(extent={{84,-40},{104,40}})));
equation
  // Only one connection allowed to a port to avoid unwanted ideal mixing
  for i in 1:nPorts loop
     assert(cardinality(ports[i]) <= 1,"
each ports[i] of boundary shall at most be connected to one component.
If two or more connections are present, ideal mixing takes
place with these connections, which is usually not the intention
of the modeller. Increase nPorts to add an additional port.
");

     ports[i].q = -q_in;
      ports[i].U = U_in;

  end for;

  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Ellipse(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,0},
          fillPattern=FillPattern.Sphere,
          fillColor={0,127,255}),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name")}),    Diagram(coordinateSystem(
          preserveAspectRatio=false)));
end TrafficFlowSource;
