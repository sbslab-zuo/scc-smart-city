within MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.Types;
type TrafficFlow = Real (final quantity = "TrafficFlow", final unit="1/h")
  "Type for traffic flow";
