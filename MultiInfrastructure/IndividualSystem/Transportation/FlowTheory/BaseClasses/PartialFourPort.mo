within MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses;
partial model PartialFourPort
 "Partial component with Four ports"
   import Modelica.Constants;
   parameter Boolean allowFlowReversal = true
    "= false to simplify equations, assuming, but not enforcing, no flow reversal for traffic flow"
    annotation(Dialog(tab="Assumptions"), Evaluate=true);

  TrafficPort_a port_a1
    "Traffic connector a1 (positive design flow direction is from port_a1 to port_b1)"
     annotation (Placement(transformation(extent={{-110,50},{-90,70}})));
  TrafficPort_b port_b1
    "Traffic connector b1 (positive design flow direction is from port_a1 to port_b1)"
     annotation (Placement(transformation(extent={{110,50},{90,70}})));
  TrafficPort_a port_a2
    "Traffic connector a2 (positive design flow direction is from port_a2 to port_b2)"
     annotation (Placement(transformation(extent={{90,-70},{110,-50}})));
  TrafficPort_b port_b2
    "Traffic connector b2 (positive design flow direction is from port_a2 to port_b2)"
     annotation (Placement(transformation(extent={{-90,-70},{-110,-50}})));

   annotation (Icon(graphics,
                    coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This partial model defines an interface for components with four ports. </p>
</html>"));
end PartialFourPort;
