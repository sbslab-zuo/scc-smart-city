within MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Examples;
model Road
  "Example that tests the Transporation.FlowTheory.Road"
  extends Modelica.Icons.Example;
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Road road(q_nominal=
       1200, U_nominal=1000)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.TrafficFlowSource
    traFloSou(nPorts=1) "Traffic flow source"
    annotation (Placement(transformation(extent={{-50,-10},{-30,10}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.FixedBoundary
    fixBou(nPorts=1) "Fixed boundary"
    annotation (Placement(transformation(extent={{50,-10},{30,10}})));
  Modelica.Blocks.Sources.CombiTimeTable qIn(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,5; 1,7; 2,2; 3,3; 4,8; 5,10; 6,80; 7,100; 8,120; 9,80; 10,50; 11,
        30; 12,20; 13,20; 14,30; 15,30; 16,20; 17,30; 18,30; 19,20; 20,30; 21,
        10; 22,5; 23,0; 24,5])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,50},{-80,70}})));
  Modelica.Blocks.Sources.RealExpression UIn(y=0.15*road.U_nominal*(qIn.y[1]/
        road.q_nominal)^5)
    annotation (Placement(transformation(extent={{-100,-6},{-80,14}})));
equation
  connect(traFloSou.ports[1], road.port_a) annotation (Line(
      points={{-30.6,0},{-20,0},{-10,0}},
      color={0,127,255},
      thickness=1));
  connect(qIn.y[1], traFloSou.q_in) annotation (Line(points={{-79,60},{-60,60},
          {-60,8},{-52,8}}, color={0,0,127}));
  connect(road.port_b, fixBou.ports[1]) annotation (Line(
      points={{10,0},{30.6,0}},
      color={0,127,255},
      thickness=1));
  connect(UIn.y, traFloSou.U_in)
    annotation (Line(points={{-79,4},{-52,4}}, color={0,0,127}));
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)),
 experiment(
  StopTime=86400,
  Tolerance=1e-06),
    __Dymola_Commands(file=
          "modelica://MultiInfrastructure/Resources/Scripts/Dymola/IndividualSystem/Transportation/FlowTheory/Examples/Road.mos"
        "Simulate and Plot"),
    Documentation(info="<html>
<p>This example shows the use of Road model.</p>
</html>"));
end Road;
