within MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Examples;
model FourPortBlock
  "Example that tests the Transporation.FourPortBlock"
  extends Modelica.Icons.Example;
  BaseClasses.TrafficFlowSource traFloSou(nPorts=2) "Traffic flow source"
    annotation (Placement(transformation(extent={{-60,-6},{-40,14}})));
  BaseClasses.FixedBoundary fixBou(nPorts=2) "Fixed Boundary"
    annotation (Placement(transformation(extent={{60,-6},{40,14}})));
  Modelica.Blocks.Sources.CombiTimeTable qIn(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,5; 1,7; 2,2; 3,3; 4,8; 5,10; 6,80; 7,100; 8,120; 9,80; 10,50; 11,
        30; 12,20; 13,20; 14,30; 15,30; 16,20; 17,30; 18,30; 19,20; 20,30; 21,
        10; 22,5; 23,0; 24,5])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,40},{-80,60}})));
  Modelica.Blocks.Sources.Constant UIn(k=0)
    annotation (Placement(transformation(extent={{-100,-20},{-80,0}})));
  Modelica.Blocks.Sources.CombiTimeTable qOut(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,5; 1,0; 2,0; 3,0; 4,8; 5,10; 6,20; 7,20; 8,30; 9,30; 10,15; 11,30;
        12,20; 13,20; 14,30; 15,30; 16,50; 17,98; 18,119; 19,80; 20,60; 21,40;
        22,10; 23,15; 24,5])
    "Number of EV charging profile for a commercial building block"
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.FourPortBlock com(
    qb1_nominal=1200,
    qb2_nominal=1200,
    Ub1_nominal=1000,
    Ub2_nominal=1000,
    numEV(fixed=true, start=50))
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
equation
  connect(qIn.y[1], traFloSou.q_in) annotation (Line(
      points={{-79,50},{-74,50},{-74,12},{-62,12}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(UIn.y, traFloSou.U_in) annotation (Line(
      points={{-79,-10},{-74,-10},{-74,8},{-62,8}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut.y[1], com.qb1) annotation (Line(
      points={{-79,90},{-20,90},{-20,4},{-12,4},{-12,3}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut.y[1], com.qb2) annotation (Line(
      points={{-79,90},{-28,90},{-28,-3},{-12,-3}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(traFloSou.ports[1], com.port_a2) annotation (Line(points={{-40.6,2},{-34,
          2},{-34,-16},{20,-16},{20,-6},{10,-6}}, color={0,127,255},
      thickness=1));
  connect(traFloSou.ports[2], com.port_a1)
    annotation (Line(points={{-40.6,6},{-25.3,6},{-10,6}}, color={0,127,255},
      thickness=1));
  connect(fixBou.ports[1], com.port_b2) annotation (Line(points={{40.6,2},{32,2},
          {32,-20},{-20,-20},{-20,-6},{-10,-6}}, color={0,127,255},
      thickness=1));
  connect(com.port_b1, fixBou.ports[2])
    annotation (Line(points={{10,6},{26,6},{40.6,6}}, color={0,127,255},
      thickness=1));
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This example shows the use of FourPortBlock model.</p>
</html>"),
 experiment(
  StopTime=86400,
  Tolerance=1e-06),
    __Dymola_Commands(file=
          "modelica://MultiInfrastructure/Resources/Scripts/Dymola/IndividualSystem/Transportation/FlowTheory/Examples/FourPortBlock.mos"
        "Simulate and Plot"));
end FourPortBlock;
