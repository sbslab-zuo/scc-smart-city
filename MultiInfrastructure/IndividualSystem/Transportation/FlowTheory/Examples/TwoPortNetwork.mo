within MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Examples;
model TwoPortNetwork "Examples that demonstrate the Transporation.FlowTheory.TwoPortBlock"
  extends Modelica.Icons.Example;
  Modelica.Blocks.Sources.CombiTimeTable resBlo(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,5; 1,7; 2,2; 3,3; 4,8; 5,10; 6,80; 7,100; 8,120; 9,80; 10,50; 11,
        30; 12,20; 13,20; 14,30; 15,30; 16,20; 17,30; 18,30; 19,20; 20,30; 21,
        10; 22,5; 23,0; 24,5])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-80,-50},{-60,-30}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.TwoPortBlock
    res(
    Ub_nominal=1000,
    qb_nominal=1200,
    numEV(fixed=true, start=600)) "Residential block" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-40,0})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.TwoPortBlock
    com(
    Ub_nominal=800,
    qb_nominal=600,
    numEV(fixed=true, start=200)) "Commercial Block" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={40,0})));
  Modelica.Blocks.Sources.CombiTimeTable comBlo(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,5; 1,0; 2,0; 3,0; 4,8; 5,10; 6,20; 7,20; 8,30; 9,30; 10,15; 11,30;
        12,20; 13,20; 14,30; 15,30; 16,50; 17,98; 18,119; 19,80; 20,60; 21,40;
        22,10; 23,15; 24,5])
    "Number of EV charging profile for a commercial building block"
    annotation (Placement(transformation(extent={{80,30},{60,50}})));

  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Road roa1(
      q_nominal=1200, U_nominal=1000)
    annotation (Placement(transformation(extent={{-10,10},{10,30}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Road roa2(
      q_nominal=600, U_nominal=800)
    annotation (Placement(transformation(extent={{10,-30},{-10,-10}})));
equation
  connect(res.port_b, roa1.port_a) annotation (Line(
      points={{-40,10},{-40,20},{-10,20}},
      color={0,127,255},
      thickness=1));
  connect(roa1.port_b, com.port_a) annotation (Line(
      points={{10,20},{40,20},{40,10}},
      color={0,127,255},
      thickness=1));
  connect(com.port_b, roa2.port_a) annotation (Line(
      points={{40,-10},{40,-20},{10,-20}},
      color={0,127,255},
      thickness=1));
  connect(roa2.port_b, res.port_a) annotation (Line(
      points={{-10,-20},{-40,-20},{-40,-10}},
      color={0,127,255},
      thickness=1));
  connect(comBlo.y[1], com.qb) annotation (Line(
      points={{59,40},{43,40},{43,12}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(resBlo.y[1], res.qb) annotation (Line(
      points={{-59,-40},{-43,-40},{-43,-12}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This example demonstrates the traffic interaction between the two blocks. 
One is the residential block and the other is the commercial block. 
Apparently, the residential blocks has a peak traffic outflow in its commuting time in the morning 
while commercial blocks has the peak in the evening commuting time. </p>
<p>The traffic outflow inputs are given and we suppose at the initialization time 0:00, 
the residential block has 1000 EVs and the commercial block has 100 EVs. 
The variation of EVs in the block can be simulated and plotted as the result.</p>
</html>"),
 experiment(
  StopTime=86400,
  Tolerance=1e-06),
    __Dymola_Commands(file=
          "modelica://MultiInfrastructure/Resources/Scripts/Dymola/IndividualSystem/Transportation/FlowTheory/Examples/TwoPortNetwork.mos"
        "Simulate and Plot"));
end TwoPortNetwork;
