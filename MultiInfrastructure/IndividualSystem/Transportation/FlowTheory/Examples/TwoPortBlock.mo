within MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Examples;
model TwoPortBlock
  "Example that tests the Transporation.TwoPortBlock"
  extends Modelica.Icons.Example;
  BaseClasses.TrafficFlowSource traFloSou(nPorts=1) "Traffic flow source"
    annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));
  BaseClasses.FixedBoundary fixBou(nPorts=1) "Fixed Boundary"
    annotation (Placement(transformation(extent={{60,-10},{40,10}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.TwoPortBlock com(
    qb_nominal=1200,
    Ub_nominal=1000,
    numEV(start=50, fixed=true))
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Blocks.Sources.CombiTimeTable qIn(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,5; 1,7; 2,2; 3,3; 4,8; 5,10; 6,80; 7,100; 8,120; 9,80; 10,50; 11,
        30; 12,20; 13,20; 14,30; 15,30; 16,20; 17,30; 18,30; 19,20; 20,30; 21,
        10; 22,5; 23,0; 24,5])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,40},{-80,60}})));
  Modelica.Blocks.Sources.Constant UIn(k=0)
    annotation (Placement(transformation(extent={{-100,-40},{-80,-20}})));
  Modelica.Blocks.Sources.CombiTimeTable qOut(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,5; 1,0; 2,0; 3,0; 4,8; 5,10; 6,20; 7,20; 8,30; 9,30; 10,15; 11,30;
        12,20; 13,20; 14,30; 15,30; 16,50; 17,98; 18,119; 19,80; 20,60; 21,40;
        22,10; 23,15; 24,5])
    "Number of EV charging profile for a commercial building block"
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
equation
  connect(traFloSou.ports[1], com.port_a)
    annotation (Line(points={{-40.6,0},{-10,0}}, color={0,127,255},
      thickness=1));
  connect(com.port_b, fixBou.ports[1])
    annotation (Line(points={{10,0},{40.6,0}}, color={0,127,255},
      thickness=1));
  connect(qIn.y[1], traFloSou.q_in) annotation (Line(
      points={{-79,50},{-74,50},{-74,8},{-62,8}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut.y[1], com.qb) annotation (Line(
      points={{-79,90},{-20,90},{-20,3},{-12,3}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(UIn.y, traFloSou.U_in) annotation (Line(
      points={{-79,-30},{-74,-30},{-74,4},{-62,4}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This example shows the use of TwoPortBlock model.</p>
</html>"),
 experiment(
  StopTime=86400,
  Tolerance=1e-06),
    __Dymola_Commands(file=
          "modelica://MultiInfrastructure/Resources/Scripts/Dymola/IndividualSystem/Transportation/FlowTheory/Examples/TwoPortBlock.mos"
        "Simulate and Plot"));
end TwoPortBlock;
