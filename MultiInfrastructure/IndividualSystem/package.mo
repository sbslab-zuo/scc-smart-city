within MultiInfrastructure;
package IndividualSystem "Collection of models that describe the individual system such as energy system, transportation system and communication system"
annotation (Documentation(info="<html>
<p>This package contains models for individual systems.</p>
</html>"));
end IndividualSystem;
