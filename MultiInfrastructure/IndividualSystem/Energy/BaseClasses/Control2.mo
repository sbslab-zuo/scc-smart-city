within MultiInfrastructure.IndividualSystem.Energy.BaseClasses;
model Control2
  Modelica.Blocks.Interfaces.RealInput Psup "Supply power"
    annotation (Placement(transformation(extent={{-140,30},{-100,70}})));
  Modelica.Blocks.Interfaces.RealInput SOC "State of charge"
    annotation (Placement(transformation(extent={{-140,-70},{-100,-30}}),
        iconTransformation(extent={{-140,-70},{-100,-30}})));
  parameter Modelica.SIunits.Time tWai "Waiting time";
  parameter Modelica.SIunits.Power thrDis "Power threshold for discharging";
  parameter Modelica.SIunits.Power thrCha "Power threshold for charging";
  parameter Real deaBan "Dead band width for switching charging/discharging on/off";
  inner Modelica.StateGraph.StateGraphRoot stateGraphRoot
    annotation (Placement(transformation(extent={{-80,68},{-60,88}})));
  Modelica.StateGraph.InitialStepWithSignal disCha(nIn=1) "discharging mode"
    annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=-90,
        origin={0,70})));
  Modelica.StateGraph.StepWithSignal staBy(nIn=2, nOut=2) "Standby" annotation (
     Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=-90,
        origin={0,0})));
  Modelica.StateGraph.StepWithSignal cha "charging mode" annotation (Placement(
        transformation(
        extent={{-10,10},{10,-10}},
        rotation=-90,
        origin={0,-70})));
  Modelica.StateGraph.Transition con1(
    enableTimer=true,
    waitTime=tWai,
    condition=SOC > 1 or SOC < 0 + deaBan or Pdem > thrDis)
    "Fire condition 1: charging mode to standby" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-40,34})));
  Modelica.StateGraph.Transition con2(
    enableTimer=true,
    waitTime=tWai,
    condition=Psup > thrCha and SOC < 1 - deaBan and SOC > 0)
    "Fire condition 2: standby to charging mode" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-40,-42})));
  Modelica.StateGraph.Transition con3(
    enableTimer=true,
    waitTime=tWai,
    condition=SOC < 0 or SOC > 1 - deaBan or Psup < thrCha)
    "Fire condition 3:charging to standby" annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=-90,
        origin={32,-42})));
  Modelica.StateGraph.Transition con4(
    enableTimer=true,
    waitTime=tWai,
    condition=Pdem < thrDis and SOC > 0 + deaBan and SOC < 1)
    "Fire condition 4: standby to discharging mode" annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=-90,
        origin={50,34})));
  Modelica.Blocks.MathInteger.MultiSwitch swi(
    y_default=0,
    nu=3,
    expr={0,1,2})
    "Switch boolean signals to real signal"
    annotation (Placement(transformation(extent={{64,-6},{88,6}})));
  Modelica.Blocks.Interfaces.IntegerOutput y "Output depending on expression"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  Modelica.Blocks.Interfaces.RealInput Pdem "Demand power"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
equation
  connect(disCha.outPort[1], con1.inPort) annotation (Line(
      points={{0,59.5},{0,50},{-40,50},{-40,38}},
      color={0,0,0},
      pattern=LinePattern.Dash));
  connect(con1.outPort, staBy.inPort[1]) annotation (Line(
      points={{-40,32.5},{-40,24},{-0.5,24},{-0.5,11}},
      color={0,0,0},
      pattern=LinePattern.Dash));
  connect(staBy.outPort[1], con2.inPort) annotation (Line(
      points={{-0.25,-10.5},{-0.25,-28},{-40,-28},{-40,-38}},
      color={0,0,0},
      pattern=LinePattern.Dash));
  connect(con2.outPort, cha.inPort[1]) annotation (Line(
      points={{-40,-43.5},{-40,-50},{0,-50},{0,-59}},
      color={0,0,0},
      pattern=LinePattern.Dash));
  connect(cha.outPort[1], con3.inPort) annotation (Line(
      points={{0,-80.5},{0,-90},{32,-90},{32,-46}},
      color={0,0,0},
      pattern=LinePattern.Dash));
  connect(con3.outPort, staBy.inPort[2]) annotation (Line(
      points={{32,-40.5},{32,24},{0.5,24},{0.5,11}},
      color={0,0,0},
      pattern=LinePattern.Dash));
  connect(con4.inPort, staBy.outPort[2]) annotation (Line(
      points={{50,30},{50,-28},{0.25,-28},{0.25,-10.5}},
      color={0,0,0},
      pattern=LinePattern.Dash));
  connect(con4.outPort, disCha.inPort[1]) annotation (Line(
      points={{50,35.5},{50,92},{0,92},{0,81}},
      color={0,0,0},
      pattern=LinePattern.Dash));
  connect(disCha.active, swi.u[1]) annotation (Line(points={{11,70},{22,70},{22,
          1.2},{64,1.2}}, color={255,0,255}));
  connect(staBy.active, swi.u[2]) annotation (Line(points={{11,-2.22045e-015},{
          38,-2.22045e-015},{38,0},{64,0}}, color={255,0,255}));
  connect(cha.active, swi.u[3]) annotation (Line(points={{11,-70},{22,-70},{22,
          -1.2},{64,-1.2}}, color={255,0,255}));
  connect(swi.y, y)
    annotation (Line(points={{88.6,0},{110,0}}, color={255,127,0}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
                                Rectangle(
        extent={{-100,-100},{100,100}},
        lineColor={0,0,127},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid), Text(
        extent={{-150,150},{150,110}},
        textString="%name",
        lineColor={0,0,255})}),                                  Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>In this control, the controller switches to the charging mode on the condition that the renewable generation has excessive load.</p>
</html>"));
end Control2;
