within MultiInfrastructure.IndividualSystem.Energy.DemandSide;
model DistributionSystem
  "This distribution system adopts the topology of IEEE16 standard test system."
  parameter Modelica.SIunits.Voltage V_nominal=V_nominal;
  parameter Modelica.SIunits.Power PBui_nominal=30000
    "Nominal power of a load";
  parameter Integer num "Number of ports in the communication block";
  parameter Integer n=12 "Number of buildings in the community";
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_p
    ter1 "Generalized terminal" annotation (Placement(transformation(extent={{-180,96},
            {-160,116}}),     iconTransformation(extent={{-180,96},{-160,116}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_p
    ter2 "Generalized terminal" annotation (Placement(transformation(extent={{-180,70},
            {-160,90}}),      iconTransformation(extent={{-180,70},{-160,90}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_p
    ter3 "Generalized terminal" annotation (Placement(transformation(extent={{-180,44},
            {-160,64}}),        iconTransformation(extent={{-180,44},{-160,64}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin1(
    use_C=false,
    V_nominal=V_nominal,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    P_nominal=4*PBui_nominal,
    l=3000,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)        "Electrical line" annotation (Placement(
        transformation(
        extent={{-10,10},{10,-10}},
        rotation=-90,
        origin={-120,92})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin3(
    P_nominal=2*PBui_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable,
    l=1000)      "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=-90,
        origin={-120,-16})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin4(
    l=1000,
    P_nominal=PBui_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=0,
        origin={-80,-60})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin5(
    P_nominal=5*PBui_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable,
    l=3000)
    "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=-90,
        origin={0,90})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin6(
    l=1000,
    P_nominal=3*PBui_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=-90,
        origin={0,30})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin7(
    l=1000,
    P_nominal=PBui_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=0,
        origin={20,44})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin8(
    l=1000,
    P_nominal=PBui_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-20,0})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin9(
    l=1000,
    P_nominal=PBui_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={0,-20})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin10(
    P_nominal=4*PBui_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable,
    l=3000)      "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={120,92})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin11(
    l=1000,
    P_nominal=PBui_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={100,44})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin12(
    l=1000,
    P_nominal=2*PBui_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={120,-20})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin13(
    l=1000,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable,
    P_nominal=PBui_nominal)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={78,-60})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin2(
    P_nominal=PBui_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable,
    l=1000)      "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=0,
        origin={-100,0})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin14(
    l=1000,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable,
    P_nominal=PBui_nominal)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-60,0})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin15(
    l=1000,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable,
    P_nominal=PBui_nominal)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={60,44})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin16(
    l=1000,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable,
    P_nominal=PBui_nominal)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={0,-60})));

  BuildBlock bui2(PBb_nominal=PBui_nominal, V_nominal=V_nominal)
    annotation (Placement(transformation(extent={{-80,-90},{-60,-70}})));
  BuildBlock bui3(PBb_nominal=PBui_nominal, V_nominal=V_nominal)
    annotation (Placement(transformation(extent={{-30,-92},{-10,-72}})));
  CommunicationTower com(num=num, V_nominal=V_nominal)
    annotation (Placement(transformation(extent={{130,-116},{150,-96}})));
  BuildBlock bui4(PBb_nominal=PBui_nominal, V_nominal=V_nominal)
    annotation (Placement(transformation(extent={{20,56},{40,76}})));
  BuildBlock bui5(PBb_nominal=PBui_nominal, V_nominal=V_nominal)
    annotation (Placement(transformation(extent={{20,-18},{40,2}})));
  BuildBlock bui6(PBb_nominal=PBui_nominal, V_nominal=V_nominal)
    annotation (Placement(transformation(extent={{-30,-34},{-10,-14}})));
  BuildBlock bui7(PBb_nominal=PBui_nominal, V_nominal=V_nominal)
    annotation (Placement(transformation(extent={{20,-52},{40,-32}})));
  BuildBlock bui8(PBb_nominal=PBui_nominal, V_nominal=V_nominal)
    annotation (Placement(transformation(extent={{50,10},{70,30}})));
  BuildBlock bui9(PBb_nominal=PBui_nominal, V_nominal=V_nominal)
    annotation (Placement(transformation(extent={{130,44},{150,64}})));
  BuildBlock bui10(PBb_nominal=PBui_nominal, V_nominal=V_nominal)
    annotation (Placement(transformation(extent={{88,-2},{108,18}})));
  BuildBlock bui11(PBb_nominal=PBui_nominal, V_nominal=V_nominal)
    annotation (Placement(transformation(extent={{132,-76},{152,-56}})));
  BuildBlock bui12(PBb_nominal=PBui_nominal, V_nominal=V_nominal)
    annotation (Placement(transformation(extent={{50,-104},{70,-84}})));
  Modelica.Blocks.Interfaces.RealInput numEV
    "Number of EV charging in the block"
    annotation (Placement(transformation(extent={{-200,-44},{-160,-4}}),
        iconTransformation(extent={{-200,-44},{-160,-4}})));

  Modelica.Blocks.Interfaces.RealInput numSenPac[num] "Number of packages sent"
    annotation (Placement(transformation(extent={{-200,-82},{-160,-42}}),
        iconTransformation(extent={{-200,-82},{-160,-42}})));
  Buildings.Controls.OBC.CDL.Interfaces.RealInput PBui [n](
    quantity="Power",
    unit="W",
    max=0)
    "Building power load(negative means consumption, positive means generation)"
    annotation (Placement(transformation(extent={{-200,-2},{-160,38}}),
        iconTransformation(extent={{-200,-2},{-160,38}})));
  BuildBlock bui1(PBb_nominal=PBui_nominal, V_nominal=V_nominal)
    annotation (Placement(transformation(extent={{-80,-40},{-60,-20}})));
  EVCharging ev( V_nominal=V_nominal)
    annotation (Placement(transformation(extent={{-100,46},{-80,66}})));
  Modelica.Blocks.Sources.RealExpression PBuiTot(y=bui1.P + bui2.P + bui3.P +
        bui4.P + bui5.P + bui6.P + bui7.P + bui8.P + bui9.P + bui10.P + bui11.P
         + bui12.P)
    annotation (Placement(transformation(extent={{140,-2},{160,18}})));
  Modelica.Blocks.Sources.RealExpression PDem(y=bui1.P + bui2.P + bui3.P + bui4.P
         + bui5.P + bui6.P + bui7.P + bui8.P + bui9.P + bui10.P + bui11.P +
        bui12.P + ev.P + com.P - lin1.cableTemp.port.Q_flow - lin2.cableTemp.port.Q_flow
         - lin3.cableTemp.port.Q_flow - lin4.cableTemp.port.Q_flow - lin5.cableTemp.port.Q_flow
         - lin6.cableTemp.port.Q_flow - lin7.cableTemp.port.Q_flow - lin8.cableTemp.port.Q_flow
         - lin9.cableTemp.port.Q_flow - lin10.cableTemp.port.Q_flow - lin11.cableTemp.port.Q_flow
         - lin12.cableTemp.port.Q_flow - lin13.cableTemp.port.Q_flow - lin14.cableTemp.port.Q_flow
         - lin15.cableTemp.port.Q_flow - lin16.cableTemp.port.Q_flow)
    annotation (Placement(transformation(extent={{140,-20},{160,0}})));
  Modelica.Blocks.Sources.RealExpression PLin(y=-lin1.cableTemp.port.Q_flow -
        lin2.cableTemp.port.Q_flow - lin3.cableTemp.port.Q_flow - lin4.cableTemp.port.Q_flow
         - lin5.cableTemp.port.Q_flow - lin6.cableTemp.port.Q_flow - lin7.cableTemp.port.Q_flow
         - lin8.cableTemp.port.Q_flow - lin9.cableTemp.port.Q_flow - lin10.cableTemp.port.Q_flow
         - lin11.cableTemp.port.Q_flow - lin12.cableTemp.port.Q_flow - lin13.cableTemp.port.Q_flow
         - lin14.cableTemp.port.Q_flow - lin15.cableTemp.port.Q_flow - lin16.cableTemp.port.Q_flow)
    annotation (Placement(transformation(extent={{140,-40},{160,-20}})));
  Modelica.Blocks.Interfaces.RealOutput powDem "Total power demand"
    annotation (Placement(transformation(extent={{180,-20},{200,0}})));
equation
  connect(lin1.terminal_n, ter1)
    annotation (Line(points={{-120,102},{-120,106},{-170,106}},
                                                     color={0,120,120}));
  connect(lin5.terminal_n, ter2) annotation (Line(points={{1.77636e-015,100},{0,
          100},{0,110},{-140,110},{-140,80},{-170,80}},
                                                      color={0,120,120}));
  connect(ter3, lin10.terminal_n) annotation (Line(points={{-170,54},{-140,54},{
          -140,116},{120,116},{120,102}},        color={0,120,120}));
  connect(bui2.term_p, lin3.terminal_p) annotation (Line(points={{-81,-74.8},{
          -120,-74.8},{-120,-26}},
                              color={0,120,120}));
  connect(bui2.term_p, lin4.terminal_n) annotation (Line(points={{-81,-74.8},{
          -120,-74.8},{-120,-60},{-90,-60}},
                                        color={0,120,120}));
  connect(bui3.term_p, lin16.terminal_p) annotation (Line(points={{-31,-76.8},{
          -40,-76.8},{-40,-60},{-10,-60}},
                                       color={0,120,120}));
  connect(bui3.term_p, lin4.terminal_p) annotation (Line(points={{-31,-76.8},{
          -40,-76.8},{-40,-60},{-70,-60}},
                                       color={0,120,120}));
  connect(bui4.term_p, lin5.terminal_p)
    annotation (Line(points={{19,71.2},{0,71.2},{0,80}},color={0,120,120}));
  connect(bui4.term_p, lin6.terminal_n)
    annotation (Line(points={{19,71.2},{0,71.2},{0,40}},color={0,120,120}));
  connect(bui4.term_p, lin7.terminal_n) annotation (Line(points={{19,71.2},{0,
          71.2},{0,44},{10,44}},
                           color={0,120,120}));
  connect(lin8.terminal_n, bui5.term_p) annotation (Line(points={{-10,
          -1.33227e-015},{-6,-1.33227e-015},{-6,0},{0,0},{0,-2.8},{19,-2.8}},
                                                               color={0,120,120}));
  connect(bui5.term_p, lin9.terminal_n)
    annotation (Line(points={{19,-2.8},{0,-2.8},{0,-10}}, color={0,120,120}));
  connect(bui5.term_p, lin6.terminal_p)
    annotation (Line(points={{19,-2.8},{0,-2.8},{0,20}}, color={0,120,120}));
  connect(bui6.term_p, lin8.terminal_p) annotation (Line(points={{-31,-18.8},{
          -40,-18.8},{-40,0},{-30,0}},
                                   color={0,120,120}));
  connect(bui6.term_p, lin14.terminal_n) annotation (Line(points={{-31,-18.8},{
          -40,-18.8},{-40,0},{-50,0}},
                                   color={0,120,120}));
  connect(bui7.term_p, lin9.terminal_p) annotation (Line(points={{19,-36.8},{0,
          -36.8},{0,-30}},
                    color={0,120,120}));
  connect(lin7.terminal_p, bui8.term_p) annotation (Line(points={{30,44},{40,44},
          {40,25.2},{49,25.2}}, color={0,120,120}));
  connect(lin15.terminal_p, bui8.term_p) annotation (Line(points={{50,44},{40,
          44},{40,25.2},{49,25.2}},
                                color={0,120,120}));
  connect(bui9.term_p, lin10.terminal_p) annotation (Line(points={{129,59.2},{120,
          59.2},{120,82}}, color={0,120,120}));
  connect(bui9.term_p, lin12.terminal_n) annotation (Line(points={{129,59.2},{
          120,59.2},{120,-10}},
                            color={0,120,120}));
  connect(bui9.term_p, lin11.terminal_n) annotation (Line(points={{129,59.2},{
          120,59.2},{120,44},{110,44}},
                                    color={0,120,120}));
  connect(bui10.term_p, lin11.terminal_p) annotation (Line(points={{87,13.2},{
          80,13.2},{80,44},{90,44}},
                                  color={0,120,120}));
  connect(bui10.term_p, lin15.terminal_n) annotation (Line(points={{87,13.2},{
          80,13.2},{80,44},{70,44}},
                                  color={0,120,120}));
  connect(bui11.term_p, lin12.terminal_p) annotation (Line(points={{131,-60.8},
          {120,-60.8},{120,-30}},color={0,120,120}));
  connect(bui11.term_p, lin13.terminal_n) annotation (Line(points={{131,-60.8},
          {120,-60.8},{120,-60},{88,-60}},color={0,120,120}));
  connect(bui12.term_p, lin16.terminal_n) annotation (Line(points={{49,-88.8},{
          44,-88.8},{44,-60},{10,-60}},
                                     color={0,120,120}));
  connect(bui12.term_p, lin13.terminal_p) annotation (Line(points={{49,-88.8},{
          44,-88.8},{44,-60},{68,-60}},
                                     color={0,120,120}));
  connect(com.numSenPac, numSenPac) annotation (Line(
      points={{128,-106},{-144,-106},{-144,-62},{-180,-62}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui[2], bui2.PBui) annotation (Line(
      points={{-180,3},{-132,3},{-132,-80},{-82,-80}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui[3], bui3.PBui) annotation (Line(
      points={{-180,6.33333},{-46,6.33333},{-46,-82},{-32,-82}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui[6], bui6.PBui) annotation (Line(
      points={{-180,16.3333},{-46,16.3333},{-46,-24},{-32,-24}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui[5], bui5.PBui) annotation (Line(
      points={{-180,13},{-4,13},{-4,-8},{18,-8}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui[7], bui7.PBui) annotation (Line(
      points={{-180,19.6667},{-4,19.6667},{-4,-42},{18,-42}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui[4], bui4.PBui) annotation (Line(
      points={{-180,9.66667},{-4,9.66667},{-4,66},{18,66}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui[8], bui8.PBui) annotation (Line(
      points={{-180,23},{-172,23},{-172,20},{48,20}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui[10], bui10.PBui) annotation (Line(
      points={{-180,29.6667},{-4,29.6667},{-4,8},{86,8}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui[9], bui9.PBui) annotation (Line(
      points={{-180,26.3333},{-178,26.3333},{-178,26},{-46,26},{-46,54},{128,54}},
      color={0,0,127},
      pattern=LinePattern.Dot));

  connect(PBui[12], bui12.PBui) annotation (Line(
      points={{-180,36.3333},{-46,36.3333},{-46,-94},{48,-94}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui[11], bui11.PBui) annotation (Line(
      points={{-180,33},{-46,33},{-46,-66},{130,-66}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(com.term_p, lin12.terminal_p) annotation (Line(points={{127,-100},{
          120,-100},{120,-30}}, color={0,120,120}));
  connect(ter2, ter2) annotation (Line(points={{-170,80},{-170,80},{-170,80}},
        color={0,120,120}));
  connect(ter3, ter3)
    annotation (Line(points={{-170,54},{-170,54}}, color={0,120,120}));
  connect(bui1.PBui, PBui[1]) annotation (Line(
      points={{-82,-30},{-132,-30},{-132,0},{-156,0},{-156,-0.333333},{-180,
          -0.333333}},
      color={0,0,127},
      pattern=LinePattern.Dot));

  connect(bui1.term_p, lin14.terminal_p) annotation (Line(points={{-81,-24.8},{
          -86,-24.8},{-86,0},{-70,0}},
                                   color={0,120,120}));
  connect(bui1.term_p, lin2.terminal_p) annotation (Line(points={{-81,-24.8},{
          -86,-24.8},{-86,0},{-90,0}},
                                   color={0,120,120}));
  connect(ev.numEV, numEV) annotation (Line(
      points={{-102,56},{-132,56},{-132,-24},{-180,-24}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(lin1.terminal_p, ev.term_p) annotation (Line(points={{-120,82},{-120,62},
          {-100,62}}, color={0,120,120}));
  connect(ev.term_p, lin3.terminal_n) annotation (Line(points={{-100,62},{-120,62},
          {-120,-6}}, color={0,120,120}));
  connect(ev.term_p, lin2.terminal_n) annotation (Line(points={{-100,62},{-120,62},
          {-120,0},{-110,0}}, color={0,120,120}));
  connect(PDem.y, powDem)
    annotation (Line(points={{161,-10},{190,-10}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-200,-140},{180,140}},
        initialScale=0.1), graphics={
        Rectangle(
          extent={{-160,120},{160,-120}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Line(
          points={{-60,-32},{-60,-72}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{-74,-32},{-44,-32}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{-72,-38},{-48,-28},{-28,-16},{-16,-2},{-12,14}},
          color={0,140,72},
          smooth=Smooth.Bezier),
        Line(
          points={{-46,-38},{-22,-28},{-2,-16},{10,-2},{14,14}},
          color={0,140,72},
          smooth=Smooth.Bezier),
        Line(
          points={{-72,-32},{-72,-38}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{-46,-32},{-46,-38}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{-100,-76},{-96,-74},{-88,-68},{-76,-54},{-72,-38}},
          color={0,140,72},
          smooth=Smooth.Bezier),
        Line(
          points={{-100,-88},{-82,-80},{-62,-68},{-50,-54},{-46,-38}},
          color={0,140,72},
          smooth=Smooth.Bezier),
        Line(
          points={{14,14},{38,24},{58,36},{70,50},{74,66}},
          color={0,140,72},
          smooth=Smooth.Bezier),
        Line(
          points={{-12,14},{12,24},{32,36},{44,50},{48,66}},
          color={0,140,72},
          smooth=Smooth.Bezier),
        Line(
          points={{60,72},{60,32}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{46,72},{76,72}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{48,72},{48,66}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{74,72},{74,66}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{48,68},{72,78},{92,90},{98,94},{100,96}},
          color={0,140,72},
          smooth=Smooth.Bezier),
        Line(
          points={{74,68},{84,72},{100,82}},
          color={0,140,72},
          smooth=Smooth.Bezier),
        Line(
          points={{0,20},{0,-20}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{-14,20},{16,20}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{-12,20},{-12,14}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{14,20},{14,14}},
          color={0,0,0},
          smooth=Smooth.None),
      Rectangle(
        extent={{-6,-50},{58,-110}},
        lineColor={150,150,150},
        fillPattern=FillPattern.Solid,
        fillColor={150,150,150}),
      Rectangle(
        extent={{6,-74},{20,-60}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{32,-74},{46,-60}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{6,-96},{20,-82}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{32,-96},{46,-82}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Polygon(
        points={{26,-26},{-16,-50},{68,-50},{26,-26}},
        lineColor={95,95,95},
        smooth=Smooth.None,
        fillPattern=FillPattern.Solid,
        fillColor={95,95,95}),
      Rectangle(
        extent={{78,-4},{142,-64}},
        lineColor={150,150,150},
        fillPattern=FillPattern.Solid,
        fillColor={150,150,150}),
      Rectangle(
        extent={{90,-28},{104,-14}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{116,-28},{130,-14}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{90,-50},{104,-36}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{116,-50},{130,-36}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Polygon(
        points={{110,20},{68,-4},{152,-4},{110,20}},
        lineColor={95,95,95},
        smooth=Smooth.None,
        fillPattern=FillPattern.Solid,
        fillColor={95,95,95}),
      Rectangle(
        extent={{-106,72},{-42,12}},
        lineColor={150,150,150},
        fillPattern=FillPattern.Solid,
        fillColor={150,150,150}),
      Rectangle(
        extent={{-94,48},{-80,62}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-68,48},{-54,62}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-94,26},{-80,40}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-68,26},{-54,40}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Polygon(
        points={{-74,96},{-116,72},{-32,72},{-74,96}},
        lineColor={95,95,95},
        smooth=Smooth.None,
        fillPattern=FillPattern.Solid,
        fillColor={95,95,95}),
        Text(
          extent={{-98,164},{104,120}},
          lineColor={0,0,0},
          textString="%name")}),
                            Diagram(graphics,
                                    coordinateSystem(
        preserveAspectRatio=false,
        extent={{-200,-140},{180,140}},
        initialScale=0.1)),
    Documentation(info="<html>
<p>This distribution system adopts the topology of the IEEE16 standard test system. It has three power feeders that can be connected to the substations.</p>
<p>For now, each node represents a building. The whole distribution system could represent a community.</p>
<p><img src=\"modelica://MultiInfrastructure/Resources/Images/IndividualSystem/Energy/DemandSide/IEEE16.PNG\"/></p>
</html>"),
    __Dymola_Commands);
end DistributionSystem;
