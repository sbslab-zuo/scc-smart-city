within MultiInfrastructure.IndividualSystem.Energy.DemandSide.Examples;
model Demand "Examples that demonstrate the demand side models"
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";

  MultiInfrastructure.IndividualSystem.Energy.DemandSide.Demand res(
    PBb_nominal=-1500000,
    V_nominal=V_nominal,
    l1=2000,
    l2=3000,
    l3=3000,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_30
      commercialCable,
    num=1)
    "Energy demand side model in a residential building block"
    annotation (Placement(transformation(extent={{20,0},{40,20}})));
  Modelica.Blocks.Sources.CombiTimeTable nev(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,100; 2,80; 4,60; 6,50; 8,30; 10,8; 12,10; 14,20; 16,30; 18,40; 20,
        60; 22,80; 24,100])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-60,-10},{-50,0}})));
  Modelica.Blocks.Sources.CombiTimeTable ns(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,0; 2,0; 4,0; 6,40; 8,100; 10,80; 12,60; 14,40; 16,30; 18,20; 20,10;
        22,0; 24,0])
    "Number of packages sent for a residential building block"
    annotation (Placement(transformation(extent={{-60,-30},{-50,-20}})));
  Modelica.Blocks.Sources.CombiTimeTable pow(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-600000; 2,-460000; 4,-380000; 6,-400000; 8,-400000; 10,-300000;
        12,-200000; 14,-300000; 16,-600000; 18,-800000; 20,-1000000; 22,-800000;
        24,-600000]) "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-60,10},{-50,20}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid
    gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{-60,60},{-40,80}})));
equation
  connect(gri.terminal, res.term_p) annotation (Line(points={{-50,60},{-50,30},
          {-20,30},{-20,18},{19.2,18}},
                              color={0,120,120},
      thickness=0.5));
  connect(pow.y[1], res.PBui)
    annotation (Line(points={{-49.5,15},{18,15}},         color={0,0,127},
      pattern=LinePattern.Dot));
  connect(nev.y[1], res.numEV) annotation (Line(points={{-49.5,-5},{-49.5,-5},{
          0,-5},{0,10},{18,10}},
                               color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ns.y[1], res.numSenPac[1]) annotation (Line(points={{-49.5,-25},{6,
          -25},{6,5},{18,5}},
                         color={0,0,127},
      pattern=LinePattern.Dot));
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -60},{100,100}})),                                   Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false, extent={{-100,-60},{100,100}})),
    Documentation(info="<html>
<p>This example illustrates how to use the models from the energy demand side in a residential block. </p>
</html>"),
experiment(
      StartTime=0,
      StopTime=864000,
      Tolerance=1e-06),
    __Dymola_Commands(file=
          "modelica://MultiInfrastructure/Resources/Scripts/Dymola/IndividualSystem/Energy/DemandSide/Examples/Demand.mos"
        "Simulate and Plot"));
end Demand;
