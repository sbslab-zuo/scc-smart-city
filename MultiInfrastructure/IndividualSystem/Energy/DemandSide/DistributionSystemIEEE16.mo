within MultiInfrastructure.IndividualSystem.Energy.DemandSide;
model DistributionSystemIEEE16
  "This distribution system adopts the topology of IEEE16 standard test system."
  parameter Modelica.SIunits.Voltage V_nominal
    "Nominal distribution voltage";
  parameter Modelica.SIunits.Power PLoa_nominal
    "Nominal power of a load";

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive
    RL4(
    V_nominal=V_nominal,
    mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_y_input,
    P_nominal=-PLoa_nominal) "Load model"
    annotation (Placement(transformation(extent={{-120,40},{-100,60}})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive
    RL5(
    V_nominal=V_nominal,
    mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_y_input,
    P_nominal=-PLoa_nominal) "Load model"
    annotation (Placement(transformation(extent={{-80,-50},{-60,-30}})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive
    RL6(
    V_nominal=V_nominal,
    mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_y_input,
    P_nominal=-PLoa_nominal) "Load model"
    annotation (Placement(transformation(extent={{-120,-100},{-100,-80}})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive
    RL7(
    V_nominal=V_nominal,
    mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_y_input,
    P_nominal=-PLoa_nominal) "Load model"
    annotation (Placement(transformation(extent={{-40,-100},{-20,-80}})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive
    RL16(
    V_nominal=V_nominal,
    mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_y_input,
    P_nominal=-PLoa_nominal) "Load model"
    annotation (Placement(transformation(extent={{40,-100},{60,-80}})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive
    RL15(
    V_nominal=V_nominal,
    mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_y_input,
    P_nominal=-PLoa_nominal) "Load model"
    annotation (Placement(transformation(extent={{120,-100},{140,-80}})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive
    RL11(
    V_nominal=V_nominal,
    mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_y_input,
    P_nominal=-PLoa_nominal) "Load model"
    annotation (Placement(transformation(extent={{-40,-50},{-20,-30}})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive
    RL12(
    V_nominal=V_nominal,
    mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_y_input,
    P_nominal=-PLoa_nominal) "Load model"
    annotation (Placement(transformation(extent={{40,-50},{60,-30}})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive
    RL9(
    V_nominal=V_nominal,
    mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_y_input,
    P_nominal=-PLoa_nominal) "Load model"
    annotation (Placement(transformation(extent={{0,-10},{20,10}})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive
    RL8(
    V_nominal=V_nominal,
    mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_y_input,
    P_nominal=-PLoa_nominal) "Load model"
    annotation (Placement(transformation(extent={{0,50},{20,70}})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive
    RL10(
    V_nominal=V_nominal,
    mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_y_input,
    P_nominal=-PLoa_nominal) "Load model"
    annotation (Placement(transformation(extent={{40,-10},{60,10}})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive
    RL14(
    V_nominal=V_nominal,
    mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_y_input,
    P_nominal=-PLoa_nominal) "Load model"
    annotation (Placement(transformation(extent={{80,-10},{100,10}})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive
    RL13(
    V_nominal=V_nominal,
    mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_y_input,
    P_nominal=-PLoa_nominal) "Load model"
    annotation (Placement(transformation(extent={{120,50},{140,70}})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_p
    ter1 "Generalized terminal" annotation (Placement(transformation(extent={{-180,
            90},{-160,110}}), iconTransformation(extent={{-180,90},{-160,110}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_p
    ter2 "Generalized terminal" annotation (Placement(transformation(extent={{-180,
            -10},{-160,10}}), iconTransformation(extent={{-180,-10},{-160,10}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_p
    ter3 "Generalized terminal" annotation (Placement(transformation(extent={{-180,
            -110},{-160,-90}}), iconTransformation(extent={{-180,-110},{-160,-90}})));
  Modelica.Blocks.Sources.CombiTimeTable pow4(extrapolation=
        Modelica.Blocks.Types.Extrapolation.Periodic, table=[0,0.1; 2,0.1;
        4,0.1; 6,0.1; 8,0.8; 10,0.7; 12,0.3; 14,0.3;
        16,0.3; 18,0.8; 20,0.7; 22,0.3; 24,0.1],
    timeScale=3600) "Power consumption profile for load 4"
    annotation (Placement(transformation(extent={{-84,44},{-96,56}})));
  Modelica.Blocks.Sources.CombiTimeTable pow5(extrapolation=
        Modelica.Blocks.Types.Extrapolation.Periodic, table=[0,0.1; 2,0.1;
        4,0.1; 6,0.1; 8,0.8; 10,0.7; 12,0.3; 14,0.3;
        16,0.3; 18,0.8; 20,0.7; 22,0.3; 24,0.1],
    timeScale=3600) "Power consumption profile for load 5"
    annotation (Placement(transformation(extent={{-44,-46},{-56,-34}})));
  Modelica.Blocks.Sources.CombiTimeTable pow6(extrapolation=
        Modelica.Blocks.Types.Extrapolation.Periodic, table=[0,0.1; 2,0.1;
        4,0.1; 6,0.1; 8,0.8; 10,0.7; 12,0.3; 14,0.3;
        16,0.3; 18,0.8; 20,0.7; 22,0.3; 24,0.1],
    timeScale=3600) "Power consumption profile for load 6"
    annotation (Placement(transformation(extent={{-84,-96},{-96,-84}})));
  Modelica.Blocks.Sources.CombiTimeTable pow15(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=[0,0.1; 2,0.1; 4,0.1; 6,0.1; 8,0.8; 10,0.7; 12,0.3; 14,0.3; 16,0.3; 18,
        0.8; 20,0.7; 22,0.3; 24,0.1],
    timeScale=3600) "Power consumption profile for load 15"
    annotation (Placement(transformation(extent={{156,-96},{144,-84}})));
  Modelica.Blocks.Sources.CombiTimeTable pow13(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=[0,0.1; 2,0.1; 4,0.1; 6,0.1; 8,0.8; 10,0.7; 12,0.3; 14,0.3; 16,0.3; 18,
        0.8; 20,0.7; 22,0.3; 24,0.1],
    timeScale=3600) "Power consumption profile for load 13"
    annotation (Placement(transformation(extent={{156,54},{144,66}})));
  Modelica.Blocks.Sources.CombiTimeTable pow8(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=[0,0.1; 2,0.1; 4,0.1; 6,0.1; 8,0.8; 10,0.7; 12,0.3; 14,0.3; 16,0.3; 18,
        0.8; 20,0.7; 22,0.3; 24,0.1],
    timeScale=3600) "Power consumption profile for load 8"
    annotation (Placement(transformation(extent={{36,54},{24,66}})));
  Modelica.Blocks.Sources.CombiTimeTable pow16(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=[0,0.1; 2,0.1; 4,0.1; 6,0.1; 8,0.8; 10,0.7; 12,0.3; 14,0.3; 16,0.3; 18,
        0.8; 20,0.7; 22,0.3; 24,0.1],
    timeScale=3600) "Power consumption profile for load 16"
    annotation (Placement(transformation(extent={{76,-96},{64,-84}})));
  Modelica.Blocks.Sources.CombiTimeTable pow14(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=[0,0.1; 2,0.1; 4,0.1; 6,0.1; 8,0.8; 10,0.7; 12,0.3; 14,0.3; 16,0.3; 18,
        0.8; 20,0.7; 22,0.3; 24,0.1],
    timeScale=3600) "Power consumption profile for load 14"
    annotation (Placement(transformation(extent={{116,-6},{104,6}})));
  Modelica.Blocks.Sources.CombiTimeTable pow12(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=[0,0.1; 2,0.1; 4,0.1; 6,0.1; 8,0.8; 10,0.7; 12,0.3; 14,0.3; 16,0.3; 18,
        0.8; 20,0.7; 22,0.3; 24,0.1],
    timeScale=3600) "Power consumption profile for load 12"
    annotation (Placement(transformation(extent={{76,-46},{64,-34}})));
  Modelica.Blocks.Sources.CombiTimeTable pow10(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=[0,0.1; 2,0.1; 4,0.1; 6,0.1; 8,0.8; 10,0.7; 12,0.3; 14,0.3; 16,0.3; 18,
        0.8; 20,0.7; 22,0.3; 24,0.1],
    timeScale=3600) "Power consumption profile for load 10"
    annotation (Placement(transformation(extent={{76,-6},{64,6}})));
  Modelica.Blocks.Sources.CombiTimeTable pow9(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=[0,0.1; 2,0.1; 4,0.1; 6,0.1; 8,0.8; 10,0.7; 12,0.3; 14,0.3; 16,0.3; 18,
        0.8; 20,0.7; 22,0.3; 24,0.1],
    timeScale=3600) "Power consumption profile for load 9"
    annotation (Placement(transformation(extent={{36,-6},{24,6}})));
  Modelica.Blocks.Sources.CombiTimeTable pow11(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=[0,0.1; 2,0.1; 4,0.1; 6,0.1; 8,0.8; 10,0.7; 12,0.3; 14,0.3; 16,0.3; 18,
        0.8; 20,0.7; 22,0.3; 24,0.1],
    timeScale=3600) "Power consumption profile for load 11"
    annotation (Placement(transformation(extent={{-4,-46},{-16,-34}})));
  Modelica.Blocks.Sources.CombiTimeTable pow7(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=[0,0.1; 2,0.1; 4,0.1; 6,0.1; 8,0.8; 10,0.7; 12,0.3; 14,0.3; 16,0.3; 18,
        0.8; 20,0.7; 22,0.3; 24,0.1],
    timeScale=3600) "Power consumption profile for load 7"
    annotation (Placement(transformation(extent={{-4,-96},{-16,-84}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin1(
    l=2000,
    use_C=false,
    V_nominal=V_nominal,
    P_nominal=4*PLoa_nominal,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_1
      commercialCable)        "Electrical line" annotation (Placement(
        transformation(
        extent={{-10,10},{10,-10}},
        rotation=-90,
        origin={-120,90})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin3(
    l=1000,
    P_nominal=2*PLoa_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=-90,
        origin={-120,-20})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin4(
    l=1000,
    P_nominal=PLoa_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=0,
        origin={-80,-60})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin5(
    l=2000,
    P_nominal=5*PLoa_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
    "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=-90,
        origin={0,90})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin6(
    l=1000,
    P_nominal=3*PLoa_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=-90,
        origin={0,20})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin7(
    l=1000,
    P_nominal=PLoa_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=0,
        origin={20,40})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin8(
    l=1000,
    P_nominal=PLoa_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-20,0})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin9(
    l=1000,
    P_nominal=PLoa_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={0,-20})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin10(
    P_nominal=4*PLoa_nominal,
    V_nominal=V_nominal,
    l=2000,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={120,90})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin11(
    l=1000,
    P_nominal=PLoa_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={100,40})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin12(
    l=1000,
    P_nominal=2*PLoa_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={120,-20})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin13(
    l=1000,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable,
    P_nominal=PLoa_nominal)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={78,-60})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin2(
    l=1000,
    P_nominal=PLoa_nominal,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=0,
        origin={-100,0})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin14(
    l=1000,
    P_nominal=0,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-60,0})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin15(
    l=1000,
    P_nominal=0,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={60,40})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin16(
    l=1000,
    P_nominal=0,
    V_nominal=V_nominal,
    use_C=false,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
                 "Electrical line" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={0,-60})));

equation
  connect(RL4.y, pow4.y[1])
    annotation (Line(points={{-100,50},{-96.6,50}}, color={0,0,127}));
  connect(RL15.y, pow15.y[1])
    annotation (Line(points={{140,-90},{143.4,-90}}, color={0,0,127}));
  connect(RL6.y, pow6.y[1])
    annotation (Line(points={{-100,-90},{-96.6,-90}}, color={0,0,127}));
  connect(RL13.y, pow13.y[1])
    annotation (Line(points={{140,60},{143.4,60}}, color={0,0,127}));
  connect(RL12.y, pow12.y[1])
    annotation (Line(points={{60,-40},{63.4,-40}}, color={0,0,127}));
  connect(RL9.y, pow9.y[1])
    annotation (Line(points={{20,0},{23.4,0}}, color={0,0,127}));
  connect(pow11.y[1], RL11.y)
    annotation (Line(points={{-16.6,-40},{-20,-40}}, color={0,0,127}));
  connect(lin1.terminal_p, RL4.terminal)
    annotation (Line(points={{-120,80},{-120,50}}, color={0,120,120}));
  connect(RL4.terminal, lin2.terminal_n)
    annotation (Line(points={{-120,50},{-120,0},{-110,0}}, color={0,120,120}));
  connect(lin3.terminal_p, RL6.terminal)
    annotation (Line(points={{-120,-30},{-120,-90}}, color={0,120,120}));
  connect(RL6.terminal, lin4.terminal_n) annotation (Line(points={{-120,-90},{-120,
          -60},{-90,-60}}, color={0,120,120}));
  connect(lin4.terminal_p, RL7.terminal) annotation (Line(points={{-70,-60},{-40,
          -60},{-40,-90}}, color={0,120,120}));
  connect(lin5.terminal_p, RL8.terminal)
    annotation (Line(points={{0,80},{0,60}}, color={0,120,120}));
  connect(RL8.terminal, lin6.terminal_n)
    annotation (Line(points={{0,60},{0,30}}, color={0,120,120}));
  connect(lin6.terminal_p, RL9.terminal)
    annotation (Line(points={{0,10},{0,0}}, color={0,120,120}));
  connect(RL8.terminal, lin7.terminal_n)
    annotation (Line(points={{0,60},{0,40},{10,40}}, color={0,120,120}));
  connect(lin7.terminal_p, RL10.terminal)
    annotation (Line(points={{30,40},{40,40},{40,0}}, color={0,120,120}));
  connect(RL9.terminal, lin8.terminal_n) annotation (Line(points={{0,0},{-10,0},
          {-10,-1.33227e-015}}, color={0,120,120}));
  connect(RL5.y, pow5.y[1])
    annotation (Line(points={{-60,-40},{-56.6,-40}}, color={0,0,127}));
  connect(RL9.terminal, lin9.terminal_n)
    annotation (Line(points={{0,0},{0,-10}}, color={0,120,120}));
  connect(lin9.terminal_p, RL12.terminal)
    annotation (Line(points={{0,-30},{0,-40},{40,-40}}, color={0,120,120}));
  connect(lin10.terminal_p, RL13.terminal)
    annotation (Line(points={{120,80},{120,60}}, color={0,120,120}));
  connect(RL13.terminal, lin11.terminal_n)
    annotation (Line(points={{120,60},{120,40},{110,40}}, color={0,120,120}));
  connect(lin11.terminal_p, RL14.terminal)
    annotation (Line(points={{90,40},{80,40},{80,0}}, color={0,120,120}));
  connect(lin8.terminal_p, RL11.terminal) annotation (Line(points={{-30,
          1.33227e-015},{-40,1.33227e-015},{-40,-40}}, color={0,120,120}));
  connect(RL13.terminal, lin12.terminal_n)
    annotation (Line(points={{120,60},{120,-10}}, color={0,120,120}));
  connect(lin12.terminal_p, RL15.terminal)
    annotation (Line(points={{120,-30},{120,-90}}, color={0,120,120}));
  connect(lin13.terminal_n, RL15.terminal) annotation (Line(points={{88,-60},{120,
          -60},{120,-90}},     color={0,120,120}));
  connect(lin13.terminal_p, RL16.terminal)
    annotation (Line(points={{68,-60},{40,-60},{40,-90}}, color={0,120,120}));
  connect(RL5.terminal, lin14.terminal_p) annotation (Line(points={{-80,-40},{-80,
          1.33227e-015},{-70,1.33227e-015}}, color={0,120,120}));
  connect(lin14.terminal_n, RL11.terminal) annotation (Line(points={{-50,-1.33227e-015},
          {-40,-1.33227e-015},{-40,-40}}, color={0,120,120}));
  connect(RL10.terminal, lin15.terminal_p)
    annotation (Line(points={{40,0},{40,40},{50,40}}, color={0,120,120}));
  connect(lin15.terminal_n, RL14.terminal)
    annotation (Line(points={{70,40},{80,40},{80,0}}, color={0,120,120}));
  connect(RL7.terminal, lin16.terminal_p) annotation (Line(points={{-40,-90},{-40,
          -60},{-10,-60}}, color={0,120,120}));
  connect(lin16.terminal_n, RL16.terminal)
    annotation (Line(points={{10,-60},{40,-60},{40,-90}}, color={0,120,120}));
  connect(RL4.terminal, lin3.terminal_n)
    annotation (Line(points={{-120,50},{-120,-10}}, color={0,120,120}));
  connect(lin2.terminal_p, RL5.terminal)
    annotation (Line(points={{-90,0},{-80,0},{-80,-40}}, color={0,120,120}));
  connect(pow7.y[1], RL7.y)
    annotation (Line(points={{-16.6,-90},{-20,-90}}, color={0,0,127}));
  connect(RL16.y, pow16.y[1])
    annotation (Line(points={{60,-90},{63.4,-90}}, color={0,0,127}));
  connect(RL14.y, pow14.y[1])
    annotation (Line(points={{100,0},{103.4,0}}, color={0,0,127}));
  connect(pow10.y[1], RL10.y)
    annotation (Line(points={{63.4,0},{60,0}}, color={0,0,127}));
  connect(pow8.y[1], RL8.y)
    annotation (Line(points={{23.4,60},{20,60}}, color={0,0,127}));
  connect(lin1.terminal_n, ter1)
    annotation (Line(points={{-120,100},{-170,100}}, color={0,120,120}));
  connect(lin5.terminal_n, ter2) annotation (Line(points={{1.77636e-015,100},{0,
          100},{0,108},{-150,108},{-150,0},{-170,0}}, color={0,120,120}));
  connect(ter3, lin10.terminal_n) annotation (Line(points={{-170,-100},{-140,
          -100},{-140,116},{120,116},{120,100}}, color={0,120,120}));
  annotation (Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-160,-120},{160,120}},
        initialScale=0.1), graphics={
        Rectangle(
          extent={{-160,120},{160,-120}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Line(
          points={{-60,-32},{-60,-72}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{-74,-32},{-44,-32}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{-72,-38},{-48,-28},{-28,-16},{-16,-2},{-12,14}},
          color={0,140,72},
          smooth=Smooth.Bezier),
        Line(
          points={{-46,-38},{-22,-28},{-2,-16},{10,-2},{14,14}},
          color={0,140,72},
          smooth=Smooth.Bezier),
        Line(
          points={{-72,-32},{-72,-38}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{-46,-32},{-46,-38}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{-100,-76},{-96,-74},{-88,-68},{-76,-54},{-72,-38}},
          color={0,140,72},
          smooth=Smooth.Bezier),
        Line(
          points={{-100,-88},{-82,-80},{-62,-68},{-50,-54},{-46,-38}},
          color={0,140,72},
          smooth=Smooth.Bezier),
        Line(
          points={{14,14},{38,24},{58,36},{70,50},{74,66}},
          color={0,140,72},
          smooth=Smooth.Bezier),
        Line(
          points={{-12,14},{12,24},{32,36},{44,50},{48,66}},
          color={0,140,72},
          smooth=Smooth.Bezier),
        Line(
          points={{60,72},{60,32}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{46,72},{76,72}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{48,72},{48,66}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{74,72},{74,66}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{48,68},{72,78},{92,90},{98,94},{100,96}},
          color={0,140,72},
          smooth=Smooth.Bezier),
        Line(
          points={{74,68},{84,72},{100,82}},
          color={0,140,72},
          smooth=Smooth.Bezier),
        Line(
          points={{0,20},{0,-20}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{-14,20},{16,20}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{-12,20},{-12,14}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{14,20},{14,14}},
          color={0,0,0},
          smooth=Smooth.None),
      Rectangle(
        extent={{-6,-50},{58,-110}},
        lineColor={150,150,150},
        fillPattern=FillPattern.Solid,
        fillColor={150,150,150}),
      Rectangle(
        extent={{6,-74},{20,-60}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{32,-74},{46,-60}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{6,-96},{20,-82}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{32,-96},{46,-82}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Polygon(
        points={{26,-26},{-16,-50},{68,-50},{26,-26}},
        lineColor={95,95,95},
        smooth=Smooth.None,
        fillPattern=FillPattern.Solid,
        fillColor={95,95,95}),
      Rectangle(
        extent={{78,-4},{142,-64}},
        lineColor={150,150,150},
        fillPattern=FillPattern.Solid,
        fillColor={150,150,150}),
      Rectangle(
        extent={{90,-28},{104,-14}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{116,-28},{130,-14}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{90,-50},{104,-36}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{116,-50},{130,-36}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Polygon(
        points={{110,20},{68,-4},{152,-4},{110,20}},
        lineColor={95,95,95},
        smooth=Smooth.None,
        fillPattern=FillPattern.Solid,
        fillColor={95,95,95}),
      Rectangle(
        extent={{-106,72},{-42,12}},
        lineColor={150,150,150},
        fillPattern=FillPattern.Solid,
        fillColor={150,150,150}),
      Rectangle(
        extent={{-94,48},{-80,62}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-68,48},{-54,62}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-94,26},{-80,40}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-68,26},{-54,40}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Polygon(
        points={{-74,96},{-116,72},{-32,72},{-74,96}},
        lineColor={95,95,95},
        smooth=Smooth.None,
        fillPattern=FillPattern.Solid,
        fillColor={95,95,95}),
        Text(
          extent={{-98,164},{104,120}},
          lineColor={0,0,0},
          textString="%name")}),
                            Diagram(graphics,
                                    coordinateSystem(
        preserveAspectRatio=false,
        extent={{-160,-120},{160,120}},
        initialScale=0.1)),
    Documentation(info="<html>
<p>This distribution system adopts the topology of the IEEE16 standard test system. It has three power feeders that can be connected to the substations.</p>
<p>For now, each node represents a building. The whole distribution system could represent a community.</p>
<p><img src=\"modelica://MultiInfrastructure/Resources/Images/IndividualSystem/Energy/DemandSide/IEEE16.PNG\"/></p>
</html>"),
    __Dymola_Commands);
end DistributionSystemIEEE16;
