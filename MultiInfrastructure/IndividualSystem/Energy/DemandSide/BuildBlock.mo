within MultiInfrastructure.IndividualSystem.Energy.DemandSide;
model BuildBlock
  "Model of building block power demand"
   parameter Modelica.SIunits.Power PBb_nominal
  "Nominal power of building blocks(negative if consumed, positive if generated)";
  parameter Modelica.SIunits.Voltage V_nominal=V_nominal "Nominal voltage";
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive loaBui(
    final mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_P_input,
      V_nominal=V_nominal)
    "Electrical load for the building blocks"
    annotation (Placement(transformation(extent={{10,-10},{-10,10}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_n term_p
    annotation (Placement(transformation(extent={{-100,50},{-120,70}}),
        iconTransformation(extent={{-120,42},{-100,62}})));
  MultiInfrastructure.Buildings.Controls.OBC.CDL.Interfaces.RealInput PBui(
    quantity="Power",
    unit="W",
    max=0)
  "Building power load(negative means consumption, positive means generation)"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Interfaces.RealOutput P(
    quantity="Power",
    unit="W",
    max=0) "Value of Real output"
    annotation (Placement(transformation(extent={{100,-50},{120,-30}})));
equation
  connect(loaBui.terminal, term_p)
    annotation (Line(points={{10,0},{60,0},{60,60},{-110,60}},
                                                color={0,120,120},
      thickness=0.5));
  connect(PBui, loaBui.Pow)
    annotation (Line(points={{-120,0},{-10,0}},color={0,0,127}));
connect(PBui, P) annotation (Line(points={{-120,0},{-40,0},{-40,-40},{110,-40}},
      color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
      Rectangle(
        extent={{-64,34},{64,-74}},
        lineColor={150,150,150},
        fillPattern=FillPattern.Solid,
        fillColor={150,150,150}),
      Polygon(
        points={{0,76},{-78,34},{80,34},{0,76}},
        lineColor={95,95,95},
        smooth=Smooth.None,
        fillPattern=FillPattern.Solid,
        fillColor={95,95,95}),
      Rectangle(
        extent={{16,-8},{44,20}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-42,-8},{-14,20}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-42,-58},{-14,-30}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{16,-58},{44,-30}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name"),
        Rectangle(extent={{-100,100},{100,-100}}, lineColor={0,0,0})}),
                                          Diagram(coordinateSystem(
          preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This model shows the power load from a building block, which depends on the power input.</p>
</html>"));
end BuildBlock;
