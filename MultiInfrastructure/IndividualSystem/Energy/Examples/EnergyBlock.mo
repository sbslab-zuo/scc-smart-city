within MultiInfrastructure.IndividualSystem.Energy.Examples;
model EnergyBlock "Example that demonstrates the energy interaction between multiple blocks"
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";

  MultiInfrastructure.IndividualSystem.Energy.Energy res(
    lat=weaDat.lat,
    PBb_nominal=-1500000,
    PEv_nominal=-1500000,
    PCt_nominal=-7000,
    redeclare
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_500
      commercialCable,
    V_nominal=V_nominal,
    f=f,
    A=100000,
    num=1)    "energy model in the residential block"
    annotation (Placement(transformation(extent={{58,60},{82,80}})));
  MultiInfrastructure.IndividualSystem.Energy.Energy com(
    lat=weaDat.lat,
    PBb_nominal=-2000000,
    PEv_nominal=-1500000,
    PCt_nominal=-80000,
    redeclare
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_500
      commercialCable,
    V_nominal=V_nominal,
    f=f,
    A=80000,
    num=1)   "energy model in the commercial block"
    annotation (Placement(transformation(extent={{58,-60},{82,-40}})));
  Modelica.Blocks.Sources.CombiTimeTable powRes(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-600000; 2,-460000; 4,-380000; 6,-400000; 8,-400000; 10,-300000; 12,
        -200000; 14,-300000; 16,-600000; 18,-800000; 20,-1000000; 22,-800000; 24,
        -600000]) "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-80,60},{-70,70}})));
  Modelica.Blocks.Sources.CombiTimeTable nevRes(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,100; 2,80; 4,60; 6,50; 8,30; 10,8; 12,10; 14,20; 16,30; 18,40; 20,60;
        22,80; 24,100])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-80,40},{-70,50}})));
  Modelica.Blocks.Sources.CombiTimeTable nsRes(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,0; 2,0; 4,0; 6,40; 8,100; 10,80; 12,60; 14,40; 16,30; 18,20; 20,10;
        22,0; 24,0]) "Number of packages sent for a residential building block"
    annotation (Placement(transformation(extent={{-80,20},{-70,30}})));
  Modelica.Blocks.Sources.CombiTimeTable powCom(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-50000; 2,-30000; 4,-50000; 6,-100000; 8,-500000; 10,-800000; 12,-1100000;
        14,-1500000; 16,-1400000; 18,-1000000; 20,-700000; 22,-300000; 24,-50000])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-80,-60},{-70,-50}})));
  Modelica.Blocks.Sources.CombiTimeTable nevCom(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,5; 2,5; 4,5; 6,10; 8,60; 10,80; 12,80; 14,70; 16,60; 18,30; 20,20;
        22,10; 24,5])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-80,-80},{-70,-70}})));
  Modelica.Blocks.Sources.CombiTimeTable nsCom(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,0; 2,0; 4,0; 6,40; 8,100; 10,80; 12,60; 14,40; 16,30; 18,20; 20,10;
        22,0; 24,0]) "Number of packages sent for a residential building block"
    annotation (Placement(transformation(extent={{-80,-100},{-70,-90}})));
  MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
    computeWetBulbTemperature=false, filNam=
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://MultiInfrastructure/Resources/WeatherData/USA_MN_Minneapolis-St.Paul.Intl.AP.726580_TMY3.mos"))
    "Weather data model"
    annotation (Placement(transformation(extent={{-80,80},{-60,100}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=270,
        origin={-70,0})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin1(
    V_nominal=V_nominal,
    l=2000,
    P_nominal=2000000,
    redeclare
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_500
      commercialCable,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.automatic)
                       "Line from or to grid"      annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={0,30})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin2(
    V_nominal=V_nominal,
    redeclare replaceable
  MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_1500
      commercialCable,
    l=2000,
    P_nominal=2000000,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial)
                       "Line from or to grid"      annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={0,-30})));
equation
  connect(nevRes.y[1], res.numEV) annotation (Line(points={{-69.5,45},{-56,45},
          {-56,65},{56,65}},
                        color={0,0,127},
      pattern=LinePattern.Dot));
  connect(weaDat.weaBus, res.weaBus) annotation (Line(
      points={{-60,90},{59,90},{59,79.8}},
      color={255,204,51},
      thickness=0.5));
  connect(weaDat.weaBus, com.weaBus) annotation (Line(
      points={{-60,90},{20,90},{20,-40.2},{59,-40.2}},
      color={255,204,51},
      thickness=0.5));
  connect(lin1.terminal_n, res.term_p) annotation (Line(points={{0,40},{0,70},{
          57,70}},           color={0,120,120},
      thickness=0.5));
  connect(lin1.terminal_p, gri.terminal) annotation (Line(points={{0,20},{0,20},
          {0,-1.77636e-015},{-60,-1.77636e-015}},   color={0,120,120},
      thickness=0.5));
  connect(gri.terminal, lin2.terminal_n) annotation (Line(points={{-60,
          -1.77636e-015},{-60,0},{0,0},{0,-20}},
                             color={0,120,120},
      thickness=0.5));
  connect(lin2.terminal_p, com.term_p) annotation (Line(points={{0,-40},{0,-50},
          {57,-50}},                     color={0,120,120},
      thickness=0.5));
  connect(powCom.y[1], com.PBui) annotation (Line(points={{-69.5,-55},{56,-55},
          {56,-53}},  color={0,0,127},
      pattern=LinePattern.Dot));
  connect(nevCom.y[1], com.numEV) annotation (Line(points={{-69.5,-75},{-54,-75},
          {-54,-55},{56,-55}},           color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powRes.y[1], res.PBui)
    annotation (Line(points={{-69.5,65},{56,65},{56,67}},   color={0,0,127},
      pattern=LinePattern.Dot));
  connect(nsCom.y[1], com.numSenPac[1]) annotation (Line(points={{-69.5,-95},{
          -54,-95},{-54,-57.2},{56,-57.2}},
                               color={0,0,127},
      pattern=LinePattern.Dot));
  connect(nsRes.y[1], res.numSenPac[1]) annotation (Line(
      points={{-69.5,25},{-52,25},{-52,62.8},{56,62.8}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  annotation (Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false, extent={{-100,-120},{100,120}})),
    __Dymola_Commands(file=
          "modelica://MultiInfrastructure/Resources/Scripts/Dymola/IndividualSystem/Energy/Examples/EnergyBlock.mos"
        "Simulate and Plot"),experiment(
      StartTime=0,
      StopTime=86400,
      Tolerance=1e-06),
    Documentation(info="<html>
    <p>This example demonstrates the energy interaction between the two blocks. 
    One is the residential block and the other is the commercial block. 
    The load peak of the residential buildings often occurs in the nocturnal period
    while that of the commercial buildings happens at the working time during the daytime. 
    Therefore, the power could flow from the residential block to the commercial block due to the oversupply. 
    If the amount is insufficient, the grid would compensate the rest of the power demand.</p>
</html>"));
end EnergyBlock;
