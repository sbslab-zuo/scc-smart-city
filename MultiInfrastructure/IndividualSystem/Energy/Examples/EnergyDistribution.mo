within MultiInfrastructure.IndividualSystem.Energy.Examples;
model EnergyDistribution
  "Example that demonstrates the use of Energy.EnergyDistribution"
  import MultiInfrastructure;
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";

  MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
    computeWetBulbTemperature=false, filNam=
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://MultiInfrastructure/Resources/WeatherData/USA_CO_Boulder-Broomfield-Jefferson.County.AP.724699_TMY3.mos"))
    "Weather data model"
    annotation (Placement(transformation(extent={{-60,60},{-40,80}})));
        //"C:/Bitbucket/multiinfrastructure/MultiInfrastructure/Resources/WeatherData/USA_CO_Boulder-Broomfield-Jefferson.County.AP.724699_TMY3.mos")
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-50,40})));
  MultiInfrastructure.IndividualSystem.Energy.EnergyDistribution ene(
    lat=weaDat.lat,
    V_nominal=10000,
    PBb_nominal=-1500000,
    PEv_nominal=-1500000,
    PCt_nominal=-70000,
    num=1,
    n=12,
    f=60,
    redeclare
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_2
      commercialCable,
    thrDis=-1.2e6,
    EMax=2e10,
    SOC_start=0.5,
    A=150000,
    thrCha=-3.5e5)
             "energy model in the block"
    annotation (Placement(transformation(extent={{40,-10},{66,10}})));
  Modelica.Blocks.Sources.CombiTimeTable nev(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,6; 1,5; 2,5; 3,5; 4,4; 5,4; 6,6; 7,8; 8,23; 9,75; 10,131; 11,136;
        12,137; 13,137; 14,139; 15,141; 16,141; 17,51; 18,47; 19,17; 20,13; 21,
        11; 22,9; 23,7; 24,6])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-60,-72},{-50,-62}})));
  Modelica.Blocks.Sources.CombiTimeTable ns(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,41; 1,30; 2,37; 3,40; 4,32; 5,33; 6,46; 7,36; 8,76; 9,89; 10,106;
        11,121; 12,135; 13,126; 14,158; 15,150; 16,161; 17,233; 18,1106; 19,289;
        20,204; 21,113; 22,100; 23,73; 24,41])
    "Number of packages sent for a residential building block"
    annotation (Placement(transformation(extent={{-60,-88},{-50,-78}})));
  Modelica.Blocks.Sources.CombiTimeTable pow3(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-113199.5177; 1,-109803.5388; 2,-102368.0109; 3,-118330.9617; 4,-106788.0383;
        5,-148854.4245; 6,-183652.7374; 7,-232847.716; 8,-247709.1024; 9,-248902.5028;
        10,-256398.0105; 11,-255070.6781; 12,-258375.3686; 13,-248890.5716; 14,
        -255974.4066; 15,-250712.9033; 16,-248438.0369; 17,-256005.5167; 18,-263049.3853;
        19,-174078.2046; 20,-172535.5662; 21,-150404.431; 22,-125633.5674; 23,-95590.35684;
        24,-113199.5177])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-60,-42},{-50,-32}})));
  Modelica.Blocks.Sources.CombiTimeTable pow2(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-39665.66037; 1,-22880.25538; 2,-14648.29608; 3,-14664.78088; 4,-14671.08278;
        5,-14817.98117; 6,-23008.873; 7,-38970.01719; 8,-43260.60256; 9,-42212.49951;
        10,-34726.26651; 11,-42235.95747; 12,-43019.25007; 13,-42320.02807; 14,
        -34790.53907; 15,-34789.96052; 16,-34769.04615; 17,-44296.78038; 18,-46418.70809;
        19,-46421.70322; 20,-39060.35347; 21,-39038.64715; 22,-38966.13298; 23,
        -38856.78773; 24,-39665.66037])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-60,-28},{-50,-18}})));
  Modelica.Blocks.Sources.CombiTimeTable pow1(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-15106.99921; 1,-15106.99921; 2,-15106.99921; 3,-15162.01319; 4,-15190.36283;
        5,-15292.58251; 6,-15288.13499; 7,-22916.16308; 8,-22530.14638; 9,-32344.27609;
        10,-48736.55298; 11,-48736.55298; 12,-48736.55298; 13,-48736.55298; 14,
        -48736.55298; 15,-48736.55298; 16,-48736.55298; 17,-54479.05298; 18,-60221.55298;
        19,-48376.76377; 20,-48376.76377; 21,-43829.27609; 22,-22377.64918; 23,
        -15106.99921; 24,-15106.99921])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-60,-14},{-50,-4}})));
  Modelica.Blocks.Sources.CombiTimeTable pow4(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-46268.5865; 1,-44616.12113; 2,-48249.87495; 3,-44890.35052; 4,-45974.81958;
        5,-48485.49996; 6,-53000.83359; 7,-71074.69613; 8,-78345.33465; 9,-97123.15792;
        10,-81180.84723; 11,-56799.73352; 12,-53500.92217; 13,-52553.03076; 14,
        -51871.65923; 15,-51188.67417; 16,-50625.71801; 17,-58819.71068; 18,-71170.73161;
        19,-85193.82708; 20,-85850.21377; 21,-91994.72379; 22,-89762.32297; 23,
        -67564.07879; 24,-46268.5865])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-60,-56},{-50,-46}})));
equation
  connect(ene.weaBus, weaDat.weaBus) annotation (Line(
      points={{39,9.8},{0,9.8},{0,70},{-40,70}},
      color={255,204,51},
      thickness=0.5));
  connect(ene.term_p, gri.terminal) annotation (Line(
      points={{39,3},{-50,3},{-50,30}},
      color={0,120,120},
      thickness=0.5));
  connect(nev.y[1], ene.numEV) annotation (Line(points={{-49.5,-67},{24,-67},{24,
          -4.6},{38,-4.6}},
                        color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ns.y[1], ene.numSenPac[1]) annotation (Line(
      points={{-49.5,-83},{30,-83},{30,-8.6},{38,-8.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow1.y[1],ene. PBui[10]) annotation (Line(
      points={{-49.5,-9},{-2,-9},{-2,0.566667},{38,0.566667}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow1.y[1],ene. PBui[11]) annotation (Line(
      points={{-49.5,-9},{-2,-9},{-2,0.9},{38,0.9}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow1.y[1],ene. PBui[12]) annotation (Line(
      points={{-49.5,-9},{-2,-9},{-2,1.23333},{38,1.23333}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow1.y[1],ene. PBui[1]) annotation (Line(
      points={{-49.5,-9},{-2,-9},{-2,-2.43333},{38,-2.43333}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow2.y[1],ene. PBui[2]) annotation (Line(
      points={{-49.5,-23},{-2,-23},{-2,-2.1},{38,-2.1}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow2.y[1],ene. PBui[3]) annotation (Line(
      points={{-49.5,-23},{-2,-23},{-2,-1.76667},{38,-1.76667}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow2.y[1],ene. PBui[6]) annotation (Line(
      points={{-49.5,-23},{-2,-23},{-2,-0.766667},{38,-0.766667}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow2.y[1],ene. PBui[7]) annotation (Line(
      points={{-49.5,-23},{-2,-23},{-2,-0.433333},{38,-0.433333}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow2.y[1],ene. PBui[8]) annotation (Line(
      points={{-49.5,-23},{-2,-23},{-2,-0.1},{38,-0.1}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow3.y[1],ene. PBui[5]) annotation (Line(
      points={{-49.5,-37},{-2,-37},{-2,-1.1},{38,-1.1}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow4.y[1],ene. PBui[4]) annotation (Line(
      points={{-49.5,-51},{-2,-51},{-2,-1.43333},{38,-1.43333}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow4.y[1],ene. PBui[9]) annotation (Line(
      points={{-49.5,-51},{-2,-51},{-2,0.233333},{38,0.233333}},
      color={0,0,127},
      pattern=LinePattern.Dot));

  annotation (
    Documentation(info="<html>
    <p>This example shows how to use an energy model in a residential block. If the
    supply renewable energy is insufficient, the grid will provide the power to the system.</p>
</html>"),
      experiment(
      StartTime=0,
      StopTime=86400,
      Tolerance=1e-06),
    __Dymola_Commands(file=
          "modelica://MultiInfrastructure/Resources/Scripts/Dymola/IndividualSystem/Energy/Examples/EnergyDistribution.mos"
        "Simulate and Plot"));
end EnergyDistribution;
