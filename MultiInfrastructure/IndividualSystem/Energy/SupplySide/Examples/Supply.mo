within MultiInfrastructure.IndividualSystem.Energy.SupplySide.Examples;
model Supply "Example that demonstrates the use of Energy.SupplySide"
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";

  MultiInfrastructure.IndividualSystem.Energy.SupplySide.Supply sup(
    lat=weaDat.lat,
    A=4000,
    PV_nominal=100000,
    PWin_nominal=50000,
    VHigh=10000,
    Zperc=0.03,
    VLow=480,
    XoverR=5,
    VABase=-100000,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable) "Energy supply side model"
    annotation (Placement(transformation(extent={{20,40},{40,60}})));
  MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
    computeWetBulbTemperature=false,
    filNam="modelica://MultiInfrastructure/Buildings/Resources/weatherdata/USA_CA_San.Francisco.Intl.AP.724940_TMY3.mos")
    "Weather data model"
    annotation (Placement(transformation(extent={{-80,60},{-60,80}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-70,30})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive
    loa(
    mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_y_input,
    V_nominal=V_nominal,
    linearized=false,
    use_pf_in=false,
    pf=0.8,
    P_nominal=-100000)
                      "Electrical load"
    annotation (Placement(transformation(extent={{18,-42},{42,-18}})));
  Modelica.Blocks.Sources.CombiTimeTable pow(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,0.05; 2,0.075; 4,0.1; 6,0.1; 8,0.1; 10,0.3; 12,0.4; 14,0.7; 16,0.5; 18,
        0.5; 20,0.4; 22,0.2; 24,0.05]) "Power consumption profile for load "
    annotation (Placement(transformation(extent={{80,-40},{60,-20}})));

equation
  connect(weaDat.weaBus, sup.weaBus) annotation (Line(
      points={{-60,70},{19,70},{19,58.4}},
      color={255,204,51},
      thickness=0.5));
  connect(sup.term_p, loa.terminal) annotation (Line(points={{19,50.2},{0,50.2},
          {0,-30},{18,-30}}, color={0,120,120},
      thickness=0.5));
  connect(sup.term_p, gri.terminal) annotation (Line(points={{19,50.2},{0,50.2},
          {0,0},{-70,0},{-70,20}},   color={0,120,120},
      thickness=0.5));
  connect(loa.y, pow.y[1])
    annotation (Line(points={{42,-30},{59,-30}},          color={0,0,127},
      pattern=LinePattern.Dot));
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
    <p>This example shows how to use an energy supply side model. The power generated 
    by the supply side model is consumed by a hypothetical load. If the supply energy is insufficient, 
    the grid will provide the power to the system. The power losses during transmission is neglected.</p>
</html>"),
experiment(
      StartTime=0,
      StopTime=864000,
      Tolerance=1e-06),
    __Dymola_Commands(file=
          "modelica://MultiInfrastructure/Resources/Scripts/Dymola/IndividualSystem/Energy/SupplySide/Examples/Supply.mos"
        "Simulate and Plot"));
end Supply;
