within MultiInfrastructure.IndividualSystem.Energy.SupplySide;
model Supply "Model of energy generation"

  parameter Modelica.SIunits.Area A "Net surface area of PV"
    annotation(Dialog(tab="PV",group="Parameters"));
  parameter Modelica.SIunits.Power PV_nominal
    "Nominal power of PV panels (negative if consumed, positive if generated)"
    annotation(Dialog(tab="PV",group="Parameters"));
  parameter Modelica.SIunits.Voltage VPV_nominal=480
    "Nominal voltage of PV (VPV_nominal >= 0)";
  parameter Real fAct=0.9 "Fraction of surface area with active solar cells"
    annotation(Dialog(tab="PV",group="Parameters"));
  parameter Real eta=0.12 "Module conversion efficiency"
    annotation(Dialog(tab="PV",group="Parameters"));
  parameter Boolean linearized=false "If =true, linearize the load"
    annotation(Dialog(tab="PV",group="Parameters"));
  parameter Modelica.SIunits.Angle til=30 "Surface tilt"
    annotation(Dialog(tab="PV",group="Orientation"));
  parameter Modelica.SIunits.Angle lat "Latitude"
    annotation(Dialog(tab="PV",group="Orientation"));
  parameter Modelica.SIunits.Angle azi=MultiInfrastructure.Buildings.Types.Azimuth.S
    "Surface azimuth"
    annotation(Dialog(tab="PV",group="Orientation"));
  parameter Real pfPV=0.9 "Power factor in PVs"
    annotation(Dialog(tab="PV",group="AC-Conversion"));
  parameter Real etaPV_DCAC=0.9 "Efficiency of DC/AC conversion in PVs"
    annotation(Dialog(tab="PV",group="AC-Conversion"));
  parameter Modelica.SIunits.Voltage VWin_nominal=480
    "Nominal voltage of wind turbine (VWin_nominal >= 0)";
  parameter Modelica.SIunits.Power PWin_nominal
    "Nominal power pf the wind turbine (negative if consumed, positive if generated)"
    annotation(Dialog(tab="Wind turbine",group="Parameters"));
  parameter Real pfWin=0.9 "Power factor in wind turbines"
    annotation(Dialog(tab="Wind turbine",group="AC-Conversion"));
  parameter Boolean tableOnFile=false
    "true, if table is defined on file or in function usertab"
    annotation(Dialog(tab="Wind turbine",group="Parameters"));
  parameter String tableName="NoName"
    "Table name on file or in function usertab (see documentation)"
    annotation(Dialog(tab="Wind turbine",group="Parameters"));
  parameter String fileName="NoName" "File where matrix is stored"
    annotation(Dialog(tab="Wind turbine",group="Parameters"));
  parameter Real etaWin_DCAC=0.9 "Efficiency of DC/AC conversion in wind turbines"
    annotation(Dialog(tab="Wind turbine",group="AC-Conversion"));
  parameter Modelica.SIunits.Height h=10 "Height over ground"
    annotation(Dialog(tab="Wind turbine",group="Wind correction"));
  parameter Modelica.SIunits.Height hRef=15
    "Reference height for wind measurement"
    annotation(Dialog(tab="Wind turbine",group="Wind correction"));
  parameter Real nWin=0.4 "Height exponent for wind profile calculation"
    annotation(Dialog(tab="Wind turbine",group="Wind correction"));
  parameter Modelica.SIunits.Voltage V_nominal=10000
    "Nominal voltage of line (V_nominal >= 0)";
  parameter Modelica.SIunits.Length l=1500 "Length of the supply main line"
    annotation(Dialog(tab="Line",group="parameters"));
  parameter Modelica.SIunits.Length l1=300 "Length of the supply line1"
    annotation(Dialog(tab="Line",group="parameters"));
  parameter Modelica.SIunits.Length l2=300 "Length of the supply line2"
    annotation(Dialog(tab="Line",group="parameters"));
  parameter Boolean use_C=false
    "Set to true to add a capacitance in the center of the line"
    annotation (Dialog(tab="Line",group="Assumptions"));

  parameter Real table[:,2]=[2.5,0; 5.5,0.1; 12,0.9; 14,1; 25,1]
    "Table of generated power (first column is wind speed, second column is power)"
    annotation (Dialog(tab="Wind turbine",group="Parameters"));
  replaceable parameter
    MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic commercialCable
    constrainedby
    MultiInfrastructure.Buildings.Electrical.Transmission.BaseClasses.BaseCable
    "Commercial cables options"
    annotation (
    Evaluate=true,Dialog(
    tab="Line",group="Manual mode",
    enable=mode == MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial),
    choicesAllMatching=true);
  parameter Modelica.SIunits.ApparentPower VABase
    "Nominal power of the transformer"
    annotation (Dialog(tab="Transformer"));
  parameter Modelica.SIunits.Voltage VHigh
    "Rms voltage on side 1 of the transformer (primary side)"
    annotation (Dialog(tab="Transformer"));
  parameter Modelica.SIunits.Voltage VLow
    "Rms voltage on side 2 of the transformer (secondary side)"
    annotation (Dialog(tab="Transformer"));
  parameter Real XoverR
    "Ratio between the complex and real components of the impedance (XL/R)"
    annotation (Dialog(tab="Transformer"));
  parameter Real Zperc "Short circuit impedance"
    annotation (Dialog(tab="Transformer"));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Sources.PVSimpleOriented
    PV(
    A=A,
    pf=pfPV,
    fAct=fAct,
    eta=eta,
    til=til,
    azi=azi,
    linearized=linearized,
    eta_DCAC=etaPV_DCAC,
    lat=lat,
    V_nominal=VPV_nominal) "PV"
    annotation (Placement(transformation(extent={{60,-80},{80,-60}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Sources.WindTurbine winTur(
    h=h,
    hRef=hRef,
    nWin=nWin,
    pf=pfWin,
    eta_DCAC=etaWin_DCAC,
    table=table,
    tableOnFile=tableOnFile,
    tableName=tableName,
    fileName=fileName,
    scale=PWin_nominal,
    V_nominal=VWin_nominal)
                "Wind turbine model"
    annotation (Placement(transformation(extent={{60,40},{80,60}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_p term_p
    "The electricity terminal to demand" annotation (Placement(transformation(
          extent={{-120,10},{-100,-10}}),iconTransformation(extent={{-120,12},{-100,
            -8}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin(
    l=l,
    P_nominal=PV_nominal + PWin_nominal,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare replaceable MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic commercialCable=commercialCable,
    V_nominal=V_nominal)                                                                                                                         "Main supply line"
    annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin1(
    P_nominal=PV_nominal,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    l=l1,
    redeclare replaceable MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic commercialCable=commercialCable,
    V_nominal=V_nominal)  "Line to the PV" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={0,-30})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin2(
    P_nominal=PWin_nominal,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    l=l2,
    redeclare replaceable MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic commercialCable=commercialCable,
    V_nominal=V_nominal)    "Line to the Wind Turbine" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,20})));

  Buildings.BoundaryConditions.WeatherData.Bus weaBus "Weather data bus"
    annotation (Placement(transformation(extent={{-130,60},{-90,100}}),
        iconTransformation(extent={{-120,74},{-100,94}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Conversion.ACACTransformer traWin(
    VHigh=VHigh,
    VLow=VLow,
    VABase=VABase,
    XoverR=XoverR,
    Zperc=Zperc)
    annotation (Placement(transformation(extent={{18,40},{38,60}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Conversion.ACACTransformer traPV(
    VHigh=VHigh,
    VLow=VLow,
    VABase=VABase,
    XoverR=XoverR,
    Zperc=Zperc)
    annotation (Placement(transformation(extent={{22,-80},{42,-60}})));
  Modelica.Blocks.Sources.RealExpression realExpression
    annotation (Placement(transformation(extent={{-164,42},{-144,62}})));
  Modelica.Blocks.Sources.RealExpression PSup(y=winTur.P*etaWin_DCAC +
        etaPV_DCAC*PV.P)
    annotation (Placement(transformation(extent={{60,-10},{80,10}})));
  Modelica.Blocks.Interfaces.RealOutput powSup "Power supply"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
equation
  connect(lin.terminal_n, term_p)
    annotation (Line(points={{-40,0},{-40,0},{-110,0}},
                                                color={0,120,120},
      thickness=0.5));
  connect(lin1.terminal_n, lin.terminal_p) annotation (Line(points={{
          1.77636e-015,-20},{0,-20},{0,0},{-20,0}},
                               color={0,120,120},
      thickness=0.5));
  connect(lin2.terminal_n, lin.terminal_p) annotation (Line(points={{
          -6.66134e-016,10},{0,10},{0,0},{-20,0}},
                           color={0,120,120},
      thickness=0.5));
  connect(winTur.vWin, weaBus.winSpe) annotation (Line(
      points={{70,62},{70,62},{70,80},{-110,80}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(PV.weaBus, weaBus) annotation (Line(
      points={{70,-61},{68,-61},{68,-20},{-60,-20},{-60,80},{-110,80}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(traWin.terminal_p, winTur.terminal)
    annotation (Line(points={{38,50},{60,50}}, color={0,120,120},
      thickness=0.5));
  connect(lin2.terminal_p, traWin.terminal_n)
    annotation (Line(points={{0,30},{0,50},{18,50}}, color={0,120,120},
      thickness=0.5));
  connect(traPV.terminal_p, PV.terminal)
    annotation (Line(points={{42,-70},{60,-70}}, color={0,120,120}));
  connect(traPV.terminal_n, lin1.terminal_p) annotation (Line(points={{22,-70},
          {-1.77636e-015,-70},{-1.77636e-015,-40}}, color={0,120,120}));
  connect(PSup.y, powSup)
    annotation (Line(points={{81,0},{110,0}}, color={0,0,127}));
  connect(powSup, powSup)
    annotation (Line(points={{110,0},{110,0}}, color={0,0,127}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),                                    graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,0},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{42,56},{88,90},{38,62},{42,56}},
          smooth=Smooth.None,
          fillColor={222,222,222},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Rectangle(
          extent={{38,58},{42,-40}},
          fillColor={233,233,233},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Rectangle(
          extent={{-48,44},{-44,-54}},
          fillColor={233,233,233},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Polygon(
          points={{-66,-88},{-18,27},{92,27},{43,-88},{-66,-88}},
          smooth=Smooth.None,
          fillColor={205,203,203},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Polygon(
          points={{-24,-9},{-12,17},{11,17},{0,-9},{-24,-9}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{9,-9},{21,17},{44,17},{33,-9},{9,-9}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{42,-9},{54,17},{77,17},{66,-9},{42,-9}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-39,-45},{-27,-19},{-4,-19},{-15,-45},{-39,-45}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-6,-45},{6,-19},{29,-19},{18,-45},{-6,-45}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{27,-45},{39,-19},{62,-19},{51,-45},{27,-45}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-55,-81},{-43,-55},{-20,-55},{-31,-81},{-55,-81}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-22,-81},{-10,-55},{13,-55},{2,-81},{-22,-81}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{11,-81},{23,-55},{46,-55},{35,-81},{11,-81}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-48,42},{-96,70},{-44,48},{-48,42}},
          smooth=Smooth.None,
          fillColor={222,222,222},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Polygon(
          points={{-44,42},{2,76},{-48,48},{-44,42}},
          smooth=Smooth.None,
          fillColor={222,222,222},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Polygon(
          points={{36,56},{54,4},{42,60},{36,56}},
          smooth=Smooth.None,
          fillColor={222,222,222},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Polygon(
          points={{-50,42},{-32,-10},{-44,46},{-50,42}},
          smooth=Smooth.None,
          fillColor={222,222,222},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Ellipse(
          extent={{-52,50},{-40,38}},
          lineColor={0,0,0},
          fillColor={222,222,222},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{38,56},{-10,84},{42,62},{38,56}},
          smooth=Smooth.None,
          fillColor={222,222,222},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Ellipse(
          extent={{34,64},{46,52}},
          lineColor={0,0,0},
          fillColor={222,222,222},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name")}),            Diagram(graphics,
                                                    coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}})),
    Documentation(info="<html>
<p>This model shows the energy supply from the PV panels and wind turbines. The power generated by the PV panels and wind turbines is transmitted to the grid through converter and the transformer. If the supply is not sufficient, the grid will provide the power through the terminal. The power is transmitted through the line model which accounts for the power losses. </p>
<p><img src=\"modelica://MultiInfrastructure/Resources/Images/IndividualSystem/Energy/SupplySide/Supply.png\"/></p>
</html>"));
end Supply;
