within MultiInfrastructure.IndividualSystem.Communication;
package Examples "Examples that demonstrate the use of models in Communication"
  extends Modelica.Icons.ExamplesPackage;


annotation (Documentation(info="<html>
<p>Examples to demonstrate the usage of communication models.</p>
</html>"));
end Examples;
