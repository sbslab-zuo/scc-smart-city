within MultiInfrastructure.IndividualSystem;
package Communication "Collection of models that describes the communication system"

annotation (Documentation(info="<html>
<P>
This package contains models for communication system.
</P>
</html>"));
end Communication;
