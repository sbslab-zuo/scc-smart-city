within MultiInfrastructure.UsersGuide.ReleaseNotes;
class Version_1_0_0 "Version 1.0.0"
  extends Modelica.Icons.ReleaseNotes;
    annotation (Documentation(info="<html>
<p>First release of the library. It contains component and system models supporting city-scale interdependent energy, transportation, and communication system modeling.</p>
</html>"));
end Version_1_0_0;
