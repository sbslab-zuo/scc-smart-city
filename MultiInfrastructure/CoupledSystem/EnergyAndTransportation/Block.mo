within MultiInfrastructure.CoupledSystem.EnergyAndTransportation;
model Block "Model that connects the energy and transportation system"
  parameter Modelica.SIunits.Angle lat "Latitude"
  annotation(Dialog(tab="Energy",group="Parameters"));
  parameter
    MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.Types.TrafficFlow
    qb_nominal "Nominal capacity at the road linked with port_b"
    annotation (Dialog(tab="Transportation", group="Parameters"));
  parameter Modelica.SIunits.Time Ub_nominal
    "Reference travel cost at the free flow road linked with port_b"
    annotation(Dialog(tab="Transportation",group="Parameters"));
  parameter Real numEV "Number of charging EV in the block"
    annotation(Dialog(tab="Transportation",group="Parameters"));
  parameter Modelica.SIunits.Frequency f=60 "Nominal grid frequency"
    annotation(Dialog(tab="Energy",group="Parameters"));
  parameter Modelica.SIunits.Voltage V_nominal=V_nominal
    "Nominal voltage(V_nominal>=0)"
    annotation(Dialog(tab="Energy",group="Parameters"));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.TwoPortBlock
    tra(
    qb_nominal=qb_nominal,
    Ub_nominal=Ub_nominal,
    numEV(fixed=true, start=numEV))
    annotation (Placement(transformation(extent={{20,-44},{40,-24}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.TrafficPort_a
    port_a1
    "Traffic connector a (positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(extent={{-114,-90},{-94,-70}})));
  Modelica.Blocks.Interfaces.RealInput qb "Traffic outflow at the port_b"
    annotation (Placement(transformation(extent={{-140,-70},{-100,-30}}),
        iconTransformation(extent={{-140,-70},{-100,-30}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.TrafficPort_b
    port_b1
    "Traffic connector b (positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(extent={{90,-90},{110,-70}})));
  MultiInfrastructure.Buildings.Controls.OBC.CDL.Interfaces.RealInput PBui "Building power load"
    annotation (Placement(transformation(extent={{-140,-10},{-100,30}}),
        iconTransformation(extent={{-140,-10},{-100,30}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_p term_p
    annotation (Placement(transformation(extent={{-120,52},{-100,72}}),
        iconTransformation(extent={{-120,52},{-100,72}})));
  MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.Bus weaBus annotation (Placement(
        transformation(extent={{-120,76},{-100,96}}), iconTransformation(extent=
           {{-120,76},{-100,96}})));

  Modelica.Blocks.Interfaces.RealInput numSenPac[1] "Number of packages sent"
    annotation (Placement(transformation(extent={{-140,-40},{-100,0}})));
  MultiInfrastructure.CoupledSystem.EnergyAndTransportation.BaseClasses.Energy ene(
    V_nominal=V_nominal,
    f=f,
    lat=lat,
    redeclare
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_500
      commercialCable,
    A=600000,
    num=num)
    annotation (Placement(transformation(extent={{32,20},{52,40}})));
  parameter Integer num=1 "Number of communication sources in the demand side"
    annotation(Dialog(tab="Energy",group="Parameters"));
equation
  connect(tra.port_a, port_a1) annotation (Line(points={{20,-34},{-80,-34},{-80,
          -80},{-104,-80}}, color={0,127,255}));
  connect(tra.port_b, port_b1) annotation (Line(points={{40,-34},{80,-34},{80,-80},
          {100,-80}}, color={0,127,255}));
  connect(term_p, term_p) annotation (Line(points={{-110,62},{-110,62},{-110,62},
          {-110,62}}, color={0,120,120}));
  connect(qb, tra.qb) annotation (Line(points={{-120,-50},{-88,-50},{-88,-31},{18,
          -31}}, color={0,0,127}));
  connect(ene.weaBus, weaBus) annotation (Line(
      points={{31,39.8},{0,39.8},{0,86},{-110,86}},
      color={255,204,51},
      thickness=0.5));
  connect(ene.term_p, term_p) annotation (Line(points={{31,30},{-20,30},{-20,62},
          {-110,62}}, color={0,120,120}));
  connect(ene.PBui, PBui) annotation (Line(points={{30,27},{-20,27},{-20,10},{
          -120,10}}, color={0,0,127}));
  connect(tra.numEVCha, ene.numEV) annotation (Line(points={{41,-42},{52,-42},{
          52,-12},{-8,-12},{-8,25},{30,25}}, color={0,0,127}));
  connect(numSenPac, ene.numSenPac) annotation (Line(points={{-120,-20},{-16,
          -20},{-16,22.6},{30,22.6}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-74,-20},{76,-64}},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{12,11},{-12,-11}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          radius=10,
          origin={61,-20},
          rotation=90),
        Rectangle(
          extent={{12,11},{-12,-11}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          radius=10,
          origin={61,16},
          rotation=90),
        Rectangle(extent={{-100,100},{100,-100}}, lineColor={0,0,0}),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name"),
        Line(points={{-58,22},{-32,22}},   color={0,0,0}),
        Line(points={{-46,22},{-46,-8}},    color={0,0,0}),
        Line(points={{-58,22},{-58,20}},   color={0,0,0}),
        Line(points={{-32,22},{-32,20}},   color={0,0,0}),
        Line(
          points={{-68,-8},{-66,-6},{-62,-2},{-58,12},{-58,20}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-68,-8},{-56,-6},{-50,-4},{-34,8},{-32,20}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(points={{-22,32},{4,32}},     color={0,0,0}),
        Line(points={{-22,32},{-22,30}},   color={0,0,0}),
        Line(points={{4,32},{4,30}},       color={0,0,0}),
        Line(
          points={{-58,20},{-42,24},{-32,26},{-26,28},{-22,30}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-32,20},{-20,22},{-10,24},{-2,26},{4,30}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-22,30},{-6,34},{4,36},{10,38},{14,40}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{4,30},{16,32},{26,34},{34,36},{40,40}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(points={{14,42},{40,42}},   color={0,0,0}),
        Line(points={{-10,32},{-10,2}},    color={0,0,0}),
        Line(points={{26,42},{26,10}},   color={0,0,0}),
        Line(points={{14,42},{14,40}},     color={0,0,0}),
        Line(points={{40,42},{40,40}},     color={0,0,0}),
        Text(
          extent={{-70,94},{68,48}},
          lineColor={28,108,200},
          pattern=LinePattern.Dash,
          lineThickness=0.5,
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid,
          textString="E+T"),
        Rectangle(
          extent={{50,20},{72,-16}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{56,20},{66,10}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={238,46,47},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{56,2},{66,-8}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{56,-14},{66,-24}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid), Line(
          points={{-74,-42},{76,-42}},
          color={255,255,255},
          pattern=LinePattern.Dash,
          thickness=0.5)}),                                      Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This model shows the charging stations and charging stations in the block.The EV charging load is calculated from the transportation system.</p>
</html>"));
end Block;
