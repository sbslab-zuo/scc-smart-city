within MultiInfrastructure.CoupledSystem.EnergyAndTransportation.BaseClasses;
model Road "Model of a road section linked to different blocks"
  extends
    MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.PartialTwoPort;
  parameter
    MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.Types.TrafficFlow
    q_nominal "Nominal capacity at the road";
  parameter Modelica.SIunits.Time U_nominal=1000 "Reference travel cost at the free flow";
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.Types.TrafficImpedance
    R "Traffic impedance of the road";

equation
  port_b.U=0;
  R=0.15*U_nominal/(q_nominal)^5;
  port_b.q^5=(port_a.U-port_b.U)/R;
  annotation (Icon(graphics={Rectangle(
          extent={{-100,20},{100,-20}},
          pattern=LinePattern.None,
          lineThickness=1,
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid), Line(
          points={{-100,0},{100,0}},
          color={255,255,255},
          pattern=LinePattern.Dash,
          thickness=0.5),
        Text(
          extent={{-140,80},{144,28}},
          lineColor={0,0,255},
          textString="%name")}), Documentation(info="<html>
<p>This road model links different two blocks together. The principle is similar to the electric circuit calculation. The traffic cost is corresponding to the voltage in the electric circuit theory which refers to the travel time while the traffic flow is corresponding to the current and the travel impedance is similar to the resistance.</p>
<p>For a certain road, the larger the traffic impedance is, the more travel cost it takes. The more traffic flow, the more travel cost it takes. When the traffic flows to the terminal, the traffic cost declines through the road based on the traffic impedance, just like the voltage-resitance relationship. </p>
<p>In this road model, we use the following relationship </p>
<p align=\"center\"><i>U=Rq<sup>b </i></sup></p>
<p>which is derived from the equation developed from the Bureau of Public Roads (BPR) </p>
<p align=\"center\"><i>U(x)=U<sub>0</sub>[1+a(x &frasl; C)<sup>b</sup>] </i></p>
<p>where <i>a</i>, <i>b</i> is coefficient. Usually <i>a=0.15</i>, <i>b=4</i> or <i>5</i>, <i>U</i> is the travel time on the road, <i>U<sub>0 </i></sub>is the free flow travel time on the linked road per unit of time, <i>q</i> is the volume of traffic on the road per unit of time, <i>C</i> is the capacity of the road per unit of time. </p>
</html>"));
end Road;
