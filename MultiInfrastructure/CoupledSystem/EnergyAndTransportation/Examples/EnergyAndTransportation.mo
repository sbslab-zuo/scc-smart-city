within MultiInfrastructure.CoupledSystem.EnergyAndTransportation.Examples;
model EnergyAndTransportation
  "Example that demonstrates the use of models in MultiInfrastructure.CoupledSystem.EnergyAndTransportation"
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";
  MultiInfrastructure.CoupledSystem.EnergyAndTransportation.Block resBlo(
  lat=weaDat.lat,
  qb_nominal=1200,
  Ub_nominal=1000,
  numEV=600,
  f=f,
  V_nominal=10000)
  annotation (Placement(transformation(extent={{-10,26},{10,46}})));
  MultiInfrastructure.CoupledSystem.EnergyAndTransportation.Block comBlo(
  lat=weaDat.lat,
  qb_nominal=600,
  Ub_nominal=800,
  numEV=200,
  f=f,
  V_nominal=10000)
  annotation (Placement(transformation(extent={{10,-34},{-10,-14}})));
  MultiInfrastructure.CoupledSystem.EnergyAndTransportation.BaseClasses.Road roa1(
   q_nominal=600, U_nominal=800)        annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-20,0})));
  MultiInfrastructure.CoupledSystem.EnergyAndTransportation.BaseClasses.Road roa2(
   q_nominal=1200, U_nominal=1000)      annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={20,0})));
  Modelica.Blocks.Sources.CombiTimeTable qRes(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,5; 1,7; 2,2; 3,3; 4,8; 5,10; 6,80; 7,100; 8,120; 9,80; 10,50; 11,30;
        12,20; 13,20; 14,30; 15,30; 16,20; 17,30; 18,30; 19,20; 20,30; 21,10; 22,
        5; 23,0; 24,5])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,26},{-90,36}})));
  Modelica.Blocks.Sources.CombiTimeTable qCom(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,5; 1,0; 2,0; 3,0; 4,8; 5,10; 6,20; 7,20; 8,30; 9,30; 10,15; 11,30;
        12,20; 13,20; 14,30; 15,30; 16,50; 17,98; 18,119; 19,80; 20,60; 21,40; 22,
        10; 23,15; 24,5])
    "Number of EV charging profile for a commercial building block"
    annotation (Placement(transformation(extent={{90,-80},{80,-70}})));
  MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
    computeWetBulbTemperature=false,
    filNam="modelica://MultiInfrastructure/Resources/WeatherData/USA_MN_Minneapolis-St.Paul.Intl.AP.726580_TMY3.mos")
    "Weather data model"
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
  Modelica.Blocks.Sources.CombiTimeTable powRes(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-600000; 2,-460000; 4,-380000; 6,-400000; 8,-400000; 10,-300000; 12,
        -200000; 14,-300000; 16,-600000; 18,-800000; 20,-1000000; 22,-800000; 24,
        -600000]) "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,58},{-90,68}})));
  Modelica.Blocks.Sources.CombiTimeTable numPacRes(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,0; 2,0; 4,0; 6,40; 8,100; 10,80; 12,60; 14,40; 16,30; 18,20; 20,10;
        22,0; 24,0]) "Number of packages sent for a residential building block"
    annotation (Placement(transformation(extent={{-100,42},{-90,52}})));
 MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=270,
        origin={-90,2})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin1(
    V_nominal=V_nominal,
    l=2000,
    P_nominal=2000000,
    redeclare
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_500
      commercialCable,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.automatic)
                       "Line from or to grid"      annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-40,20})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin2(
    V_nominal=V_nominal,
    redeclare replaceable
  MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_1500
      commercialCable,
    l=2000,
    P_nominal=2000000,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial)
                       "Line from or to grid"      annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-40,-20})));
  Modelica.Blocks.Sources.CombiTimeTable powCom(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-50000; 2,-30000; 4,-50000; 6,-100000; 8,-500000; 10,-800000; 12,-1100000;
        14,-1500000; 16,-1400000; 18,-1000000; 20,-700000; 22,-300000; 24,-50000])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{90,-28},{80,-18}})));
  Modelica.Blocks.Sources.CombiTimeTable numPacCom(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,0; 2,0; 4,0; 6,40; 8,100; 10,80; 12,60; 14,40; 16,30; 18,20; 20,10;
        22,0; 24,0]) "Number of packages sent for a residential building block"
    annotation (Placement(transformation(extent={{90,-52},{80,-42}})));
equation
  connect(resBlo.port_b1, roa2.port_a) annotation (Line(
      points={{10,28},{20,28},{20,10}},
      color={0,127,255},
      thickness=1));
  connect(roa2.port_b, comBlo.port_a1) annotation (Line(
      points={{20,-10},{20,-32},{10.4,-32}},
      color={0,127,255},
      thickness=1));
  connect(comBlo.port_b1, roa1.port_a) annotation (Line(
      points={{-10,-32},{-20,-32},{-20,-10}},
      color={0,127,255},
      thickness=1));
  connect(roa1.port_b, resBlo.port_a1) annotation (Line(
      points={{-20,10},{-20,28},{-10.4,28}},
      color={0,127,255},
      thickness=1));
  connect(qRes.y[1], resBlo.qb) annotation (Line(
      points={{-89.5,31},{-12,31}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qCom.y[1], comBlo.qb) annotation (Line(
      points={{79.5,-75},{40,-75},{40,-29},{12,-29}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(weaDat.weaBus, resBlo.weaBus) annotation (Line(
      points={{-80,90},{-20,90},{-20,44.6},{-11,44.6}},
      color={255,204,51},
      thickness=0.5));
  connect(gri.terminal, lin1.terminal_p)
    annotation (Line(points={{-80,2},{-40,2},{-40,10}}, color={0,120,120},
    thickness=0.5));
  connect(gri.terminal, lin2.terminal_n) annotation (Line(
      points={{-80,2},{-40,2},{-40,-10}},
      color={0,120,120},
      thickness=0.5));
  connect(lin1.terminal_n, resBlo.term_p) annotation (Line(
      points={{-40,30},{-40,42},{-11,42},{-11,42.2}},
      color={0,120,120},
      thickness=0.5));
  connect(lin2.terminal_p, comBlo.term_p) annotation (Line(
      points={{-40,-30},{-40,-60},{30,-60},{30,-17.8},{11,-17.8}},
      color={0,120,120},
      thickness=0.5));
  connect(weaDat.weaBus, comBlo.weaBus) annotation (Line(
      points={{-80,90},{60,90},{60,-15.4},{11,-15.4}},
      color={255,204,51},
      thickness=0.5));
  connect(powRes.y[1], resBlo.PBui) annotation (Line(
      points={{-89.5,63},{-30,63},{-30,37},{-12,37}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(numPacRes.y[1], resBlo.numSenPac[1]) annotation (Line(
      points={{-89.5,47},{-32,47},{-32,34},{-12,34}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(comBlo.PBui, powCom.y[1]) annotation (Line(
      points={{12,-23},{79.5,-23}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(numPacCom.y[1], comBlo.numSenPac[1]) annotation (Line(
      points={{79.5,-47},{44,-47},{44,-26},{12,-26}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)),
  Documentation(info="<html>
  <p>This example demonstrates the interaction between the energy system and transportation system.
  The EV charging power consumption is calculated from the transportation system,
  which is related to the traffic condition between the blocks. 
  The results from this example show both energy consumption profile and the traffic conditions.</p>
</html>"),
  __Dymola_Commands(file=
        "Resources/Scripts/Dymola/CoupledSystem/EnergyAndTransportation/Examples/EnergyAndTransportation.mos"
      "Simulate and Plot"));
end EnergyAndTransportation;
