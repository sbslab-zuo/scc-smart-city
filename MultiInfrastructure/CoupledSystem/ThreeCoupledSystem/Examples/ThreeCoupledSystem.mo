within MultiInfrastructure.CoupledSystem.ThreeCoupledSystem.Examples;
model ThreeCoupledSystem "Example that demonstrates the use of models in 
  MultiInfrastructure.CoupledSystem.EnergyAndCommunication"
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystem.Block resBlo(
    qb_nominal=1200,
    Ub_nominal=1000,
    numEV=600,
    V_nominal=V_nominal,
    f=f,
    lat=weaDat.lat,
    num=1)     annotation (Placement(transformation(extent={{-8,40},{12,60}})));
  MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
    computeWetBulbTemperature=false,
    filNam="modelica://MultiInfrastructure/Resources/WeatherData/USA_MN_Minneapolis-St.Paul.Intl.AP.726580_TMY3.mos")
    "Weather data model"
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
  Modelica.Blocks.Sources.CombiTimeTable powRes(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-600000; 2,-460000; 4,-380000; 6,-400000; 8,-400000; 10,-300000; 12,
        -200000; 14,-300000; 16,-600000; 18,-800000; 20,-1000000; 22,-800000; 24,
        -600000]) "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,58},{-90,68}})));
  Modelica.Blocks.Sources.CombiTimeTable qRes(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,5; 1,7; 2,2; 3,3; 4,8; 5,10; 6,80; 7,100; 8,120; 9,80; 10,50; 11,30;
        12,20; 13,20; 14,30; 15,30; 16,20; 17,30; 18,30; 19,20; 20,30; 21,10; 22,
        5; 23,0; 24,5])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,28},{-90,38}})));
   MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=270,
        origin={-90,2})));
   MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin1(
    V_nominal=V_nominal,
    l=2000,
    P_nominal=2000000,
    redeclare
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_500
      commercialCable,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.automatic)
                       "Line from or to grid"      annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-60,20})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin2(
    V_nominal=V_nominal,
    redeclare replaceable
  MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_1500
      commercialCable,
    l=2000,
    P_nominal=2000000,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial)
                       "Line from or to grid"      annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-60,-20})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystem.Block comBlo(
    qb_nominal=1200,
    Ub_nominal=1000,
    numEV=600,
    V_nominal=V_nominal,
    f=f,
    lat=weaDat.lat,
    num=1) annotation (Placement(transformation(extent={{10,-52},{-10,-32}})));
  Modelica.Blocks.Sources.CombiTimeTable powCom(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-50000; 2,-30000; 4,-50000; 6,-100000; 8,-500000; 10,-800000; 12,-1100000;
        14,-1500000; 16,-1400000; 18,-1000000; 20,-700000; 22,-300000; 24,-50000])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{90,-46},{80,-36}})));
  Modelica.Blocks.Sources.CombiTimeTable qCom(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,5; 1,0; 2,0; 3,0; 4,8; 5,10; 6,20; 7,20; 8,30; 9,30; 10,15; 11,30;
        12,20; 13,20; 14,30; 15,30; 16,50; 17,98; 18,119; 19,80; 20,60; 21,40; 22,
        10; 23,15; 24,5])
    "Number of EV charging profile for a commercial building block"
    annotation (Placement(transformation(extent={{90,-80},{80,-70}})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystem.BaseClasses.RoadCom roa1(
  q_nominal=600, U_nominal=800) annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-24,4})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystem.BaseClasses.RoadCom roa2(
  q_nominal=1200, U_nominal=1000) annotation (
      Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={24,0})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystem.BaseClasses.Transmission tra1(
  kc={0.03}, cc={60}) annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-40,-28})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystem.BaseClasses.Transmission tra2(
    num=1,
    kc={0.03},
    cc={60}) annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={-32,-28})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystem.BaseClasses.Transmission tra4(
    num=1,
    kc={0.03},
    cc={40}) annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={38,24})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystem.BaseClasses.Transmission tra3(
  kc={0.03}, cc={40}) annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={32,28})));
equation
  connect(gri.terminal, lin1.terminal_p) annotation (Line(
      points={{-80,2},{-60,2},{-60,10}},
      color={0,120,120},
      thickness=0.5));
  connect(gri.terminal, lin2.terminal_n) annotation (Line(
      points={{-80,2},{-60,2},{-60,-10}},
      color={0,120,120},
      thickness=0.5));
  connect(lin1.terminal_n, resBlo.term_p) annotation (Line(
      points={{-60,30},{-60,56.2},{-9,56.2}},
      color={0,120,120},
      thickness=0.5));
  connect(comBlo.term_p, lin2.terminal_p) annotation (Line(
      points={{11,-35.8},{36,-35.8},{36,-62},{-60,-62},{-60,-30}},
      color={0,120,120},
      thickness=0.5));
  connect(weaDat.weaBus, resBlo.weaBus) annotation (Line(
      points={{-80,90},{-20,90},{-20,58.6},{-9,58.6}},
      color={255,204,51},
      thickness=0.5));
  connect(weaDat.weaBus, comBlo.weaBus) annotation (Line(
      points={{-80,90},{60,90},{60,-34},{10,-34},{10,-34},{10,-34},{10,-33.4},{11,
          -33.4}},
      color={255,204,51},
      thickness=0.5));
  connect(powRes.y[1], resBlo.PBui) annotation (Line(
      points={{-89.5,63},{-22,63},{-22,51},{-10,51}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powCom.y[1], comBlo.PBui) annotation (Line(
      points={{79.5,-41},{30,-41},{30,-41},{12,-41}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(roa1.port_b, resBlo.port_a1) annotation (Line(
      points={{-24,14},{-24,42},{-8.4,42}},
      color={0,127,255},
      thickness=1));
  connect(resBlo.port_b1, roa2.port_a) annotation (Line(
      points={{12,42},{24,42},{24,10}},
      color={0,127,255},
      thickness=1));
  connect(roa2.port_b, comBlo.port_a1) annotation (Line(
      points={{24,-10},{24,-50},{10.4,-50}},
      color={0,127,255},
      thickness=1));
  connect(comBlo.port_b1, roa1.port_a) annotation (Line(
      points={{-10,-50},{-24,-50},{-24,-6}},
      color={0,127,255},
      thickness=1));
  connect(qRes.y[1], resBlo.qb) annotation (Line(
      points={{-89.5,33},{-49.75,33},{-49.75,44},{-10,44}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qCom.y[1], comBlo.qb) annotation (Line(
      points={{79.5,-75},{44,-75},{44,-48},{12,-48}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(resBlo.numSenPac[1], tra3.numSenPac[1]) annotation (Line(
      points={{13,48},{32,48},{32,39}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra3.numRecPac[1], roa2.numRecPac) annotation (Line(
      points={{32,17},{32,11}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa2.numSenPac, tra4.numSenPac[1]) annotation (Line(
      points={{32,-11},{32,-16},{38,-16},{38,13}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra4.numRecPac[1], resBlo.numRecPac[1]) annotation (Line(
      points={{38,35},{38,68},{-18,68},{-18,48},{-10,48}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(qRes.y[1], roa2.numPacSet) annotation (Line(
      points={{-89.5,33},{20.6,33},{20.6,11}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(comBlo.numSenPac[1], tra2.numSenPac[1]) annotation (Line(
      points={{-11,-44},{-32,-44},{-32,-39}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra2.numRecPac[1], roa1.numRecPac) annotation (Line(
      points={{-32,-17},{-32,-7}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa1.numSenPac, tra1.numSenPac[1]) annotation (Line(
      points={{-32,15},{-32,20},{-40,20},{-40,-17}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra1.numRecPac[1], comBlo.numRecPac[1]) annotation (Line(
      points={{-40,-39},{-40,-58},{20,-58},{20,-44},{12,-44}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(qCom.y[1], roa1.numPacSet) annotation (Line(
      points={{79.5,-75},{48,-75},{48,-20},{-20.6,-20},{-20.6,-7}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)),
 experiment(
  StopTime=86400,
  Tolerance=1e-6),
    __Dymola_Commands(file="Resources/Scripts/Dymola/CoupledSystem/ThreeCoupledSystem/Examples/ThreeCoupledSystem.mos"
        "Simulate and Plot"),
    Documentation(info="<html>
     <p>This example demonstrates the coupling the three system.The EV charging power 
    consumption is calculated from the transportation system, which is related to the 
    traffic condition between the blocks.Also,the wireless communication power consumption is
    calculated from the communication system.  The wireless communication quality deteriorates
    with the increase of the vehicles on the road while the communication quality impacts on
    the road traffic conditions. The results from this example show the energy,
    transportation and communication conditions.</p>
</html>"));
end ThreeCoupledSystem;
