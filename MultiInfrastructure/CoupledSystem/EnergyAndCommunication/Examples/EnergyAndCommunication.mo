within MultiInfrastructure.CoupledSystem.EnergyAndCommunication.Examples;
model EnergyAndCommunication "Example that demonstrates the use of models in 
  MultiInfrastructure.CoupledSystem.EnergyAndCommunication"
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";
  MultiInfrastructure.CoupledSystem.EnergyAndCommunication.Block comBlo(
    lat=weaDat.lat,
    num=2,
    V_nominal=V_nominal,
    f=f)
    annotation (Placement(transformation(extent={{12,-40},{-8,-20}})));
  MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
    computeWetBulbTemperature=false,
    filNam="modelica://MultiInfrastructure/Resources/WeatherData/USA_MN_Minneapolis-St.Paul.Intl.AP.726580_TMY3.mos")
    "Weather data model"
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid  gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=270,
        origin={-90,2})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin1(
    V_nominal=V_nominal,
    l=2000,
    P_nominal=2000000,
    redeclare 
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_500
      commercialCable,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.automatic)
                       "Line from or to grid"      annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-40,20})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin2(
    V_nominal=V_nominal,
    redeclare replaceable 
  MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_1500
      commercialCable,
    l=2000,
    P_nominal=2000000,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial)
                       "Line from or to grid"      annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-40,-20})));
  Modelica.Blocks.Sources.CombiTimeTable powRes(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-600000; 2,-460000; 4,-380000; 6,-400000; 8,-400000; 10,-300000; 12,
        -200000; 14,-300000; 16,-600000; 18,-800000; 20,-1000000; 22,-800000; 24,
        -600000]) "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,60},{-90,70}})));
  Modelica.Blocks.Sources.CombiTimeTable powCom(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-50000; 2,-30000; 4,-50000; 6,-100000; 8,-500000; 10,-800000; 12,-1100000;
        14,-1500000; 16,-1400000; 18,-1000000; 20,-700000; 22,-300000; 24,-50000])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{84,-34},{74,-24}})));
  Modelica.Blocks.Sources.CombiTimeTable nevRes(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,100; 2,80; 4,60; 6,50; 8,30; 10,8; 12,10; 14,20; 16,30; 18,40; 20,60;
        22,80; 24,100])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,40},{-90,50}})));
  Modelica.Blocks.Sources.CombiTimeTable nevCom(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,5; 2,5; 4,5; 6,10; 8,60; 10,80; 12,80; 14,70; 16,60; 18,30; 20,20;
        22,10; 24,5])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{84,-50},{74,-40}})));
  MultiInfrastructure.CoupledSystem.EnergyAndCommunication.BaseClasses.Transmission tra1(
  num=2,
  kc={0.03,0.03},
  cc={105,100})
  annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-18,12})));
  MultiInfrastructure.CoupledSystem.EnergyAndCommunication.BaseClasses.Transmission tra2(
  num=2,
  cc={100,105},
  kc={0.03,0.05})
  annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=90,
        origin={18,12})));
  Modelica.Blocks.Sources.CombiTimeTable numPac1(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,5; 1,0; 2,0; 3,0; 4,8; 5,10; 6,20; 7,20; 8,30; 9,30; 10,15; 11,30; 12,
        20; 13,20; 14,30; 15,30; 16,50; 17,98; 18,119; 19,80; 20,60; 21,40; 22,10;
        23,15; 24,5]) "Number of vehicles for a commercial building block"
    annotation (Placement(transformation(extent={{-100,20},{-90,30}})));
  Modelica.Blocks.Sources.CombiTimeTable numPac2(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,5; 1,7; 2,2; 3,3; 4,8; 5,10; 6,80; 7,100; 8,120; 9,80; 10,50; 11,30; 12,
        20; 13,20; 14,30; 15,30; 16,20; 17,30; 18,30; 19,20; 20,30; 21,10; 22,5; 23,
        0; 24,5]) "Number of vehicle for a residential building block"
    annotation (Placement(transformation(extent={{84,-66},{74,-56}})));
  MultiInfrastructure.CoupledSystem.EnergyAndCommunication.Block resBlo(
    num=2,
    lat=weaDat.lat,
    V_nominal=V_nominal,
    f=f) annotation (Placement(transformation(extent={{-6,44},{14,64}})));
equation 
  connect(gri.terminal, lin1.terminal_p) annotation (Line(
      points={{-80,2},{-40,2},{-40,10}},
      color={0,120,120},
      thickness=0.5));
  connect(gri.terminal, lin2.terminal_n) annotation (Line(
      points={{-80,2},{-40,2},{-40,-10}},
      color={0,120,120},
      thickness=0.5));
  connect(lin2.terminal_p, comBlo.term_p1) annotation (Line(
      points={{-40,-30},{-40,-52},{40,-52},{40,-24},{13,-24}},
      color={0,120,120},
      thickness=0.5));
  connect(weaDat.weaBus, comBlo.weaBus) annotation (Line(
      points={{-80,90},{40,90},{40,-22},{12.6,-22}},
      color={255,204,51},
      thickness=0.5));
  connect(powCom.y[1], comBlo.PBui) annotation (Line(points={{73.5,-29},{
          44.75,-29},{14,-29}},             color={0,0,127},
      pattern=LinePattern.Dot));
  connect(nevCom.y[1], comBlo.numEV) annotation (Line(points={{73.5,-45},{
          26,-45},{26,-32},{14,-32}}, color={0,0,127},
      pattern=LinePattern.Dot));
  connect(tra2.numRecPac[1], comBlo.numRecPac[1]) annotation (Line(points={{17.5,1},
          {18,1},{18,-35},{14,-35}},          color={0,0,127},
      pattern=LinePattern.Dash));
  connect(numPac1.y[1], resBlo.numRecPac[1]) annotation (Line(points={{-89.5,25},
          {-58,25},{-34,25},{-34,26},{-34,49},{-8,49}},           color={0,0,127},
      pattern=LinePattern.Dot));

  connect(nevRes.y[1], resBlo.numEV) annotation (Line(points={{-89.5,45},{
          -60,45},{-36,45},{-36,52},{-30,52},{-8,52}}, color={0,0,127},
      pattern=LinePattern.Dot));
  connect(lin1.terminal_n, resBlo.term_p1) annotation (Line(
      points={{-40,30},{-40,60},{-7,60}},
      color={0,120,120},
      thickness=0.5));
  connect(weaDat.weaBus, resBlo.weaBus) annotation (Line(
      points={{-80,90},{-30,90},{-30,62},{-6.6,62}},
      color={255,204,51},
      thickness=0.5));
  connect(powRes.y[1], resBlo.PBui) annotation (Line(points={{-89.5,65},{
          -36,65},{-36,55},{-8,55}}, color={0,0,127},
      pattern=LinePattern.Dot));
  connect(resBlo.numSenPac, tra2.numSenPac)
    annotation (Line(points={{15,50},{18,50},{18,23}}, color={0,0,127},
      pattern=LinePattern.Dash));
  connect(numPac2.y[1], comBlo.numRecPac[2]) annotation (Line(points={{73.5,
          -61},{24,-61},{24,-33},{14,-33}}, color={0,0,127},
      pattern=LinePattern.Dot));
  connect(comBlo.numSenPac, tra1.numSenPac) annotation (Line(points={{-9,
          -34},{-18,-34},{-18,1}}, color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra1.numRecPac[2], resBlo.numRecPac[2]) annotation (Line(points={{-18.5,
          23},{-18.5,22},{-18,22},{-18,36},{-18,51},{-8,51}},
        color={0,0,127},
      pattern=LinePattern.Dash));
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)),
 experiment(
  StopTime=86400,
  Tolerance=1e-06),
    __Dymola_Commands(file=
          "Resources/Scripts/Dymola/CoupledSystem/EnergyAndCommunication/Examples/EnergyAndCommunication.mos"
        "Simulate and Plot"),
    Documentation(info="<html>
    <p>This example demonstrates the interaction between the energy system and communication system. 
    The wireless communication power consumption is calculated from the communication system. 
    The results from this example show both energy consumption profile and the communciation conditions.</p>
</html>"));
end EnergyAndCommunication;
