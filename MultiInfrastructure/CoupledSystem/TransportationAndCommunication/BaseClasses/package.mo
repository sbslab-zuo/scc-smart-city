within MultiInfrastructure.CoupledSystem.TransportationAndCommunication;
package BaseClasses

annotation (Documentation(info="<html>
<p>This package contains base classes that are used to construct the models in TransportationAndCommunication.</p>
</html>"));
end BaseClasses;
