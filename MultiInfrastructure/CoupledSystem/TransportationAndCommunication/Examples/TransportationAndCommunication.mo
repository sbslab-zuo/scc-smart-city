within MultiInfrastructure.CoupledSystem.TransportationAndCommunication.Examples;
model TransportationAndCommunication "Example that demonstrates the use of models in 
  MultiInfrastructure.CoupledSystem.TransportationAndCommunication"
  extends Modelica.Icons.Example;
  MultiInfrastructure.CoupledSystem.TransportationAndCommunication.Block resBlo(
    qb_nominal=1200,
    Ub_nominal=1000,
    numEV=600,
    num=1)     annotation (Placement(transformation(extent={{-10,30},{10,50}})));
  MultiInfrastructure.CoupledSystem.TransportationAndCommunication.Block comBlo(
    qb_nominal=600,
    Ub_nominal=800,
    numEV=100)
    annotation (Placement(transformation(extent={{10,-56},{-10,-36}})));
  MultiInfrastructure.CoupledSystem.TransportationAndCommunication.BaseClasses.RoadCom roa1(q_nominal=600, U_nominal=800) annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-24,4})));
  MultiInfrastructure.CoupledSystem.TransportationAndCommunication.BaseClasses.RoadCom roa2(q_nominal=1200, U_nominal=1000) annotation (
      Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={24,0})));
  Modelica.Blocks.Sources.CombiTimeTable qRes(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,5; 1,7; 2,2; 3,3; 4,8; 5,10; 6,80; 7,100; 8,120; 9,80; 10,50; 11,
        30; 12,20; 13,20; 14,30; 15,30; 16,20; 17,30; 18,30; 19,20; 20,30; 21,
        10; 22,5; 23,0; 24,5])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-80,34},{-60,54}})));
  Modelica.Blocks.Sources.CombiTimeTable qBlo(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,5; 1,0; 2,0; 3,0; 4,8; 5,10; 6,20; 7,20; 8,30; 9,30; 10,15; 11,30;
        12,20; 13,20; 14,30; 15,30; 16,50; 17,98; 18,119; 19,80; 20,60; 21,40;
        22,10; 23,15; 24,5])
    "Number of EV charging profile for a commercial building block"
    annotation (Placement(transformation(extent={{80,-76},{60,-56}})));
  MultiInfrastructure.CoupledSystem.TransportationAndCommunication.BaseClasses.Transmission
  tra3(kc={0.03}, cc={40}) annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={32,28})));
  MultiInfrastructure.CoupledSystem.TransportationAndCommunication.BaseClasses.Transmission
  tra4(
    num=1,
    kc={0.03},
    cc={40}) annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={38,24})));
  MultiInfrastructure.CoupledSystem.TransportationAndCommunication.BaseClasses.Transmission
  tra1(kc={0.03}, cc={60}) annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-40,-28})));
  MultiInfrastructure.CoupledSystem.TransportationAndCommunication.BaseClasses.Transmission
  tra2(
    num=1,
    kc={0.03},
    cc={60}) annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={-32,-28})));
equation
  connect(resBlo.port_b1, roa2.port_a)
    annotation (Line(points={{10.4,40},{24,40},{24,10}}, color={0,127,255},
      thickness=1));
  connect(roa2.port_b, comBlo.port_a1) annotation (Line(points={{24,-10},{24,
          -46},{10.4,-46}}, color={0,127,255},
      thickness=1));
  connect(roa1.port_a, comBlo.port_b1) annotation (Line(points={{-24,-6},{-24,
          -46},{-10.4,-46}}, color={0,127,255},
      thickness=1));
  connect(roa1.port_b, resBlo.port_a1) annotation (Line(points={{-24,14},{-24,
          40},{-10.4,40}}, color={0,127,255},
      thickness=1));
  connect(qRes.y[1], resBlo.qb)
    annotation (Line(points={{-59,44},{-36,44},{-36,36},{-12,36}},
                                                 color={0,0,127},
      pattern=LinePattern.Dot));
  connect(comBlo.qb, qBlo.y[1]) annotation (Line(
      points={{12,-50},{34,-50},{34,-66},{59,-66}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qRes.y[1], roa2.numPacSet) annotation (Line(
      points={{-59,44},{-36,44},{-36,22},{12,22},{20.6,22},{20.6,16},{20.6,11}},
      color={0,0,127},
      pattern=LinePattern.Dot));

  connect(roa1.numPacSet, qBlo.y[1]) annotation (Line(
      points={{-20.6,-7},{-20.6,-20},{40,-20},{40,-66},{59,-66}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(resBlo.numPac[1], tra3.numSenPac[1]) annotation (Line(
      points={{11,44.2},{32,44.2},{32,39}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa2.numSenPac, tra4.numSenPac[1]) annotation (Line(
      points={{32,-11},{32,-16},{38,-16},{38,13}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra4.numRecPac[1], resBlo.numRecPac[1]) annotation (Line(
      points={{38,35},{38,60},{-20,60},{-20,44.2},{-12,44.2}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa2.numRecPac, tra3.numRecPac[1]) annotation (Line(
      points={{32,11},{32,17}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa1.numSenPac, tra1.numSenPac[1]) annotation (Line(
      points={{-32,15},{-32,20},{-40,20},{-40,-17}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra1.numRecPac[1], comBlo.numRecPac[1]) annotation (Line(
      points={{-40,-39},{-40,-60},{20,-60},{20,-41.8},{12,-41.8}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(comBlo.numPac[1], tra2.numSenPac[1]) annotation (Line(
      points={{-11,-41.8},{-32,-41.8},{-32,-39}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra2.numRecPac[1], roa1.numRecPac) annotation (Line(
      points={{-32,-17},{-32,-7}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false)), Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)),
 experiment(
  StopTime=86400,
  Tolerance=1e-6),
    __Dymola_Commands(file=
          "Resources/Scripts/Dymola/CoupledSystem/TransportationAndCommunication/Examples/TransportationAndCommunication.mos"
        "Simulate and Plot"),
    Documentation(info="<html>
    <p>This example demonstrates the interaction between the transportation system and communication system.
    The wireless communication quality deteriorates with the increase of the vehicles 
    on the road while the communication quality impacts on the road traffic conditions. 
    The results from this example show both the traffic and communication conditions. </p>
</html>"));
end TransportationAndCommunication;
