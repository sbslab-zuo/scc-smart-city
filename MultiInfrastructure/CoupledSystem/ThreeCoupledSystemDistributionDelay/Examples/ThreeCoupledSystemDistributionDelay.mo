within MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDistributionDelay.Examples;
model ThreeCoupledSystemDistributionDelay
  "Example that demonstrates the use of models in MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDistributionDelay"
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDistributionDelay.Block resBlo(
    V_nominal=V_nominal,
    f=f,
    lat=weaDat.lat,
    num=1,
    numEV=1500) annotation (Placement(transformation(extent={{-10,40},{10,60}})));
  MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
    computeWetBulbTemperature=false,
    filNam="modelica://MultiInfrastructure/Resources/WeatherData/USA_MN_Minneapolis-St.Paul.Intl.AP.726580_TMY3.mos")
    "Weather data model"
    annotation (Placement(transformation(extent={{100,80},{80,100}})));
  Modelica.Blocks.Sources.CombiTimeTable qRes(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,15; 1,21; 2,6; 3,9; 4,24; 5,30; 6,240; 7,360; 8,580; 9,240; 10,150;
        11,88; 12,60; 13,60; 14,90; 15,90; 16,60; 17,90; 18,90; 19,60; 20,90;
        21,30; 22,15; 23,0; 24,15])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,30},{-90,40}})));
   MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=270,
        origin={-90,0})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin2(
    V_nominal=V_nominal,
    l=2000,
    P_nominal=2000000,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare replaceable
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_1
      commercialCable)
    "Line from or to grid"
    annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-60,-20})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDistributionDelay.Block comBlo(
    V_nominal=V_nominal,
    f=f,
    lat=weaDat.lat,
    num=1,
    numEV=200)
    annotation (Placement(transformation(extent={{10,-32},{-10,-52}})));
  Modelica.Blocks.Sources.CombiTimeTable qCom(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,15; 1,0; 2,0; 3,0; 4,24; 5,30; 6,60; 7,60; 8,90; 9,90; 10,45; 11,
        90; 12,57; 13,61; 14,90; 15,90; 16,150; 17,275; 18,601; 19,325; 20,180;
        21,85; 22,35; 23,45; 24,15])
    "Number of EV charging profile for a commercial building block"
    annotation (Placement(transformation(extent={{100,-86},{90,-76}})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDistributionDelay.BaseClasses.Transmission
    tra1(kc={0.03}, cc={360}) annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-40,-26})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDistributionDelay.BaseClasses.Transmission
    tra2(
    num=1,
    kc={0.03},
    cc={360}) annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={-32,-26})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDistributionDelay.BaseClasses.Transmission
    tra4(
    num=1,
    kc={0.03},
    cc={400}) annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={40,32})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDistributionDelay.BaseClasses.Transmission
    tra3(kc={0.03}, cc={400}) annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={32,32})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDistributionDelay.BaseClasses.RoadVariableDelayCom
    roa1(l=1200, redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-6,2})));
  Modelica.Blocks.Sources.CombiTimeTable proRes(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.35; 1,0.35; 2,0.35; 3,0.35; 4,0.2; 5,0.2; 6,0.2; 7,0.2; 8,0.1; 9,
        0.1; 10,0.1; 11,0.1; 12,0.1; 13,0.1; 14,0.1; 15,0.1; 16,0.1; 17,0.1; 18,
        0.1; 19,0.1; 20,0.2; 21,0.2; 22,0.35; 23,0.35; 24,0.35])
    "Probability of charging for a single EV at different time in a residential block"
    annotation (Placement(transformation(extent={{-100,16},{-90,26}})));
  Modelica.Blocks.Sources.CombiTimeTable proCom(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.1; 1,0.1; 2,0.1; 3,0.1; 4,0.1; 5,0.1; 6,0.1; 7,0.1; 8,0.15; 9,
        0.15; 10,0.15; 11,0.2; 12,0.2; 13,0.2; 14,0.2; 15,0.2; 16,0.15; 17,0.15;
        18,0.1; 19,0.1; 20,0.1; 21,0.1; 22,0.1; 23,0.1; 24,0.1])
    "Probability of charging for a single EV at different time in a commercial block"
    annotation (Placement(transformation(extent={{100,-100},{90,-90}})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDistributionDelay.BaseClasses.RoadVariableDelayCom
    roa2(l=1200, redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.CClassRoad30
      roaTyp) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={6,2})));
  Modelica.Blocks.Sources.CombiTimeTable powRes4(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-84720; 2,-74880; 4,-70080; 6,-55920; 8,-76320; 10,-45120; 12,-40320;
        14,-54720; 16,-102720; 18,-147120; 20,-167520; 22,-141120; 24,-84720])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,90},{-90,100}})));
  Modelica.Blocks.Sources.CombiTimeTable powRes3(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-66000; 2,-41800; 4,-44000; 6,-46200; 8,-39600; 10,-38830; 12,-26730;
        14,-34100; 16,-66550; 18,-81400; 20,-88000; 22,-84040; 24,-66000])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,74},{-90,84}})));
  Modelica.Blocks.Sources.CombiTimeTable powRes2(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-32513; 2,-34300; 4,-25720; 6,-34138; 8,-30238; 10,-17400; 12,-11063;
        14,-17400; 16,-49413; 18,-58188; 20,-76063; 22,-37388; 24,-32513])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,60},{-90,70}})));
  Modelica.Blocks.Sources.CombiTimeTable powRes1(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-37800; 2,-27300; 4,-21600; 6,-22300; 8,-22200; 10,-15300; 12,-7500;
        14,-15300; 16,-37800; 18,-52200; 20,-67840; 22,-52300; 24,-37800])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,44},{-90,54}})));
  Modelica.Blocks.Sources.CombiTimeTable powCom4(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-237929; 2,-237929; 4,-250152; 6,-405843; 8,-862476; 10,-930419; 12,
        -938514; 14,-687295; 16,-736484; 18,-474911; 20,-297855; 22,-274596; 24,
        -237929]) "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{100,-30},{90,-20}})));
  Modelica.Blocks.Sources.CombiTimeTable powCom3(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-170247; 2,-163757; 4,-156618; 6,-277908; 8,-372734; 10,-275424; 12,
        -267122; 14,-256838; 16,-286342; 18,-379222; 20,-393572; 22,-340757; 24,
        -170247]) "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{100,-44},{90,-34}})));
  Modelica.Blocks.Sources.CombiTimeTable powCom2(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-33576; 2,-33576; 4,-33576; 6,-53198; 8,-77930; 10,-79689; 12,-78508;
        14,-45799; 16,-46163; 18,-24082; 20,-26174; 22,-33576; 24,-33576])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{100,-58},{90,-48}})));
  Modelica.Blocks.Sources.CombiTimeTable powCom1(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-16760; 2,-16226; 4,-17427; 6,-22552; 8,-24154; 10,-20099; 12,-21350;
        14,-21634; 16,-31645; 18,-41618; 20,-39718; 22,-27601; 24,-16760])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{100,-72},{90,-62}})));
   MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin1(
    V_nominal=V_nominal,
    l=2000,
    P_nominal=2000000,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_1
      commercialCable)
    "Line from or to grid"
    annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-60,20})));
equation
  connect(gri.terminal, lin1.terminal_p) annotation (Line(
      points={{-80,-1.77636e-015},{-60,-1.77636e-015},{-60,10}},
      color={0,120,120},
      thickness=0.5));
  connect(gri.terminal, lin2.terminal_n) annotation (Line(
      points={{-80,-1.77636e-015},{-60,-1.77636e-015},{-60,-10}},
      color={0,120,120},
      thickness=0.5));
  connect(lin1.terminal_n, resBlo.term_p) annotation (Line(
      points={{-60,30},{-60,56.2},{-11,56.2}},
      color={0,120,120},
      thickness=0.5));
  connect(comBlo.term_p, lin2.terminal_p) annotation (Line(
      points={{11,-48.2},{60,-48.2},{60,-64},{-60,-64},{-60,-30}},
      color={0,120,120},
      thickness=0.5));
  connect(weaDat.weaBus, resBlo.weaBus) annotation (Line(
      points={{80,90},{-20,90},{-20,58.6},{-11,58.6}},
      color={255,204,51},
      thickness=0.5));
  connect(resBlo.numSenPac[1], tra3.numSenPac[1]) annotation (Line(
      points={{11,48},{32,48},{32,43}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra3.numRecPac[1], roa2.numRecPac) annotation (Line(
      points={{32,21},{32,16},{14,16},{14,13}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa2.numSenPac, tra4.numSenPac[1]) annotation (Line(
      points={{14,-9},{14,-20},{40,-20},{40,21}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra4.numRecPac[1], resBlo.numRecPac[1]) annotation (Line(
      points={{40,43},{40,68},{-18,68},{-18,48},{-12,48}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(qRes.y[1], roa2.numPacSet) annotation (Line(
      points={{-89.5,35},{2.8,35},{2.8,13}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(comBlo.numSenPac[1], tra2.numSenPac[1]) annotation (Line(
      points={{-11,-40},{-32,-40},{-32,-37}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra2.numRecPac[1], roa1.numRecPac) annotation (Line(
      points={{-32,-15},{-32,-12},{-14,-12},{-14,-9}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa1.numSenPac, tra1.numSenPac[1]) annotation (Line(
      points={{-14,13},{-14,20},{-40,20},{-40,-15}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra1.numRecPac[1], comBlo.numRecPac[1]) annotation (Line(
      points={{-40,-37},{-40,-58},{20,-58},{20,-40},{12,-40}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(resBlo.qOut[1], roa2.qIn) annotation (Line(
      points={{6,38},{6,14}},
      color={28,108,200},
      thickness=1));
  connect(comBlo.qOut[1], roa1.qIn) annotation (Line(
      points={{-6,-30},{-6,-10}},
      color={28,108,200},
      thickness=1));
  connect(roa1.qOut, resBlo.qIn[1]) annotation (Line(
      points={{-6,13},{-6,38}},
      color={28,108,200},
      thickness=1));
  connect(proRes.y[1], resBlo.proEV) annotation (Line(
      points={{-89.5,21},{-32,21},{-32,40.6},{-12,40.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(proCom.y[1], comBlo.proEV) annotation (Line(
      points={{89.5,-95},{44,-95},{44,-32.6},{12,-32.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qRes.y[1], resBlo.qOutSet[1]) annotation (Line(
      points={{-89.5,35},{-40,35},{-40,45.6},{-12,45.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qCom.y[1], comBlo.qOutSet[1]) annotation (Line(
      points={{89.5,-81},{44,-81},{44,-37.6},{12,-37.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
connect(powRes1.y[1], resBlo.PBui[10]) annotation (Line(
      points={{-89.5,49},{-34,49},{-34,52},{-12,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powRes1.y[1], resBlo.PBui[11]) annotation (Line(
      points={{-89.5,49},{-34,49},{-34,52},{-12,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powRes1.y[1], resBlo.PBui[12]) annotation (Line(
      points={{-89.5,49},{-34,49},{-34,52},{-12,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powRes1.y[1], resBlo.PBui[1]) annotation (Line(
      points={{-89.5,49},{-34,49},{-34,52},{-12,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powRes2.y[1], resBlo.PBui[2]) annotation (Line(
      points={{-89.5,65},{-34,65},{-34,52},{-12,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powRes2.y[1], resBlo.PBui[3]) annotation (Line(
      points={{-89.5,65},{-34,65},{-34,52},{-12,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powRes2.y[1], resBlo.PBui[6]) annotation (Line(
      points={{-89.5,65},{-34,65},{-34,52},{-12,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powRes2.y[1], resBlo.PBui[7]) annotation (Line(
      points={{-89.5,65},{-34,65},{-34,52},{-12,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powRes2.y[1], resBlo.PBui[8]) annotation (Line(
      points={{-89.5,65},{-34,65},{-34,52},{-12,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powRes3.y[1], resBlo.PBui[5]) annotation (Line(
      points={{-89.5,79},{-34,79},{-34,52},{-12,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powRes4.y[1], resBlo.PBui[4]) annotation (Line(
      points={{-89.5,95},{-34,95},{-34,52},{-12,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powRes4.y[1], resBlo.PBui[9]) annotation (Line(
      points={{-89.5,95},{-34,95},{-34,52},{-12,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powCom1.y[1], comBlo.PBui[10]) annotation (Line(
      points={{89.5,-67},{44,-67},{44,-44},{12,-44}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powCom1.y[1], comBlo.PBui[11]) annotation (Line(
      points={{89.5,-67},{44,-67},{44,-44},{12,-44}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powCom1.y[1], comBlo.PBui[12]) annotation (Line(
      points={{89.5,-67},{44,-67},{44,-44},{12,-44}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powCom1.y[1], comBlo.PBui[1]) annotation (Line(
      points={{89.5,-67},{44,-67},{44,-44},{12,-44}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powCom2.y[1], comBlo.PBui[2]) annotation (Line(
      points={{89.5,-53},{44,-53},{44,-44},{12,-44}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powCom2.y[1], comBlo.PBui[3]) annotation (Line(
      points={{89.5,-53},{44,-53},{44,-44},{12,-44}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powCom2.y[1], comBlo.PBui[6]) annotation (Line(
      points={{89.5,-53},{44,-53},{44,-44},{12,-44}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powCom2.y[1], comBlo.PBui[7]) annotation (Line(
      points={{89.5,-53},{44,-53},{44,-44},{12,-44}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powCom2.y[1], comBlo.PBui[8]) annotation (Line(
      points={{89.5,-53},{44,-53},{44,-44},{12,-44}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powCom3.y[1], comBlo.PBui[5]) annotation (Line(
      points={{89.5,-39},{44,-39},{44,-44},{12,-44}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powCom4.y[1], comBlo.PBui[4]) annotation (Line(
      points={{89.5,-25},{44,-25},{44,-44},{12,-44}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powCom4.y[1], comBlo.PBui[9]) annotation (Line(
      points={{89.5,-25},{44,-25},{44,-44},{12,-44}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qCom.y[1], roa1.numPacSet) annotation (Line(
      points={{89.5,-81},{44,-81},{44,-26},{-2.8,-26},{-2.8,-9}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(roa2.qOut, comBlo.qIn[1]) annotation (Line(
      points={{6,-9},{6,-30}},
      color={28,108,200},
      thickness=1));
  connect(weaDat.weaBus, comBlo.weaBus) annotation (Line(
      points={{80,90},{70,90},{70,-50.6},{11,-50.6}},
      color={255,204,51},
      thickness=0.5));
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,120}})),                                  Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,
            100}})),
 experiment(
  StopTime=86400,
  Tolerance=1e-06),
    __Dymola_Commands(file="modelica://MultiInfrastructure/Resources/Scripts/Dymola/CoupledSystem/ThreeCoupledSystemDistributionDelay/Examples/ThreeCoupledSystemDistributionDelay.mos"
        "Simulate and Plot"),
    Documentation(info="<html>
     <p>This example demonstrates the coupling the three system.The EV charging power 
    consumption is calculated from the transportation system, which is related to the 
    traffic condition between the blocks.Also,the wireless communication power consumption is
    calculated from the communication system.  The wireless communication quality deteriorates
    with the increase of the vehicles on the road while the communication quality impacts on
    the road traffic conditions. The results from this example show the energy,
    transportation and communication conditions.</p>
</html>"));
end ThreeCoupledSystemDistributionDelay;
