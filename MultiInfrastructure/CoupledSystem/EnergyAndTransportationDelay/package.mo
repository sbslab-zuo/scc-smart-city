within MultiInfrastructure.CoupledSystem;
package EnergyAndTransportationDelay

annotation (Documentation(info="<html>
<p>This package contains models for coupled systems of energy and transportation. </p>
</html>"));
end EnergyAndTransportationDelay;
