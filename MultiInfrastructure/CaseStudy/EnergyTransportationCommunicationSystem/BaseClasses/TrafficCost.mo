within MultiInfrastructure.CaseStudy.EnergyTransportationCommunicationSystem.BaseClasses;
model TrafficCost
  "Model of calculation of traffic cost using the speed-flow correlation"
  parameter Modelica.SIunits.Length l=1000 "Length of the road";
  Real num "Number of the vehicles on the road";
  Real b  "Regression coefficient of the road velocity function";
  Real qAve "Average traffic flow on the road";
  Modelica.SIunits.Velocity u( start=roaTyp.uf)
                                               "Traffic speed";
  Modelica.SIunits.Time t "Travel time on the road";
  Modelica.Blocks.Interfaces.RealInput qIn "Arrived traffic flow on the road"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Interfaces.RealOutput delTim
    "Delay time calculation based on the traffic theory"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  replaceable parameter MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.Generic roaTyp
    "Road type" annotation (choicesAllMatching=true, Placement(transformation(
          extent={{40,40},{60,60}})));

equation
  num=qIn;
  qAve=num/l*u;
  b=roaTyp.a2 + roaTyp.a3*(qAve/(roaTyp.q_nominal/3600))^3;
  u=roaTyp.a1*roaTyp.uf/(1 + (qAve/(roaTyp.q_nominal/3600))^b);
  t= if noEvent(u>0.1) then l/u else l/0.1;
  delTim=t;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={Text(
          extent={{-60,28},{54,-26}},
          lineColor={238,46,47},
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid,
          textString="Delay"), Rectangle(
          extent={{-80,40},{80,-40}},
          lineColor={238,46,47},
          lineThickness=0.5),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name")}),                                 Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This model calculates the traffic cost of the road using a practical correlation of the vehicle speed and traffic flow. </p>

<h4>Reference</h4>
<ul>
<li>
Wei, W. (2003). Practical speed-flow relationship model of highway traffic-flow. Journal of Southeast University (Natural Science Edition), 4, 025.
</li>
</ul>
</html>"));
end TrafficCost;
