within MultiInfrastructure.CaseStudy.Figure;
model ThreeCoupled
  "Model that connects the energy,transportation and communication system"
  final parameter Integer n=12 "Number of buildings in the community";
  parameter Modelica.SIunits.Voltage V_nominal=10000
    "Nominal voltage(V_nominal>=0)"
    annotation(Dialog(tab="Energy",group="Parameters"));
  parameter Modelica.SIunits.Frequency f=60 "Nominal grid frequency"
    annotation(Dialog(tab="Energy",group="Parameters"));
  parameter Modelica.SIunits.Angle lat=lat "Latitude"
    annotation(Dialog(tab="Energy",group="Parameters"));
  parameter Modelica.SIunits.Area A=600000 "Net surface area of PV"
    annotation (Dialog(tab="Energy",group="Renewable Generation"));
  parameter Modelica.SIunits.Power PWin_nominal=-ene.PLoa_nominal
    annotation (Dialog(tab="Energy",group="Renewable Generation"));
  parameter Real SOC_start=0.6 "Initial charge"
    annotation (Dialog(tab="Energy",group="Battery"));
  parameter Modelica.SIunits.Energy EMax=18000000000 "Maximum available charge"
    annotation (Dialog(tab="Energy",group="Battery"));
  parameter Real betDis=0.3 "Discharging velocity coefficient"
    annotation (Dialog(tab="Energy",group="Battery"));
  parameter Real betCha=0.9 "Charging velocity coefficient"
    annotation (Dialog(tab="Energy",group="Battery"));
  parameter Modelica.SIunits.Power thrDis=-3e5 "Discharging power threshold"
    annotation (Dialog(tab="Energy",group="Battery"));
  parameter Modelica.SIunits.Power thrCha=-2e5 "Charging power threshold"
    annotation (Dialog(tab="Energy",group="Battery"));
  parameter Real numEV(fixed=true, start=numEV)
    "Number of charging EV in the block"
    annotation(Dialog(tab="Transportation",group="Parameters"));
  parameter Integer num=2 "Number of ports for transportation and communication system";
  IndividualSystem.Energy.EnergyDistribution ene(
    V_nominal=V_nominal,
    f=f,
    lat=lat,
    num=num,
    A=A,
    PWin_nominal=PWin_nominal,
    SOC_start=SOC_start,
    EMax=EMax,
    betDis=betDis,
    betCha=betCha,
    thrDis=thrDis,
    thrCha=thrCha,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
    annotation (Placement(transformation(extent={{20,8},{46,28}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_p term_p
    annotation (Placement(transformation(extent={{-120,22},{-100,42}}),
        iconTransformation(extent={{-120,22},{-100,42}})));
  MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.Bus weaBus annotation (Placement(
        transformation(extent={{-120,38},{-100,58}}), iconTransformation(extent={{-120,38},
            {-100,58}})));
  Modelica.Blocks.Interfaces.RealInput numRecPac[num]
    "Number of received packets"
    annotation (Placement(transformation(extent={{-140,-40},{-100,0}})));

  Modelica.Blocks.Interfaces.RealInput qOutSet[num] "Connector of Real input signal"
    annotation (Placement(transformation(extent={{-140,-68},{-100,-28}})));
  Modelica.Blocks.Interfaces.RealInput qIn[num] "Connector of Real input signal"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=90,
        origin={-60,-100})));
  Modelica.Blocks.Interfaces.RealInput proEV
    "Probability of single EV to be charged"
    annotation (Placement(transformation(extent={{-140,-94},{-100,-54}})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDistributionDelay.BaseClasses.TransportationBlock
    tra(num=num, numEV(start=numEV, fixed=true))
    annotation (Placement(transformation(extent={{20,-66},{40,-46}})));
  Modelica.Blocks.Interfaces.RealOutput qOut [num] "Connector of Real output signal"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={60,-100}),
        iconTransformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={60,-100})));
  Buildings.Controls.OBC.CDL.Interfaces.RealInput PBui [n](
    quantity="Power",
    unit="W",
    max=0)
    "Building power load(negative means consumption, positive means generation)"
    annotation (Placement(transformation(extent={{-140,-12},{-100,28}}),
        iconTransformation(extent={{-140,-12},{-100,28}})));
equation
  connect(term_p,term_p)  annotation (Line(points={{-110,32},{-110,32}},
                      color={0,120,120}));
  connect(ene.weaBus,weaBus)  annotation (Line(
      points={{19,27.8},{0,27.8},{0,48},{-110,48}},
      color={255,204,51},
      thickness=0.5));
  connect(ene.term_p,term_p)  annotation (Line(points={{19,21},{-20,21},{-20,32},
          {-110,32}}, color={0,120,120},
      thickness=0.5));
  connect(qOutSet,tra. qOutSet) annotation (Line(points={{-120,-48},{0,-48},{0,
          -51},{18,-51}}, color={0,0,127}));
  connect(qIn,tra. qIn) annotation (Line(points={{-60,-100},{-60,-56},{18,-56}},
                 color={0,0,127}));
  connect(proEV,tra. proEV) annotation (Line(points={{-120,-74},{12,-74},{12,
          -64},{18,-64}},
                     color={0,0,127}));
  connect(tra.qOut,qOut)  annotation (Line(points={{41,-56},{60,-56},{60,-100}},
                     color={0,0,127}));
  connect(tra.numEVCha, ene.numEV) annotation (Line(points={{41,-64},{48,-64},{
          48,-32},{4,-32},{4,13.4},{18,13.4}},
                                      color={0,0,127}));
  connect(ene.PBui[7], PBui[7]) annotation (Line(
      points={{18,17.5667},{0,17.5667},{0,8},{-120,8},{-120,9.66667}},
      color={0,0,127}));
  connect(numRecPac, ene.numSenPac) annotation (Line(points={{-120,-20},{10,-20},
          {10,9.4},{18,9.4}}, color={0,0,127}));
    annotation(Dialog(tab="Transportation",group="Parameters"),
              Dialog(tab="Energy",group="Parameters"),
            Dialog(tab="Energy",group="Parameters"),
              Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Line(points={{-96,-48},{-94,-54},{-92,-60},{-90,-64},{-88,-68},{-86,
              -70}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-76,-48},{-78,-54},{-80,-60},{-82,-64},{-84,-68},{-86,
              -70}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-76,-48},{-74,-42},{-72,-36},{-70,-32},{-68,-28},{-66,
              -26}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-56,-48},{-58,-42},{-60,-36},{-62,-32},{-64,-28},{-66,
              -26}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-56,-48},{-54,-54},{-52,-60},{-50,-64},{-48,-68},{-46,
              -70}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-36,-48},{-38,-54},{-40,-60},{-42,-64},{-44,-68},{-46,
              -70}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-36,-48},{-34,-42},{-32,-36},{-30,-32},{-28,-28},{-26,
              -26}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-16,-48},{-18,-42},{-20,-36},{-22,-32},{-24,-28},{-26,
              -26}},                                                 color={28,
              108,200},
          pattern=LinePattern.Dash),
        Line(points={{-16,-48},{-14,-54},{-12,-60},{-10,-64},{-8,-68},{-6,-70}},
                                                                     color={28,
              108,200},
          pattern=LinePattern.Dash),
        Line(points={{4,-48},{2,-54},{0,-60},{-2,-64},{-4,-68},{-6,-70}}, color={28,
              108,200},
          pattern=LinePattern.Dash),
        Line(points={{4,-48},{6,-42},{8,-36},{10,-32},{12,-28},{14,-26}},
                                                                     color={28,
              108,200},
          pattern=LinePattern.Dash),
        Line(points={{24,-48},{22,-42},{20,-36},{18,-32},{16,-28},{14,-26}},
                                                                     color={28,
              108,200},
          pattern=LinePattern.Dash),
        Text(
          extent={{-152,158},{132,106}},
          lineColor={0,0,255},
          textString="%name"),
        Rectangle(extent={{-100,100},{100,-100}}, lineColor={0,0,0}),
        Rectangle(
          extent={{-78,20},{72,-24}},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{12,11},{-12,-11}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          radius=10,
          origin={57,20},
          rotation=90),
        Rectangle(
          extent={{12,11},{-12,-11}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          radius=10,
          origin={57,56},
          rotation=90),
        Line(points={{-62,62},{-36,62}},   color={0,0,0}),
        Line(points={{-50,62},{-50,32}},    color={0,0,0}),
        Line(points={{-62,62},{-62,60}},   color={0,0,0}),
        Line(points={{-36,62},{-36,60}},   color={0,0,0}),
        Line(
          points={{-72,32},{-70,34},{-66,38},{-62,52},{-62,60}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-72,32},{-60,34},{-54,36},{-38,48},{-36,60}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(points={{-26,72},{0,72}},     color={0,0,0}),
        Line(points={{-26,72},{-26,70}},   color={0,0,0}),
        Line(points={{0,72},{0,70}},       color={0,0,0}),
        Line(
          points={{-62,60},{-46,64},{-36,66},{-30,68},{-26,70}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-36,60},{-24,62},{-14,64},{-6,66},{0,70}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-26,70},{-10,74},{0,76},{6,78},{10,80}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{0,70},{12,72},{22,74},{30,76},{36,80}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(points={{10,82},{36,82}},   color={0,0,0}),
        Line(points={{-14,72},{-14,42}},   color={0,0,0}),
        Line(points={{22,82},{22,50}},   color={0,0,0}),
        Line(points={{10,82},{10,80}},     color={0,0,0}),
        Line(points={{36,82},{36,80}},     color={0,0,0}),
        Rectangle(
          extent={{46,60},{68,24}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{52,60},{62,50}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={238,46,47},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{52,42},{62,32}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{52,26},{62,16}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid), Line(
          points={{-78,-2},{72,-2}},
          color={255,255,255},
          pattern=LinePattern.Dash,
          thickness=0.5),
        Ellipse(
          extent={{19.5,0.5},{91.5,-71.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{22.5,-2.5},{88.5,-68.5}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{34.5,-14.5},{76.5,-56.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{37.5,-17.5},{73.5,-53.5}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{43.5,-23.5},{67.5,-47.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{54,-34},{30,-96},{32,-96},{54,-38},{54,-34}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{48,-50},{62,-52}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{50.5,-30.5},{60,-40}},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Polygon(
          points={{56,-34},{80,-96},{78,-96},{56,-38},{56,-34}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{44,-62},{66,-64}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}),                      Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This model shows the three agents (energy, transportation and communication) in the block.</p>
</html>"));
end ThreeCoupled;
