within MultiInfrastructure.CaseStudy.Figure;
model ConnectedCommunities_EnergyTransportation
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";
  EnergyTransportationSystem.BaseClasses.EnergyTransportation resBlo1(
    lat=weaDat.lat,
    numEV=800,
    A=100000) annotation (Placement(transformation(extent={{-50,40},{-30,60}})));
  EnergyTransportationSystem.BaseClasses.EnergyTransportation resBlo2(
    lat=weaDat.lat,
    numEV=800,
    betDis=0.6,
    betCha=0.8,
    thrCha=-6e5,
    A=100000) annotation (Placement(transformation(extent={{50,40},{30,60}})));
  EnergyTransportationSystem.BaseClasses.EnergyTransportation comBlo(
    lat=weaDat.lat,
    numEV=200,
    betDis=0.3,
    betCha=0.9,
    thrCha=-3.5e5,
    A=150000)
    annotation (Placement(transformation(extent={{-10,-46},{10,-66}})));
  CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses.RoadVariableDelay
    roa1(redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp, l=8000) annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={0,32})));
  CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses.RoadVariableDelay
    roa2(redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp, l=8000) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={0,24})));
  CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses.RoadVariableDelay
    roa3(        redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp, l=10000)
              annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={-34,0})));
  CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses.RoadVariableDelay
    roa4(redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp, l=10000)
                      annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-46,0})));
  CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses.RoadVariableDelay
    roa5(        redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp, l=9000)
              annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={34,0})));
  CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses.RoadVariableDelay
    roa6(redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp, l=9000) annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={46,0})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
    computeWetBulbTemperature=false, filNam="modelica://MultiInfrastructure/Resources/WeatherData/USA_CA_San.Francisco.Intl.AP.724940_TMY3.mos")
    "Weather data model"
    annotation (Placement(transformation(extent={{-100,94},{-80,114}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-90,84})));
  Modelica.Blocks.Sources.CombiTimeTable PBui1(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-31642.24665; 1,-25737.65655; 2,-23374.2521; 3,-22685.2667; 4,-22501.96805;
        5,-22001.6213; 6,-23525.33505; 7,-28600.1871; 8,-33781.5394; 9,-29866.28095;
        10,-23544.42185; 11,-22349.86555; 12,-21847.5527; 13,-21501.7796; 14,-21735.9029;
        15,-22513.4214; 16,-24298.4994; 17,-30480.0999; 18,-41825.40685; 19,-50450.39685;
        20,-50320.518; 21,-48195.64265; 22,-44804.9175; 23,-37865.276; 24,-31642.24665])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,42},{-90,52}})));
  Modelica.Blocks.Sources.CombiTimeTable PBui3(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-15106.99921; 1,-15106.99921; 2,-15106.99921; 3,-15162.01319; 4,-15190.36283;
        5,-15292.58251; 6,-15288.13499; 7,-22916.16308; 8,-22530.14638; 9,-32344.27609;
        10,-48736.55298; 11,-48736.55298; 12,-48736.55298; 13,-48736.55298; 14,-48736.55298;
        15,-48736.55298; 16,-48736.55298; 17,-54479.05298; 18,-60221.55298; 19,-48376.76377;
        20,-48376.76377; 21,-43829.27609; 22,-22377.64918; 23,-15106.99921; 24,-15106.99921])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,-38},{-90,-28}})));
  Modelica.Blocks.Sources.CombiTimeTable PBui2(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-36856.7682; 1,-30109.66872; 2,-26911.8066; 3,-25905.7908; 4,-25780.80756;
        5,-25155.5178; 6,-27082.74654; 7,-32366.52792; 8,-39100.60374; 9,-34581.82464;
        10,-27258.65676; 11,-25785.80442; 12,-25415.8515; 13,-24931.6566; 14,-25393.53036;
        15,-26149.71714; 16,-28371.35976; 17,-35629.00254; 18,-48714.62742; 19,
        -59249.40528; 20,-59161.0404; 21,-56793.59538; 22,-52699.33704; 23,-44223.9339;
        24,-36856.7682])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{84,42},{74,52}})));
  Modelica.Blocks.Sources.CombiTimeTable qOut1(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,16; 1,14; 2,18; 3,10; 4,15; 5,43; 6,57; 7,91; 8,542; 9,124; 10,82;
        11,59; 12,55; 13,65; 14,82; 15,75; 16,77; 17,58; 18,46; 19,70; 20,58; 21,
        21; 22,16; 23,15; 24,16]) "Traffic outflow for residential block1"
    annotation (Placement(transformation(extent={{-100,24},{-90,34}})));
  Modelica.Blocks.Sources.CombiTimeTable proRes1(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.3; 1,0.28; 2,0.25; 3,0.25; 4,0.15; 5,0.1; 6,0.1; 7,0.1; 8,0.05; 9,
        0.05; 10,0.05; 11,0.05; 12,0.05; 13,0.05; 14,0.05; 15,0.05; 16,0.05; 17,
        0.05; 18,0.05; 19,0.05; 20,0.15; 21,0.15; 22,0.3; 23,0.3; 24,0.3])
    "Probability of charging for a single EV at different time in a residential block"
    annotation (Placement(transformation(extent={{-100,4},{-90,14}})));
  Modelica.Blocks.Sources.CombiTimeTable qOut2(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,18; 1,10; 2,5; 3,8; 4,18; 5,54; 6,72; 7,108; 8,576; 9,109; 10,79; 11,
        80; 12,81; 13,85; 14,97; 15,72; 16,38; 17,49; 18,65; 19,74; 20,65; 21,28;
        22,15; 23,18; 24,18]) "Traffic outflow for residential block1"
    annotation (Placement(transformation(extent={{84,24},{74,34}})));
  Modelica.Blocks.Sources.CombiTimeTable proRes2(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.3; 1,0.28; 2,0.25; 3,0.25; 4,0.15; 5,0.1; 6,0.1; 7,0.1; 8,0.05; 9,
        0.05; 10,0.05; 11,0.05; 12,0.05; 13,0.05; 14,0.05; 15,0.05; 16,0.05; 17,
        0.05; 18,0.05; 19,0.05; 20,0.15; 21,0.15; 22,0.3; 23,0.3; 24,0.3])
    "Probability of charging for a single EV at different time in a residential block"
    annotation (Placement(transformation(extent={{84,4},{74,14}})));
  Modelica.Blocks.Sources.CombiTimeTable qOut3(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,18; 1,13; 2,16; 3,18; 4,14; 5,19; 6,26; 7,16; 8,35; 9,41; 10,49; 11,
        55; 12,63; 13,57; 14,69; 15,79; 16,75; 17,108; 18,551; 19,124; 20,106; 21,
        58; 22,54; 23,42; 24,18]) "Traffic outflow for residential block1"
    annotation (Placement(transformation(extent={{-100,-56},{-90,-46}})));
  Modelica.Blocks.Sources.CombiTimeTable proRes3(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.03; 1,0.03; 2,0.03; 3,0.03; 4,0.03; 5,0.03; 6,0.03; 7,0.03; 8,0.05;
        9,0.05; 10,0.08; 11,0.08; 12,0.08; 13,0.08; 14,0.08; 15,0.08; 16,0.08; 17,
        0.03; 18,0.03; 19,0.03; 20,0.03; 21,0.03; 22,0.03; 23,0.03; 24,0.03])
    "Probability of charging for a single EV at different time in a residential block"
    annotation (Placement(transformation(extent={{-100,-72},{-90,-62}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin1(
    V_nominal=V_nominal,
    P_nominal=PLoa_nominal,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    l=l,
    redeclare replaceable
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic
      commercialCable=commercialCable) "Line from or to grid" annotation (
      Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={-64,0})));

  Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin2(
    V_nominal=V_nominal,
    P_nominal=PLoa_nominal,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    l=l,
    redeclare replaceable
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic
      commercialCable=commercialCable) "Line from or to grid" annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={0,68})));

  Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin3(
    V_nominal=V_nominal,
    P_nominal=PLoa_nominal,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    l=l,
    redeclare replaceable
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic
      commercialCable=commercialCable) "Line from or to grid" annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-72,56})));

equation
  connect(resBlo1.qOut[1], roa1.qIn)
    annotation (Line(points={{-34,38},{-34,32},{-12,32}}, color={28,108,200},
      thickness=1));
  connect(roa1.qOut, resBlo2.qIn[1])
    annotation (Line(points={{11,32},{46,32},{46,38}}, color={28,108,200},
      thickness=1));
  connect(resBlo1.qOut[2], roa3.qIn)
    annotation (Line(points={{-34,38},{-34,12}},        color={28,108,200},
      thickness=1));
  connect(resBlo2.qOut[1], roa2.qIn)
    annotation (Line(points={{34,38},{34,24},{12,24}}, color={28,108,200},
      thickness=1));
  connect(roa2.qOut, resBlo1.qIn[1])
    annotation (Line(points={{-11,24},{-46,24},{-46,38}}, color={28,108,200},
      thickness=1));
  connect(resBlo2.qOut[2], roa5.qIn)
    annotation (Line(points={{34,38},{34,12}},       color={28,108,200},
      thickness=1));
  connect(roa5.qOut, comBlo.qIn[2]) annotation (Line(points={{34,-11},{34,-28},
          {-6,-28},{-6,-44}}, color={28,108,200},
      thickness=1));
  connect(roa3.qOut, comBlo.qIn[1]) annotation (Line(
      points={{-34,-11},{-34,-28},{-6,-28},{-6,-44}},
      color={28,108,200},
      thickness=1));
  connect(roa6.qOut, resBlo2.qIn[2]) annotation (Line(
      points={{46,11},{46,38}},
      color={28,108,200},
      thickness=1));
  connect(comBlo.qOut[1], roa4.qIn) annotation (Line(
      points={{6,-44},{6,-36},{-46,-36},{-46,-12}},
      color={28,108,200},
      thickness=1));
  connect(roa4.qOut, resBlo1.qIn[2]) annotation (Line(
      points={{-46,11},{-46,38}},
      color={28,108,200},
      thickness=1));
  connect(comBlo.qOut[2], roa6.qIn) annotation (Line(
      points={{6,-44},{6,-36},{46,-36},{46,-12}},
      color={28,108,200},
      thickness=1));
  connect(weaDat.weaBus, resBlo1.weaBus) annotation (Line(
      points={{-80,104},{-60,104},{-60,58.6},{-51,58.6}},
      color={255,204,51},
      thickness=0.5));
  connect(weaDat.weaBus, resBlo2.weaBus) annotation (Line(
      points={{-80,104},{70,104},{70,58.6},{51,58.6}},
      color={255,204,51},
      thickness=0.5));
  connect(weaDat.weaBus, comBlo.weaBus) annotation (Line(
      points={{-80,104},{-60,104},{-60,-64.6},{-11,-64.6}},
      color={255,204,51},
      thickness=0.5));
  connect(PBui1.y[1], resBlo1.PBui[10]) annotation (Line(
      points={{-89.5,47},{-78,47},{-78,52},{-52,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui1.y[1], resBlo1.PBui[11]) annotation (Line(
      points={{-89.5,47},{-78,47},{-78,52},{-52,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui1.y[1], resBlo1.PBui[12]) annotation (Line(
      points={{-89.5,47},{-78,47},{-78,52},{-52,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui3.y[1], comBlo.PBui[10]) annotation (Line(
      points={{-89.5,-33},{-80,-33},{-80,-58},{-12,-58}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui3.y[1], comBlo.PBui[11]) annotation (Line(
      points={{-89.5,-33},{-80,-33},{-80,-58},{-12,-58}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui3.y[1], comBlo.PBui[12]) annotation (Line(
      points={{-89.5,-33},{-80,-33},{-80,-58},{-12,-58}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui1.y[1], resBlo1.PBui[1]) annotation (Line(
      points={{-89.5,47},{-78,47},{-78,52},{-52,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui2.y[1], resBlo2.PBui[10]) annotation (Line(
      points={{73.5,47},{60,47},{60,52},{52,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut1.y[1], resBlo1.qOutSet[2]) annotation (Line(
      points={{-89.5,29},{-78,29},{-78,45},{-52,45}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut1.y[1], resBlo1.numSenPac[2]) annotation (Line(
      points={{-89.5,29},{-78,29},{-78,48},{-52,48}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(proRes1.y[1], resBlo1.proEV) annotation (Line(
      points={{-89.5,9},{-78,9},{-78,40.6},{-52,40.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut2.y[1], resBlo2.qOutSet[2]) annotation (Line(points={{73.5,29},{
          60,29},{60,45},{52,45}},
                               color={0,0,127},
      pattern=LinePattern.Dot));
  connect(proRes2.y[1], resBlo2.proEV) annotation (Line(
      points={{73.5,9},{60,9},{60,40.6},{52,40.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut2.y[1], resBlo2.numSenPac[2]) annotation (Line(
      points={{73.5,29},{60,29},{60,48},{52,48}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(proRes3.y[1], comBlo.proEV) annotation (Line(
      points={{-89.5,-67},{-72,-67},{-72,-46.6},{-12,-46.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut3.y[1], comBlo.qOutSet[1]) annotation (Line(
      points={{-89.5,-51},{-12,-51}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut3.y[1], comBlo.numSenPac[2]) annotation (Line(
      points={{-89.5,-51},{-72,-51},{-72,-54},{-12,-54}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(gri.terminal, lin2.terminal_n) annotation (Line(
      points={{-90,74},{-90,68},{-10,68}},
      color={0,120,120},
      thickness=0.5));
  connect(resBlo2.term_p, lin2.terminal_p) annotation (Line(
      points={{51,56.2},{58,56.2},{58,68},{10,68}},
      color={0,120,120},
      thickness=0.5));
  connect(gri.terminal, lin1.terminal_n) annotation (Line(
      points={{-90,74},{-90,68},{-64,68},{-64,10}},
      color={0,120,120},
      thickness=0.5));
  connect(lin1.terminal_p, comBlo.term_p) annotation (Line(
      points={{-64,-10},{-64,-62.2},{-11,-62.2}},
      color={0,120,120},
      thickness=0.5));
  connect(gri.terminal, lin3.terminal_n) annotation (Line(
      points={{-90,74},{-90,56},{-82,56}},
      color={0,120,120},
      thickness=0.5));
  connect(lin3.terminal_p, resBlo1.term_p) annotation (Line(
      points={{-62,56},{-62,56.2},{-51,56.2}},
      color={0,120,120},
      thickness=0.5));
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false, extent={{-120,
            -100},{100,120}})),                                  Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{100,
            120}})));
end ConnectedCommunities_EnergyTransportation;
