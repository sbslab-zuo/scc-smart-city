within MultiInfrastructure.CaseStudy.EnergyTransportationSystem;
model Tutorial "Tutorial to demonstrate the energy and transportation system"
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";

  MultiInfrastructure.CaseStudy.EnergyTransportationSystem.BaseClasses.EnergyTransportation resBlo(
    lat=weaDat.lat,
    numEV=850,
    A=20000,
    EMax=1.8e10,
    betDis=0.5,
    betCha=1,
    num=1) "Residential block"
    annotation (Placement(transformation(extent={{-40,0},{-20,20}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
      computeWetBulbTemperature=false, filNam=
        ModelicaServices.ExternalReferences.loadResource("modelica://MultiInfrastructure/Resources/WeatherData/USA_CA_San.Francisco.Intl.AP.724940_TMY3.mos"))
    annotation (Placement(transformation(extent={{-100,70},{-80,90}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(f=f, V=V_nominal)
    annotation (Placement(transformation(extent={{-100,40},{-80,60}})));
  Modelica.Blocks.Sources.CombiTimeTable powBuiRes(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://MultiInfrastructure/Resources/Input/CaseStudy/EnergyTransportationSystem/Tutorial/powBuiRes.txt"),
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-31642.24665; 1,-25737.65655; 2,-23374.2521; 3,-22685.2667; 4,-22501.96805;
        5,-22001.6213; 6,-23525.33505; 7,-28600.1871; 8,-33781.5394; 9,-29866.28095;
        10,-23544.42185; 11,-22349.86555; 12,-21847.5527; 13,-21501.7796; 14,-21735.9029;
        15,-22513.4214; 16,-24298.4994; 17,-30480.0999; 18,-41825.40685; 19,-50450.39685;
        20,-50320.518; 21,-48195.64265; 22,-44804.9175; 23,-37865.276; 24,-31642.24665])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,0},{-80,20}})));

  Modelica.Blocks.Sources.CombiTimeTable proRes(
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://MultiInfrastructure/Resources/Input/CaseStudy/EnergyTransportationSystem/Tutorial/proRes.txt"),
    tableOnFile=true,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.3; 1,0.28; 2,0.25; 3,0.25; 4,0.15; 5,0.1; 6,0.1; 7,0.1; 8,0.05;
        9,0.05; 10,0.05; 11,0.05; 12,0.05; 13,0.05; 14,0.05; 15,0.05; 16,0.05;
        17,0.05; 18,0.05; 19,0.05; 20,0.15; 21,0.15; 22,0.3; 23,0.3; 24,0.3])
    "Probability of charging for a single EV at different time in a residential block"
    annotation (Placement(transformation(extent={{-100,-80},{-80,-60}})));

  MultiInfrastructure.CaseStudy.EnergyTransportationSystem.BaseClasses.EnergyTransportation comBlo(
    lat=weaDat.lat,
    numEV=850,
    A=20000,
    EMax=1.8e10,
    betDis=0.5,
    betCha=1,
    num=1) "Commercial block"
    annotation (Placement(transformation(extent={{40,0},{20,20}})));
  Modelica.Blocks.Sources.CombiTimeTable powBuiCom(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://MultiInfrastructure/Resources/Input/CaseStudy/EnergyTransportationSystem/Tutorial/powBuiCom.txt"),
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-15106.99921,-39665.66037,-113199.5177,-46268.5865; 1,-15106.99921,
        -22880.25538,-109803.5388,-44616.12113; 2,-15106.99921,-14648.29608,-102368.0109,
        -48249.87495; 3,-15162.01319,-14664.78088,-118330.9617,-44890.35052; 4,
        -15190.36283,-14671.08278,-106788.0383,-45974.81958; 5,-15292.58251,-14817.98117,
        -148854.4245,-48485.49996; 6,-15288.13499,-23008.873,-183652.7374,-53000.83359;
        7,-22916.16308,-38970.01719,-232847.716,-71074.69613; 8,-22530.14638,-43260.60256,
        -247709.1024,-78345.33465; 9,-32344.27609,-42212.49951,-248902.5028,-97123.15792;
        10,-48736.55298,-34726.26651,-256398.0105,-81180.84723; 11,-48736.55298,
        -42235.95747,-255070.6781,-56799.73352; 12,-48736.55298,-43019.25007,-258375.3686,
        -53500.92217; 13,-48736.55298,-42320.02807,-248890.5716,-52553.03076;
        14,-48736.55298,-34790.53907,-255974.4066,-51871.65923; 15,-48736.55298,
        -34789.96052,-250712.9033,-51188.67417; 16,-48736.55298,-34769.04615,-248438.0369,
        -50625.71801; 17,-54479.05298,-44296.78038,-256005.5167,-58819.71068;
        18,-60221.55298,-46418.70809,-263049.3853,-71170.73161; 19,-48376.76377,
        -46421.70322,-174078.2046,-85193.82708; 20,-48376.76377,-39060.35347,-172535.5662,
        -85850.21377; 21,-43829.27609,-39038.64715,-150404.431,-91994.72379; 22,
        -22377.64918,-38966.13298,-125633.5674,-89762.32297; 23,-15106.99921,-38856.78773,
        -95590.35684,-67564.07879; 24,-15106.99921,-39665.66037,-113199.5177,-46268.5865])
    "Power profile for a commercial building block"
    annotation (Placement(transformation(extent={{100,0},{80,20}})));

  MultiInfrastructure.CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses.RoadVariableDelay
    roa2(
    numIni=5,
    l=9000,
    redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp)         annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={0,-40})));

  MultiInfrastructure.CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses.RoadVariableDelay
    roa1(
    numIni=5,
    l=9000,
    redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp)         annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={0,-20})));
  Modelica.Blocks.Sources.CombiTimeTable qOutComRes(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://MultiInfrastructure/Resources/Input/CaseStudy/EnergyTransportationSystem/Tutorial/qOutComRes.txt"),
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,23; 1,17; 2,21; 3,22; 4,18; 5,14; 6,20; 7,20; 8,41; 9,48; 10,57;
        11,66; 12,72; 13,69; 14,89; 15,71; 16,86; 17,125; 18,555; 19,165; 20,98;
        21,55; 22,46; 23,31; 24,23])
    "Traffic outflow for commercial block to residential block"
    annotation (Placement(transformation(extent={{100,-40},{80,-20}})));
  Modelica.Blocks.Sources.CombiTimeTable qOutResCom(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://MultiInfrastructure/Resources/Input/CaseStudy/EnergyTransportationSystem/Tutorial/qOutResCom.txt"),
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,16; 1,14; 2,18; 3,10; 4,15; 5,43; 6,57; 7,91; 8,542; 9,124; 10,82;
        11,59; 12,55; 13,65; 14,82; 15,75; 16,77; 17,58; 18,46; 19,70; 20,58;
        21,21; 22,16; 23,15; 24,16])
    "Traffic outflow for residential block1 to commercial block"
    annotation (Placement(transformation(extent={{-100,-40},{-80,-20}})));
  Modelica.Blocks.Sources.CombiTimeTable proCom(
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://MultiInfrastructure/Resources/Input/CaseStudy/EnergyTransportationSystem/Tutorial/proCom.txt"),
    tableOnFile=true,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.03; 1,0.03; 2,0.03; 3,0.03; 4,0.03; 5,0.03; 6,0.03; 7,0.03; 8,
        0.05; 9,0.05; 10,0.08; 11,0.08; 12,0.08; 13,0.08; 14,0.08; 15,0.08; 16,
        0.08; 17,0.03; 18,0.03; 19,0.03; 20,0.03; 21,0.03; 22,0.03; 23,0.03; 24,
        0.03])
    "Probability of charging for a single EV at different time in a commercial block"
    annotation (Placement(transformation(extent={{100,-80},{80,-60}})));
equation
  connect(weaDat.weaBus, resBlo.weaBus) annotation (Line(
      points={{-80,80},{-50,80},{-50,18.6},{-41,18.6}},
      color={255,204,51},
      thickness=0.5));
  connect(gri.terminal, resBlo.term_p) annotation (Line(points={{-90,40},{-90,
          30},{-60,30},{-60,16.2},{-41,16.2}},
                             color={0,120,120}));
  for i in {1,2,3,4,5,6,7,8,9,10,11,12} loop
    connect(powBuiRes.y[1], resBlo.PBui[i]);
  end for;
  connect(proRes.y[1], resBlo.proEV) annotation (Line(points={{-79,-70},{-60,
          -70},{-60,0.6},{-42,0.6}},   color={0,0,127}));
  for i in {1,2,3,4,5,6,7,8,9,10,11,12} loop
    connect(powBuiCom.y[1], comBlo.PBui[i]);
  end for;
  connect(comBlo.qOut[1], roa2.qIn) annotation (Line(points={{24,-2},{24,-40},{
          12,-40}},                         color={0,0,127}));
  connect(roa2.qOut, resBlo.qIn[1]) annotation (Line(points={{-11,-40},{-36,-40},
          {-36,-2}},                              color={0,0,127}));
  connect(resBlo.qOut[1], roa1.qIn)
    annotation (Line(points={{-24,-2},{-24,-20},{-12,-20}},
                                                       color={0,0,127}));
  connect(roa1.qOut, comBlo.qIn[1])
    annotation (Line(points={{11,-20},{36,-20},{36,-2}},
                                                       color={0,0,127}));
  connect(qOutComRes.y[1], comBlo.qOutSet[1]) annotation (Line(points={{79,-30},
          {70,-30},{70,4},{42,4}},   color={0,0,127}));
  connect(qOutComRes.y[1], comBlo.numSenPac[1]) annotation (Line(points={{79,-30},
          {70,-30},{70,8},{42,8}},   color={0,0,127}));
  connect(weaDat.weaBus, comBlo.weaBus) annotation (Line(
      points={{-80,80},{60,80},{60,18.6},{41,18.6}},
      color={255,204,51},
      thickness=0.5));
  connect(qOutResCom.y[1], resBlo.numSenPac[1]) annotation (Line(points={{-79,-30},
          {-70,-30},{-70,8},{-42,8}}, color={0,0,127}));
  connect(qOutResCom.y[1], resBlo.qOutSet[1]) annotation (Line(points={{-79,-30},
          {-70,-30},{-70,4},{-42,4}}, color={0,0,127}));
  connect(proCom.y[1], comBlo.proEV) annotation (Line(points={{79,-70},{60,-70},
          {60,0.6},{42,0.6}},   color={0,0,127}));
  connect(gri.terminal, comBlo.term_p) annotation (Line(points={{-90,40},{-90,
          30},{50,30},{50,16.2},{41,16.2}}, color={0,120,120}));
  annotation (Icon(graphics,
                   coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{120,100}})),                                  Diagram(graphics,
        coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,
            100}})),
             __Dymola_Commands(file="modelica://MultiInfrastructure/Resources/Scripts/Dymola/CaseStudy/EnergyTransportationSystem/Tutorial/Tutorial.mos"
        "Simulate and plot"),
    experiment(StopTime=86400,
     __Dymola_Algorithm="Cvode",
     Tolerance=1e-4));
end Tutorial;
