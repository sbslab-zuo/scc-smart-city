within MultiInfrastructure.Buildings.Controls.OBC.ASHRAE;
package G36 "Package with control sequences from ASHRAE Guideline 36"
  extends Modelica.Icons.VariantsPackage;

annotation (Documentation(info="<html>
<p>
This package contains control sequences from
ASHRAE Guideline 36.
</p>
</html>"));
end G36;
